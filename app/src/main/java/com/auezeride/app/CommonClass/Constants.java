package com.auezeride.app.CommonClass;


import androidx.fragment.app.Fragment;

public class Constants {
    public static Fragment TripFlowFragmant = null;
    public static Fragment TripFlowSearchScreen = null;
    public static Fragment TempFragment = null;
    public static String GooglPlaceApikey = "AIzaSyBLcDq_lVP2Qe2PHQcdLamEbVLws7FDoVM";
    public static String GoogleDirectionkey = "AIzaSyBLcDq_lVP2Qe2PHQcdLamEbVLws7FDoVM";
    public static String GoogleGeocoderAPI = "https://maps.googleapis.com/maps/api/";
    public static String FlowStatus = "0";
    public static String CheckRiderStatus= "0";

    public static String capitalizeFirstLetter(String original) {
        if (original == null || original.length() == 0) {
            return original;
        }
        return original.substring(0, 1).toUpperCase() + original.substring(1);
    }

    // The minimum distance to change Updates in meters
    public static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 1; // 10 meters

    // The minimum time between updates in milliseconds
    public static final long MIN_TIME_BW_UPDATES = 1000 * 2; //2 Seconds
    //Map zoom level
    public static final float MAP_ZOOM_SIZE = 15;
    public static final long SET_INTERVAL = 5000; //5 Seconds
    public static final long SET_FASTESTINTERVAL = 3000; //3 Seconds

    public static final int REQUEST_CHECK_SETTINGS = 0x1;


    public static final float MAP_ZOOM_SIZE_ONTRIP = 17;


}
