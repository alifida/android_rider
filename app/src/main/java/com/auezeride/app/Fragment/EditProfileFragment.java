package com.auezeride.app.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.core.content.FileProvider;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.google.gson.Gson;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import com.auezeride.app.CommonClass.BaseFragment;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import com.auezeride.app.Activity.ProfileActivity;
import com.auezeride.app.CommonClass.CommonData;
import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.CommonClass.PermissionManager;
import com.auezeride.app.CommonClass.RoundImageTransform;
import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.CustomizeDialog.CustomDialogClass;
import com.auezeride.app.Model.EditProfileModel;
import com.auezeride.app.Model.LanguageCurrencyModel;
import com.auezeride.app.Presenter.UpdateProfilePresenter;

import com.auezeride.app.R;

import com.auezeride.app.View.CurrencyLanguageView;
import com.auezeride.app.View.UpdateProfileView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;


public class EditProfileFragment extends BaseFragment implements Validator.ValidationListener, CurrencyLanguageView, UpdateProfileView {
    @BindView(R.id.rider_profile_image)
    ImageView riderProfileImage;
    // @NotEmpty(message = getString(R.string.enter_your_first_name))
    @NotEmpty(message = "Enter Your First Name")
    @BindView(R.id.fname_edit)
    MaterialEditText fnameEdit;

    @NotEmpty(message = "Enter Your Last Name")
    //  @NotEmpty(message = getString(R.string.enter_your__last_name))
    @BindView(R.id.lname_edt)
    MaterialEditText lnameEdt;
    @NotEmpty(message = "")
    // @Email(message = getString(R.string.enter_valid_email_id))
    @Email(message = "Enter Valid Email ID")
    @BindView(R.id.email_edt)
    MaterialEditText emailEdt;
    @BindView(R.id.cc_edt)
    MaterialEditText ccEdt;
    @NotEmpty(message = "Enter Your Mobile Number")
    // @NotEmpty(message = getString(R.string.enter_your_mobile_number))
    @BindView(R.id.mobile_edt)
    MaterialEditText mobileEdt;
    @BindView(R.id.language_spinner)
    Spinner languageSpinner;
    @BindView(R.id.currency_spinner)
    Spinner currencySpinner;
    @BindView(R.id.update_information_btn)
    Button updateInformationBtn;
    Unbinder unbinder;
    Validator validator;

    Uri uri = null;
    private boolean CameraOrGalary = false;

    private PermissionManager permissionManager;
    public String path;

    private File photoFile;
    @BindView(R.id.back_img)
    ImageButton backImg;
    @BindView(R.id.password_edt)
    MaterialEditText passwordEdt;

    private CompositeDisposable disposable;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private CountryPicker picker;
    Activity activity;
    Context context;
    View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }
        }
        {
            // Inflate the layout for this fragment
            activity = getActivity();
            context = getContext();
            permissionManager = new PermissionManager();
            FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
            fontChanger.replaceFonts((ViewGroup) activity.findViewById(android.R.id.content));
            view = inflater.inflate(R.layout.fragment_edit_profile, container, false);
            unbinder = ButterKnife.bind(this, view);
            validator = new Validator(this);
            validator.setValidationListener(this);
            //CurrencyLanguagePresenter currencyLanguagePresenter = new CurrencyLanguagePresenter(this);
            // currencyLanguagePresenter.getCurrencyLanguage(activity);
            picker = CountryPicker.newInstance("Select Country");  // dialog title
            picker.setListener(new CountryPickerListener() {
                @Override
                public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                    ccEdt.setText(dialCode);
                    picker.dismiss();
                    // Implement your code here
                }
            });
            disposable = new CompositeDisposable();

            setData();
            Utiles.CircleImageView(Utiles.NullPointer(SharedHelper.getKey(context, "profile")), riderProfileImage, context);
            return view;
        }


        //lngTxt.setText(Utiles.NullPointer(SharedHelper.getKey(context, "language")));
        // currencyTxt.setText(Utiles.NullPointer(SharedHelper.getKey(context, "currency")));


    }

    private void setData() {

        fnameEdit.setText(Utiles.NullPointer(SharedHelper.getKey(context, "fname")));
        lnameEdt.setText(Utiles.NullPointer(SharedHelper.getKey(context, "lname")));
        emailEdt.setText(Utiles.NullPointer(SharedHelper.getKey(context, "emailid")));
        mobileEdt.setText(Utiles.NullPointer(SharedHelper.getKey(context, "mobile")));
        ccEdt.setText(Utiles.NullPointer(SharedHelper.getKey(context, "cc")));

        RemoveTheFocus();
    }

    private void RemoveTheFocus() {
        fnameEdit.setError(null);
        lnameEdt.setError(null);
        emailEdt.setError(null);
        mobileEdt.setError(null);
        ccEdt.setError(null);

    }

    @Override
    public void onResume() {
        super.onResume();
        setData();
    }

    @Override
    public void onDestroyView() {
        RemoveTheFocus();
        Utiles.clearInstance();
        super.onDestroyView();
        unbinder.unbind();
        if (disposable != null && disposable.isDisposed()) {
            disposable.clear();
        }
    }


    @Override
    public void onValidationSucceeded() {
        getUpdateProfile();
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(activity);
            if (view instanceof EditText) {
                switch (view.getId()) {
                    case R.id.fname_edit:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_your_first_name));
                        break;
                    case R.id.lname_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_your__last_name));
                        break;
                    case R.id.email_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_valid_email_id));
                        break;
                    case R.id.mobile_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_your_mobile_number));
                        break;
                }
            } else {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void LanguageSpinner(List<LanguageCurrencyModel.Data> Data) {
        List<String> langaugeList = new ArrayList<String>();
        for (int i = 0; Data.size() > i; i++) {
            langaugeList.add(Data.get(i).getName());
        }
        ArrayAdapter<String> languageAdapter = new ArrayAdapter<String>(activity,
                R.layout.dropdown, langaugeList);
        languageAdapter.setDropDownViewResource(R.layout.dropdown);
        languageSpinner.setAdapter(languageAdapter);
        int spinnerPosition = languageAdapter.getPosition(Utiles.NullPointer(SharedHelper.getKey(context, "language")));
        languageSpinner.setSelection(spinnerPosition);
        languageSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                CommonData.strLanguage = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private void Currencyspinner(List<LanguageCurrencyModel.Data> Data) {

        List<String> currencyList = new ArrayList<String>();
        for (int i = 0; Data.size() > i; i++) {
            currencyList.add(Data.get(i).getName());

        }
        ArrayAdapter<String> currencyAdapter = new ArrayAdapter<String>(activity,
                R.layout.dropdown, currencyList);
        currencyAdapter.setDropDownViewResource(R.layout.dropdown);
        currencySpinner.setAdapter(currencyAdapter);
        int spinnerPosition = currencyAdapter.getPosition(Utiles.NullPointer(SharedHelper.getKey(context, "currency")));
        currencySpinner.setSelection(spinnerPosition);
        currencySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {
                CommonData.strCurrency = adapterView.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    @Override
    public void countriesReady(List<LanguageCurrencyModel> LanguageCurrencyModel) {

        for (int i = 0; LanguageCurrencyModel.size() > i; i++) {
            if (LanguageCurrencyModel.get(i).getId().equalsIgnoreCase("1")) {
                Currencyspinner(LanguageCurrencyModel.get(i).getDatas());
            } else if (LanguageCurrencyModel.get(i).getId().equalsIgnoreCase("2")) {
                LanguageSpinner(LanguageCurrencyModel.get(i).getDatas());
            }

        }

    }

    @OnClick({R.id.cc_edt, R.id.update_information_btn, R.id.rider_profile_image})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cc_edt:
            /*    assert getFragmentManager() != null;
                picker.show(getFragmentManager(), "COUNTRY_PICKER");*/
                break;
            case R.id.update_information_btn:
                Utiles.hideKeyboard(activity);
                validator.validate();
                break;
            case R.id.rider_profile_image:

                ChooseUserProfile();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 4:
                if (resultCode == Activity.RESULT_OK) {
                   /* String imagepath = data.getStringExtra("image_path");
                    uri = Uri.parse(imagepath);
                    CameraOrGalary = true;
                    LocalImage(new File(uri.getPath()).toString(), riderProfileImage, activity);*/
                    LocalImage(path, riderProfileImage, activity);
                    Log.e("response", "" + path);
                    CameraOrGalary = true;
                }
                break;
            case 5:
                if (resultCode == Activity.RESULT_OK && data != null) {
                    uri = data.getData();
                    LocalImage(getRealPathFromURI(uri), riderProfileImage, activity);
                    CameraOrGalary = false;

                }
                break;
        }
    }

    private void LocalImage(String ImagePath, final ImageView imageView, final Activity activity) {
        System.out.println("enter the image view in android" + ImagePath);
        Glide.with(activity)
                .load(ImagePath)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .skipMemoryCache(true)
                .transform(new RoundImageTransform())
                .into(imageView);

    }

    @SuppressLint("LongLogTag")
    private void getUpdateProfile() {
        Utiles.hideKeyboard(activity);
        File file = null;

        if (uri != null) {
            if (CameraOrGalary) {
                file = new File(Objects.requireNonNull(uri.getPath()));
                // file = Utiles.saveBitmapToFile(file, "camera");


            } else {
                file = new File(getRealPathFromURI(uri));
                // file = Utiles.saveBitmapToFile(file, "gallery");
                // ImageUpload(file);

            }
            disposable.add(new Compressor(activity)
                    .setMaxWidth(640)
                    .setMaxHeight(480)
                    .setQuality(100)
                    .compressToFileAsFlowable(file)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(this::ImageUpload, throwable -> {
                        throwable.printStackTrace();
                        Utiles.displayMessage(getView(), activity, activity.getResources().getString(R.string.invalid_image));
                    }));
        } else {
            ImageUpload(file);
        }


    }

    private void ImageUpload(File file) {
        MultipartBody.Part filePart = null;
        HashMap<String, RequestBody> map = new HashMap<>();
        map.put("fname", RequestBody.create(MediaType.parse("text/plain"), Objects.requireNonNull(fnameEdit.getText()).toString()));
        map.put("lname", RequestBody.create(MediaType.parse("text/plain"), Objects.requireNonNull(lnameEdt.getText()).toString()));
        map.put("email", RequestBody.create(MediaType.parse("text/plain"), Objects.requireNonNull(emailEdt.getText()).toString()));
        map.put("phone", RequestBody.create(MediaType.parse("text/plain"), Objects.requireNonNull(mobileEdt.getText()).toString()));
        map.put("cnty", RequestBody.create(MediaType.parse("text/plain"), ""));
        map.put("cntyname", RequestBody.create(MediaType.parse("text/plain"), ""));
        map.put("lang", RequestBody.create(MediaType.parse("text/plain"), CommonData.strLanguage));
        map.put("cur", RequestBody.create(MediaType.parse("text/plain"), CommonData.strCurrency));
        map.put("phcode", RequestBody.create(MediaType.parse("text/plain"), Objects.requireNonNull(ccEdt.getText()).toString()));
        if (uri != null) {
            if (CameraOrGalary) {
                filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse(getMimeType(file.toString())), file));
                UpdateProfilePresenter updateProfilePresenter = new UpdateProfilePresenter(this);
                updateProfilePresenter.UpdateProfile(map, filePart, activity, context);
            } else {
                filePart = MultipartBody.Part.createFormData("file", file.getName(), RequestBody.create(MediaType.parse(getMimeType(getRealPathFromURI(uri))), file));
                UpdateProfilePresenter updateProfilePresenter = new UpdateProfilePresenter(this);
                updateProfilePresenter.UpdateProfile(map, filePart, activity, context);
            }

        } else {
            UpdateProfilePresenter updateProfilePresenter = new UpdateProfilePresenter(this);
            updateProfilePresenter.UpdateProfile(map, filePart, activity, context);
        }
    }

    @Override
    public void OnSuccessfully(Response<EditProfileModel> Response) {
        assert Response.body() != null;
        if (Response.body().getSuccess()) {
            SharedHelper.putKey(context, "fname", Response.body().getRequest().getFname());
            SharedHelper.putKey(context, "lname", Response.body().getRequest().getLname());
            SharedHelper.putKey(context, "emailid", Response.body().getRequest().getEmail());
            SharedHelper.putKey(context, "cc", Response.body().getRequest().getPhcode());
            SharedHelper.putKey(context, "mobile", Response.body().getRequest().getPhone());
            SharedHelper.putKey(context, "language", Response.body().getRequest().getLang());
            SharedHelper.putKey(context, "currency", Response.body().getRequest().getCur());
            SharedHelper.putKey(context, "profile", Response.body().getFileurl());
            Intent intent = new Intent(context, ProfileActivity.class);
            startActivity(intent);
            activity.finish();

        }
    }

    @Override
    public void OnFailure(Response<EditProfileModel> Response) {
        Log.e("TAG", "update profile response " + new Gson().toJson(Response.errorBody()));
        try {
            Utiles.showErrorMessage(Response.errorBody().string(), activity, getView());
        } catch (Exception e) {
            e.printStackTrace();
            Utiles.displayMessage(getView(), context, context.getString(R.string.poor_network));
        }

    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    private static String getMimeType(String url) {
        String type = null;
        String extension = MimeTypeMap.getFileExtensionFromUrl(url);
        if (extension != null) {
            type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
        }
        return type;
    }

    private void ChooseUserProfile() {
        final Dialog dialog = new Dialog(activity);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;
        dialog.setContentView(R.layout.cameraorgalary);
        final View decorView = dialog.getWindow().getDecorView();
        Utiles.StartAnimation(decorView);
        Button camera_btn = dialog.findViewById(R.id.camera_btn);
        Button gallary_btn = dialog.findViewById(R.id.gallary_btn);

        View.OnClickListener clickListener = view -> {
            switch (view.getId()) {
                case R.id.camera_btn:

                    Utiles.DismissiAnimation(decorView, dialog);
                    // getActivity().startActivityForResult(new Intent(context, CustmizedCamera.class), 4);
                    takePhotoFromCamera();
                    break;
                case R.id.gallary_btn:
                    Utiles.DismissiAnimation(decorView, dialog);
                    if (permissionManager.userHasPermission(getActivity())) {
                        Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, 5);
                    } else {
                        permissionManager.requestPermission(getActivity());

                    }
                    break;
            }
        };


        camera_btn.setOnClickListener(clickListener);
        gallary_btn.setOnClickListener(clickListener);
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (!isRemoving() && !isDetached()) {
                    dialog.show();
                }
            }
        });
    }

    private void takePhotoFromCamera() {
        if (permissionManager.userHasPermission(getActivity())) {
            takePicture();
        } else {
            permissionManager.requestPermission(getActivity());
        }
    }

    private void takePicture() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(Objects.requireNonNull(getActivity()).getPackageManager()) != null) {
            Uri photoURI = null;
            try {
                photoFile = createImageFileWith();
                path = photoFile.getAbsolutePath();
                uri = Uri.parse(path);
                photoURI = FileProvider.getUriForFile(getActivity(), getString(R.string.file_provider_authority), photoFile);
                Log.e("response", "" + path + "////" + photoURI);

            } catch (IOException ex) {
                Log.e("TakePicture", Objects.requireNonNull(ex.getMessage()));
            }
            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
            if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP) {
                takePictureIntent.setClipData(ClipData.newRawUri("", photoURI));
                takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
            }
            startActivityForResult(takePictureIntent, 4);
        }
    }


    private File createImageFileWith() throws IOException {
        @SuppressLint("SimpleDateFormat") final String timestamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        final String imageFileName = "JPEG_" + timestamp;
        File storageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "pics");
        storageDir.mkdirs();
        return File.createTempFile(imageFileName, ".jpg", storageDir);
    }

    @OnClick({R.id.back_img, R.id.password_edt})
    public void onViewClickeds(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                Utiles.hideKeyboard(activity);
                assert getFragmentManager() != null;
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.password_edt:
                CustomDialogClass dialogClass = new CustomDialogClass(activity);
                dialogClass.setCancelable(false);
                Objects.requireNonNull(dialogClass.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;
                try {
                    final View decorView = dialogClass.getWindow().getDecorView();
                    Utiles.StartAnimation(decorView);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                dialogClass.show();
                break;
        }
    }


}
