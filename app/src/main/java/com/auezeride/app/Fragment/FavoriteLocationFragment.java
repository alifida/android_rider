package com.auezeride.app.Fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import com.auezeride.app.Activity.SetPinLocationActivity;
import com.auezeride.app.Adapter.FavouriteAdapter;
import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.CommonClass.CommonData;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.EventBus.AddFavoriteEvent;
import com.auezeride.app.GooglePlace.GooglePlcaeModel.PlacesResults;
import com.auezeride.app.GooglePlace.GooglePlcaeModel.Prediction;
import com.auezeride.app.GooglePlace.ReverseGeoCoderModel.ReverseGeocoderModel;
import com.auezeride.app.Presenter.GooglePlcaePresenter;
import com.auezeride.app.R;
import com.auezeride.app.View.GoogleAutoPlaceView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Optional;
import butterknife.Unbinder;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class FavoriteLocationFragment extends BaseFragment implements GoogleAutoPlaceView, FavouriteAdapter.FavoriteLocation {


    @BindView(R.id.back_img)
    ImageButton backImg;
    @BindView(R.id.title_txt)
    TextView titleTxt;
    @BindView(R.id.menu_img)
    ImageButton menuImg;
    @BindView(R.id.header)
    RelativeLayout header;
    Unbinder unbinder;
    @BindView(R.id.place_recycleview)
    RecyclerView placeRecycleview;
    private GooglePlcaePresenter googlePlcaePresenter;

    public FavoriteLocationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_favorite_location, container, false);
        unbinder = ButterKnife.bind(this, view);
        googlePlcaePresenter = new GooglePlcaePresenter(this);
        setAdapter();
        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
        if(EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().unregister(this);
        }
    }

    @Optional
    @OnClick({R.id.back_img, R.id.menu_img})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                fragmentManager.popBackStackImmediate();
                break;
            case R.id.menu_img:
                CommonData.HeaderTitle = "Add Favorite Location";
                Intent intents = new Intent(context, SetPinLocationActivity.class);
                startActivity(intents);
                break;
        }
    }

    @Override
    public void GooglePlacee(List<Prediction> prediction) {

    }

    @Override
    public void GooglePlaceError(Response<PlacesResults> resultsResponse) {

    }

    @Override
    public void GoogleReverseGoecoder(Response<ReverseGeocoderModel> response) {

    }

    @Override
    public void OnSuccessfully(Response<ResponseBody> response) {
        try {
            assert response.body() != null;
            String message = response.body().string();
            JSONObject jsonObject = new JSONObject(message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnFailure(Response<ResponseBody> response) {
        try {
            assert response.errorBody() != null;
            String Message = response.errorBody().string();

            JSONObject jsonObject = new JSONObject(Message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }

        } catch (IOException | JSONException e) {
            Utiles.displayMessage(getView(), context, activity.getString(R.string.something_went_wrong));
        }
    }

    public void setAdapter() {
        if (CommonData.addressList != null) {
            placeRecycleview.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
            placeRecycleview.setItemAnimator(new DefaultItemAnimator());
            FavouriteAdapter favouriteAdapter = new FavouriteAdapter(activity, this);
            placeRecycleview.setAdapter(favouriteAdapter);
        }

    }

    @Override
    public void favoriteLocation(int position, boolean isadded) {
        if(!isadded){
            googlePlcaePresenter.getDeteleteAddress(activity, CommonData.addressList.get(position).getId());
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        if(!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
    }
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessage(AddFavoriteEvent event) {
        setAdapter();
        EventBus.getDefault().removeStickyEvent(AddFavoriteEvent.class); // don't forget to remove the sticky event if youre done with it
    }
}
