package com.auezeride.app.Fragment;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.credentials.Credential;
import com.google.android.gms.auth.api.credentials.HintRequest;
import com.google.android.gms.auth.api.phone.SmsRetriever;
import com.google.android.gms.auth.api.phone.SmsRetrieverClient;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.Task;
import com.google.i18n.phonenumbers.NumberParseException;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import com.auezeride.app.Appcontroller.MySMSBroadcastReceiver;
import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.CustomizeDialog.OTPDialog;
import com.auezeride.app.Model.OTPModel;

import com.rengwuxian.materialedittext.MaterialEditText;

import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.CommonClass.JWTUtils;
import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.MainActivity;
import com.auezeride.app.Model.ProfileModel;
import com.auezeride.app.Model.RegisterModel;
import com.auezeride.app.Presenter.ProfilePresenter;
import com.auezeride.app.Presenter.RegisterPresenter;

import com.auezeride.app.R;

import com.auezeride.app.View.ProfileView;
import com.auezeride.app.View.RegisterView;

import com.ybs.countrypicker.Country;
import com.ybs.countrypicker.CountryPicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import com.auezeride.app.CommonClass.CommonData;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class RegisterFragment extends BaseFragment implements RegisterView, Validator.ValidationListener, ProfileView, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    @BindView(R.id.back_img)
    ImageButton backImg;

    @NotEmpty(message = "")
    @Length(min = 4, message = "Enter 4 Minimum to 12 Character")
    //  @Length(min = 4, message = getString(R.string.enter_minimum_to_chararacterss))
    @BindView(R.id.fname_edit)
    MaterialEditText fnameEdit;
    @NotEmpty(message = "")
    @Length(min = 4, message = "Enter 4 Minimum to 12 Character")
    @BindView(R.id.lname_edt)
    MaterialEditText lnameEdt;
    @NotEmpty(message = "")
    @Email(message = "Enter Valid Email id")
    @BindView(R.id.email_edt)
    MaterialEditText emailEdt;
    @NotEmpty(message = "")
    @Length(min = 6, message = "Enter the Minimum 6 to 12 Character")
    // @Length(min = 6, message = getString(R.string.enter_the_minimum_six_character))
    @BindView(R.id.password_edt)
    MaterialEditText passwordEdt;
    @BindView(R.id.cc_edt)
    MaterialEditText ccEdt;
    @NotEmpty(message = "")
    @Length(min = 6, max = 15, message = "Enter the Minimum 6 to 15 Character")
    // @Length(min = 6,max = 15 ,message = getString(R.string.enter_the_minimum_six_to_five_teen_character))
    @BindView(R.id.mobile_edt)
    MaterialEditText mobileEdt;
    @BindView(R.id.referral_edt)
    MaterialEditText referralEdt;
    @BindView(R.id.terms_condition_txt)
    CheckBox termsConditionTxt;
    @BindView(R.id.submit)
    Button submit;
    Unbinder unbinder;
    Context context;

    CountryPicker picker;

    public RegisterFragment() {
    }

    Validator validator;
    Activity activity;
    RegisterPresenter registerPresenter;
    private GoogleApiClient client;
    MySMSBroadcastReceiver smsBroadcast;
    private int RC_HINT = 2;
    FragmentActivity fragmentActivity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_register, container, false);
        unbinder = ButterKnife.bind(this, view);
        context = getContext();
        activity = getActivity();
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts((ViewGroup) activity.findViewById(android.R.id.content));
        validator = new Validator(this);
        validator.setValidationListener(this);
        registerPresenter = new RegisterPresenter(this);
        if (Build.VERSION.SDK_INT >= 23) {
            String[] PERMISSIONS = {android.Manifest.permission.READ_PHONE_STATE};
            if (!hasPermissions(context, PERMISSIONS)) {
                ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, 1);
            } else {
                SimCardStatusChecked();
            }
        } else {
            SimCardStatusChecked();
        }
        fragmentActivity = getActivity();
        picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener((name, code, dialCode, flagDrawableResID) -> {
            ccEdt.setText(dialCode);
            picker.dismiss();
            // Implement your code here
        });

        try {
            if (client == null) {
                client = new GoogleApiClient.Builder(activity)
                        .addConnectionCallbacks(this)
                        .enableAutoManage(fragmentActivity, this)
                        .addApi(Auth.CREDENTIALS_API)
                        .build();
                client.connect();
                requestHint();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }

        startSMSListener();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(SmsRetriever.SMS_RETRIEVED_ACTION);
        activity.registerReceiver(smsBroadcast, intentFilter);


        return view;

    }


    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        if (client != null && client.isConnected()) {
            client.stopAutoManage(fragmentActivity);
            client.disconnect();
        }
        unbinder.unbind();
    }

    @OnClick({R.id.back_img, R.id.submit, R.id.cc_edt,R.id.terms_contionlink})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                Utiles.hideKeyboard(activity);
                assert getFragmentManager() != null;
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.submit:
                Utiles.hideKeyboard(activity);
                if(!termsConditionTxt.isChecked()){
                    Utiles.CommonToast(activity,activity.getResources().getString(R.string.please_check_terms_and_condition));
                    return;
                }
                validator.validate();
                break;
            case R.id.cc_edt:
                assert getFragmentManager() != null;
                picker.show(getFragmentManager(), "COUNTRY_PICKER");
                break;
            case R.id.terms_contionlink:
                    moveToFragment(new SupportFragment());
                break;
        }
    }

    private void requestHint() {
        HintRequest hintRequest = new HintRequest.Builder()
                .setPhoneNumberIdentifierSupported(true)
                .build();
        try {
            PendingIntent intent = Auth.CredentialsApi.getHintPickerIntent(
                    client, hintRequest);
            activity.startIntentSenderForResult(intent.getIntentSender(), RC_HINT, null, 0, 0, 0);
        } catch (IntentSender.SendIntentException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void RegisterView(Response<RegisterModel> Response) {
        Log.e("tage", "token in " + Response.body().getToken());
        try {
            SharedHelper.putKey(Objects.requireNonNull(getContext()), "token", Response.body().getToken());
            new JWTUtils(this, null).decoded(Response.body().getToken(), "reg");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Errorlogview(Response<RegisterModel> Response) {
        try {
            assert Response.errorBody() != null;
            String Message = Response.errorBody().string();

            JSONObject jsonObject = new JSONObject(Message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }

        } catch (IOException | JSONException e) {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void JsonResponse(String object) {
        try {
            JSONObject obj = new JSONObject(object);
            String email = obj.getString("email");
            String name = obj.getString("name");
            String id = obj.getString("id");
            Log.e("response", "" + email + "==" + name + "==" + id);

            SharedHelper.putKey(context, "userid", id);
            SharedHelper.putKey(context, "email", email);

            ProfilePresenter profilePresenter = new ProfilePresenter(this);
            profilePresenter.getProfile(activity, true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSuccessOTP(Response<OTPModel> Response) {
        assert Response.body() != null;
        System.out.println("Enter OTP code" + Response.body().getCode());
        OTPDialog dialogClass = new OTPDialog(activity, Response.body().getCode(), this);
        dialogClass.setCancelable(false);
        Objects.requireNonNull(dialogClass.getWindow()).getAttributes().windowAnimations = R.style.DialogTheme;
        dialogClass.show();
    }

    @Override
    public void onFailureOTP(Response<OTPModel> Response) {
        try {
            assert Response.errorBody() != null;
            String Message = Response.errorBody().string();

            JSONObject jsonObject = new JSONObject(Message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }

        } catch (IOException | JSONException e) {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }

    }

    @Override
    public void OTPVerification() {
        Utiles.hideKeyboard(activity);
        RegisterationModel();
    }

    @Override
    public void onValidationSucceeded() {
        Utiles.hideKeyboard(activity);
        registerPresenter.getOTP(Objects.requireNonNull(mobileEdt.getText()).toString(), Objects.requireNonNull(emailEdt.getText()).toString(), Objects.requireNonNull(ccEdt.getText()).toString(), activity);
    }

    private void RegisterationModel() {
        HashMap<String, String> map = new HashMap<>();
        map.put("fname", Objects.requireNonNull(fnameEdit.getText()).toString());
        map.put("lname", Objects.requireNonNull(lnameEdt.getText()).toString());
        map.put("email", Objects.requireNonNull(emailEdt.getText()).toString());
        map.put("phone", Objects.requireNonNull(mobileEdt.getText()).toString());
        map.put("password", Objects.requireNonNull(passwordEdt.getText()).toString());
        map.put("phcode", Objects.requireNonNull(ccEdt.getText()).toString());
        map.put("cnty", "");
        map.put("fcmId", SharedHelper.getToken(context, "device_token"));
        map.put("cntyname", "");
        map.put("referal", referralEdt.getText().toString());
        map.put("lang", CommonData.strLanguage);
        map.put("cur", CommonData.strCurrency);

        registerPresenter.getRegisterApi(map, activity);
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(activity);
            if (view instanceof EditText) {
                switch (view.getId()) {
                    case R.id.fname_edit:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_minimum_to_chararacterss));
                        break;
                    case R.id.lname_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_minimum_to_chararacterss));
                        break;
                    case R.id.email_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_valid_email_id));
                        break;
                    case R.id.password_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_the_minimum_six_character));
                        break;
                    case R.id.mobile_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_the_minimum_six_to_five_teen_character));
                        break;
                }
            } else {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void OnSuccessfully(Response<List<ProfileModel>> Response) {
        try {
            assert Response.body() != null;
            SharedHelper.putKey(context, "fname", Response.body().get(0).getFname());
            SharedHelper.putKey(context, "lname", Response.body().get(0).getLname());
            SharedHelper.putKey(context, "emailid", Response.body().get(0).getEmail());
            SharedHelper.putKey(context, "referal_code", Response.body().get(0).getReferal());
            SharedHelper.putKey(context, "cc", Response.body().get(0).getPhcode());
            SharedHelper.putKey(context, "mobile", Response.body().get(0).getPhone());
            SharedHelper.putKey(context, "language", Response.body().get(0).getLang());
            SharedHelper.putKey(context, "currency", Response.body().get(0).getCur());
            SharedHelper.putKey(context, "profile", Response.body().get(1).getProfileurl());
            CommonData.addressList.addAll(Response.body().get(0).getAddress());
        } catch (Exception e) {
            e.printStackTrace();
        }

        Intent i = new Intent(context, MainActivity.class);
        startActivity(i);
        Utiles.UpDateRiders(SharedHelper.getKey(context, "userid"), context);
        activity.finish();
    }

    @Override
    public void OnFailure(Response<List<ProfileModel>> Response) {
        try {
            assert Response.errorBody() != null;
            Utiles.showErrorMessage(Response.errorBody().string(), activity, getView());
        } catch (Exception e) {
            e.printStackTrace();
            Utiles.displayMessage(getView(), context, context.getString(R.string.poor_network));
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SimCardStatusChecked();
                    //Do here
                } else {
                    Utiles.CommonToast(activity, activity.getResources().getString(R.string.required_permissionss));
                }
            }
        }
    }

    private static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void SimCardStatusChecked() {
        TelephonyManager telMgr = (TelephonyManager) activity.getSystemService(Context.TELEPHONY_SERVICE);
        assert telMgr != null;
        int simState = telMgr.getSimState();
        switch (simState) {
            case TelephonyManager.SIM_STATE_ABSENT:
           //     Utiles.CommonToast(activity, "Please Check Sim Card");
                break;
            case TelephonyManager.SIM_STATE_NETWORK_LOCKED:
              //  Utiles.CommonToast(activity, "Please Check NetWork");

                // do something
                break;
            case TelephonyManager.SIM_STATE_PIN_REQUIRED:
               // Utiles.CommonToast(activity, "Please Check Pin");
                // do something
                break;
            case TelephonyManager.SIM_STATE_PUK_REQUIRED:
               // Utiles.CommonToast(activity, "Please Check PUK");
                // do something
                break;
            case TelephonyManager.SIM_STATE_READY:
                Countrycoder();
                break;
            case TelephonyManager.SIM_STATE_UNKNOWN:
              //  Utiles.CommonToast(activity, "Please Check Sim Card");
                // do something
                break;
        }
    }

    public void Countrycoder() {
        Country country = Country.getCountryFromSIM(context); //Get app country based on SIM card
        ccEdt.setText(country.getDialCode());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    public void startSMSListener() {
        SmsRetrieverClient client = SmsRetriever.getClient(activity /* context */);
        Task<Void> task = client.startSmsRetriever();
        task.addOnSuccessListener(aVoid -> {
            // Utiles.CommonToast(activity, "start");
        });

        task.addOnFailureListener(e -> {
            // Failed to start retriever, inspect Exception for more details
            // ...
            //Utiles.CommonToast(activity, "Failed");
        });


    }

    public void mobileNumberParse(String number) {
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(number, "");
            int countryCode = numberProto.getCountryCode();
            long nationalNumber = numberProto.getNationalNumber();
            mobileEdt.setText(String.valueOf(nationalNumber));

        } catch (NumberParseException e) {
            System.err.println("NumberParseException was thrown: " + e.toString());
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_HINT && resultCode == Activity.RESULT_OK) {

            Credential credential = data.getParcelableExtra(Credential.EXTRA_KEY);
            assert credential != null;
            System.out.println("enter the mobile number" + credential.getId());
            mobileNumberParse(credential.getId());

        }
    }
    private void moveToFragment(Fragment fragment) {
        Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction()
                .replace(android.R.id.content, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commit();

    }
}
