package com.auezeride.app.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;

import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.CommonClass.JWTUtils;
import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.MainActivity;
import com.auezeride.app.Model.LoginModel;
import com.auezeride.app.Model.ProfileModel;
import com.auezeride.app.Presenter.LoginPresenter;
import com.auezeride.app.Presenter.ProfilePresenter;

import com.auezeride.app.R;

import com.auezeride.app.View.Loginview;
import com.auezeride.app.View.ProfileView;

import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import com.auezeride.app.CommonClass.CommonData;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class LoginFragment extends BaseFragment implements Loginview, Validator.ValidationListener, ProfileView {

    @NotEmpty(message = "Enter the Valid Email or Mobile")
    // @NotEmpty(message = getString(R.string.enter_the_valid_email_or_mobile))
    @BindView(R.id.email_edt)
    EditText emailEdt;
    @NotEmpty(message = "Enter the Password")
    @BindView(R.id.password_edt)
    EditText passwordEdt;
    @BindView(R.id.login_btn)
    Button loginBtn;
    @BindView(R.id.fpassword_txt)
    TextView fpasswordTxt;
    Unbinder unbinder;

    public LoginFragment() {
        // Required empty public constructor
    }

    Context context;
    Activity activity;
    Validator validator;
    CountryPicker picker;
    String strCountryCountry = "";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);

        unbinder = ButterKnife.bind(this, view);
        context = getContext();
        activity = getActivity();

        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts((ViewGroup) activity.findViewById(android.R.id.content));
        validator = new Validator(this);
        validator.setValidationListener(this);

        picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                strCountryCountry = dialCode;
                picker.dismiss();
                getLogin();
                // Implement your code here
            }
        });
       /* new Thread(() -> {

                System.out.println("emter the new token"+ FirebaseInstanceId.getInstance().getToken());

        }).start();*/


        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();

    }


    @OnClick({R.id.login_btn, R.id.fpassword_txt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.login_btn:
                Utiles.hideKeyboard(activity);
                validator.validate();


                break;
            case R.id.fpassword_txt:
                Fragment fragment = new ForgotPasswordFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.login_fragment, fragment);
                fragmentTransaction.addToBackStack("ForgetPassword");
                fragmentTransaction.commit();
                break;
        }
    }

    @Override
    public void LoginVIew(Response<LoginModel> Response) {
        try {
            SharedHelper.putKey(context, "token", Response.body().getToken());
            new JWTUtils(null, this).decoded(Response.body().getToken(), "login");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Errorlogview(Response<LoginModel> Response) {
        try {
            String Message = Response.errorBody().string();

            JSONObject jsonObject = new JSONObject(Message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }

        } catch (IOException | JSONException e) {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }


    }

    @Override
    public void JsonResponse(String object) {
        try {
            JSONObject obj = new JSONObject(object);
            String email = obj.getString("email");
            String name = obj.getString("name");
            String id = obj.getString("id");
            Log.e("response", "" + email + "==" + name + "==" + id);

            SharedHelper.putKey(getContext(), "userid", id);
            SharedHelper.putKey(getContext(), "email", email);

            ProfilePresenter profilePresenter = new ProfilePresenter(this);
            profilePresenter.getProfile(activity, true);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onValidationSucceeded() {
        if (checkForEmail(emailEdt.getText().toString())) {
            if (checkForMobile(emailEdt.getText().toString())) {
                strCountryCountry = "+91";
                //     picker.show(getFragmentManager(), "COUNTRY_PICKER");
                getLogin();
            } else {
                Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.please_enter_valid_email));
            }
        } else {
            getLogin();
        }

    }

    public void getLogin() {
        Utiles.hideKeyboard(activity);
        HashMap<String, String> map = new HashMap<>();
        map.put("username", emailEdt.getText().toString());
        map.put("password", passwordEdt.getText().toString());
        map.put("phcode", strCountryCountry);
        map.put("fcmId", SharedHelper.getToken(context, "device_token"));
        Log.e("tage", "Login tag" + map);
        LoginPresenter loginPresenter = new LoginPresenter(this);
        loginPresenter.getLogin(map, activity);
    }

    public boolean checkForEmail(String mStrEmail) {
        Context c;


        if (android.util.Patterns.EMAIL_ADDRESS.matcher(mStrEmail).matches()) {
            return false;
        }
        return true;
    }


    public boolean checkForMobile(String mStrMobile) {
        if (android.util.Patterns.PHONE.matcher(mStrMobile).matches()) {
            return true;
        }

        return false;
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(activity);
            if (view instanceof EditText) {


                switch (view.getId()) {
                    case R.id.email_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.please_enter_valid_email));
                        break;
                    case R.id.password_edt:
                        ((EditText) view).setError(activity.getResources().getString(R.string.enter_pasword));
                        break;
                }
            } else {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void OnSuccessfully(Response<List<ProfileModel>> Response) {
        Utiles.hideKeyboard(activity);
        try {
            assert Response.body() != null;
            SharedHelper.putKey(context, "fname", Response.body().get(0).getFname());
            SharedHelper.putKey(context, "lname", Response.body().get(0).getLname());
            SharedHelper.putKey(context, "referal_code", Response.body().get(0).getReferal());
            SharedHelper.putKey(context, "emailid", Response.body().get(0).getEmail());
            SharedHelper.putKey(context, "cc", Response.body().get(0).getPhcode());
            SharedHelper.putKey(context, "mobile", Response.body().get(0).getPhone());
            SharedHelper.putKey(context, "language", Response.body().get(0).getLang());
            SharedHelper.putKey(context, "currency", Response.body().get(0).getCur());
            SharedHelper.putKey(context, "card_number", Response.body().get(0).getCard().getLast4());
            SharedHelper.putKey(context, "profile", Response.body().get(1).getProfileurl());
            CommonData.addressList.addAll(Response.body().get(0).getAddress());
            if (Response.body().get(0).getEmgContact() != null && Response.body().get(0).getEmgContact().isEmpty()) {
                SharedHelper.putStatus(context, "isEmergency", false);
            } else {
                SharedHelper.putStatus(context, "isEmergency", true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Intent i = new Intent(context, MainActivity.class);
        startActivity(i);
        getActivity().finish();
    }

    @Override
    public void OnFailure(Response<List<ProfileModel>> Response) {
        try {
            Utiles.showErrorMessage(Response.errorBody().string(), activity, getView());
        } catch (Exception e) {
            e.printStackTrace();
            Utiles.displayMessage(getView(), context, context.getString(R.string.poor_network));
        }

    }


}
