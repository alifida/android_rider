package com.auezeride.app.Fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.ForgetPasswordModel;
import com.auezeride.app.Model.OTPVerificationModel;
import com.auezeride.app.Presenter.ForgetPasswordPresenter;
import com.auezeride.app.R;
import com.auezeride.app.View.ForgetPasswordView;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.auezeride.app.CommonClass.FontChangeCrawler;


import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class ForgotPasswordFragment extends BaseFragment implements ForgetPasswordView, Validator.ValidationListener {

    @BindView(R.id.back_img)
    ImageButton backImg;
    @NotEmpty(message = "")
    @Email(message = "Enter Valid Email ID")
    @BindView(R.id.email_edt)
    MaterialEditText emailEdt;
    @BindView(R.id.submit_btn)
    Button submitBtn;
    Unbinder unbinder;
    @BindView(R.id.password_edt)
    MaterialEditText passwordEdt;
    @BindView(R.id.confirmpassword_edt)
    MaterialEditText confirmpasswordEdt;
    @BindView(R.id.otp_edt)
    MaterialEditText otpEdt;
    @BindView(R.id.change_password_Layout)
    LinearLayout changePasswordLayout;
    @BindView(R.id.enter_email_text)
    TextView enterEmailText;

    public ForgotPasswordFragment() {
    }

    public static String strOTP;

    ForgetPasswordPresenter forgetPasswordPresenter;

    Activity activity;
    Context context;
    private String strStatus ="";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    Validator validator;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        context = getContext();
        validator = new Validator(this);
        validator.setValidationListener(this);
        enterEmailText.setText(activity.getResources().getString(R.string.enter_email_address_you_used_to_reg));
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts((ViewGroup) activity.findViewById(android.R.id.content));
        forgetPasswordPresenter = new ForgetPasswordPresenter(activity, this);
        return view;
    }


    @Override
    public void onDetach() {
        super.onDetach();

    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();
    }

    @OnClick({R.id.back_img, R.id.submit_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_img:
                assert getFragmentManager() != null;
                getFragmentManager().popBackStackImmediate();
                break;
            case R.id.submit_btn:
                if (strStatus.equalsIgnoreCase("Change Password")) {
                    if (Utiles.PasswordValidation(passwordEdt, null, "password")) {
                        Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.enter_your_password_minimum));
                    } else if (Utiles.PasswordValidation(confirmpasswordEdt, null, "password")) {
                        Utiles.displayMessage(getView(), context,activity.getResources().getString(R.string.enter_your_confirm_minimum_ss));
                    } else if (Utiles.PasswordValidation(passwordEdt, confirmpasswordEdt, "match")) {
                        Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.enter_your_valid_confirm_password));
                    } else if (Utiles.PasswordValidation(otpEdt, null, "opt")) {
                        Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.enter_your_opts));
                    } else if (!strOTP.equals(otpEdt.getText().toString())) {
                        Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.enter_valid_otp));
                    } else {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("password", passwordEdt.getText().toString());
                        map.put("confirmpassword", confirmpasswordEdt.getText().toString());
                        map.put("otp", strOTP);
                        map.put("email", emailEdt.getText().toString());
                        forgetPasswordPresenter.OTPVerification(map);
                    }

                } else {
                    validator.validate();
                }
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void OnSuccessfully(Response<ForgetPasswordModel> Response) {
        strStatus="Change Password";
        submitBtn.setText(activity.getResources().getString(R.string.change_passworddd));
        strOTP = Response.body().getOTP();
        enterEmailText.setText(activity.getResources().getString(R.string.enter_new_pass_and_otp_received_in_your_email));
        emailEdt.setVisibility(View.GONE);
        changePasswordLayout.setVisibility(View.VISIBLE);
        Utiles.displayMessage(getView(), context, Response.body().getMessage());
    }

    @Override
    public void OnFailure(Response<ForgetPasswordModel> Response) {
        try {
            String Message = Response.errorBody().string();

            JSONObject jsonObject = new JSONObject(Message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }

        } catch (IOException | JSONException e) {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }

    }

    @Override
    public void OnOtpSuccessfully(Response<OTPVerificationModel> Response) {
        Utiles.displayMessage(getView(), context, Response.body().getMessage());
        getFragmentManager().popBackStackImmediate();
    }

    @Override
    public void OnOtpFailure(Response<OTPVerificationModel> Response) {
        try {
            assert Response.errorBody() != null;
            String Message = Response.errorBody().string();

            JSONObject jsonObject = new JSONObject(Message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getView(), context, jsonObject.optString("message"));
            }

        } catch (IOException | JSONException e) {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }

    }

    @Override
    public void onValidationSucceeded() {
        forgetPasswordPresenter.SendEamilTOGenerateOPT(emailEdt.getText().toString());
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(activity);
            if (view instanceof EditText) {
                ((EditText) view).setError(activity.getResources().getString(R.string.enter_valid_email_id));
            } else {
                Toast.makeText(activity, message, Toast.LENGTH_LONG).show();
            }
        }

    }
}