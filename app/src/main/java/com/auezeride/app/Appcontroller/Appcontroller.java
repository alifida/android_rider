package com.auezeride.app.Appcontroller;

import android.annotation.SuppressLint;
import android.content.Context;
import androidx.multidex.MultiDex;
import androidx.multidex.MultiDexApplication;
import androidx.appcompat.app.AppCompatDelegate;

import com.crashlytics.android.Crashlytics;
import com.crashlytics.android.ndk.CrashlyticsNdk;
import com.google.android.libraries.places.api.Places;

import com.auezeride.app.CommonClass.Constants;
import com.auezeride.app.R;

import io.fabric.sdk.android.Fabric;
import io.github.inflationx.calligraphy3.CalligraphyConfig;
import io.github.inflationx.calligraphy3.CalligraphyInterceptor;
import io.github.inflationx.viewpump.ViewPump;

/**
 * Created by com on 3/4/18.
 */

@SuppressLint("Registered")
public class Appcontroller extends MultiDexApplication {

    private static final String TAG = Appcontroller.class.getSimpleName();
    @SuppressLint("StaticFieldLeak")
    private static Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics(), new CrashlyticsNdk());
        MultiDex.install(this);
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        AppSignatureHelper appSignature = new AppSignatureHelper(this);
        appSignature.getAppSignatures();
        ViewPump.init(ViewPump.builder()
                .addInterceptor(new CalligraphyInterceptor(
                        new CalligraphyConfig.Builder()
                                .setDefaultFontPath(getString(R.string.app_font))
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build());
        context = getApplicationContext();

        if(!Places.isInitialized())
            Places.initialize(this, Constants.GoogleDirectionkey);
    }


    public static Context getContexts() {
        return context;
    }
}
