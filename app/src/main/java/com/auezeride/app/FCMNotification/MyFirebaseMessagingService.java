package com.auezeride.app.FCMNotification;

import android.annotation.SuppressLint;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.auezeride.app.Activity.SplashActivity;
import com.auezeride.app.CommonClass.SharedHelper;

import com.auezeride.app.R;

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);
        System.out.println("enter the onmessage recieve" + remoteMessage);

        if (remoteMessage.getData() != null) {
            Log.d(TAG, "From: " + remoteMessage.getFrom());
            Log.d(TAG, "Notification Message Body: " + remoteMessage.getData());
            //Calling method to generate notification

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                Sendnotification(remoteMessage.getData().get("message"));
            } else {
                sendNotification(remoteMessage.getData().get("message"));
            }

        } else {
            Log.d(TAG, "FCM Notification failed");
        }
    }

    @Override
    public void onNewToken(@NonNull String s) {
        super.onNewToken(s);
        SharedHelper.putToken(getApplicationContext(),"device_token",s);
    }

    private void sendNotification(String messageBody) {
        Intent intent = new Intent(this, SplashActivity.class);
        intent.putExtra("Notification", messageBody);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder;

        if (Build.VERSION.SDK_INT < 26) {
            notificationBuilder = new NotificationCompat.Builder(this);
        } else {
            notificationBuilder = new NotificationCompat.Builder(this, "");
        }

        notificationBuilder.setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                //.setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setPriority(NotificationManager.IMPORTANCE_HIGH)
                .setContentIntent(pendingIntent);

        notificationBuilder.setSmallIcon(getNotificationIcon(notificationBuilder), 1);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());

    }

    private int getNotificationIcon(NotificationCompat.Builder notificationBuilder) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            notificationBuilder.setColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
            return R.mipmap.ic_launcher;
        } else {
            return R.drawable.ic_applogo;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void Sendnotification(String messageBody) {
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        String id = "id_product";
        // The app-visible name of the channel.
        CharSequence name = "Product";
        // The app-visible description of the channel.
        String description = "Notifications regarding our products";
        int importance = NotificationManager.IMPORTANCE_MAX;
        @SuppressLint("WrongConstant")
        NotificationChannel mChannel = new NotificationChannel(id, name, importance);
        // Configure the notification channel.
        mChannel.setDescription(description);
        mChannel.enableLights(true);

        // Sets the notification light color for notifications posted to this
        // channel, if the device supports this feature.
        mChannel.setLightColor(R.color.colorPrimary);
        notificationManager.createNotificationChannel(mChannel);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
/*
        Intent intent1 = new Intent(getApplicationContext(), SplashActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 123, intent1, PendingIntent.FLAG_UPDATE_CURRENT);
*/
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), "id_product")
                .setChannelId(id)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentTitle(getResources().getString(R.string.app_name))
                //.setAutoCancel(true).setContentIntent(pendingIntent)
                .setSound(defaultSoundUri)
                .setContentText(messageBody)
                .setWhen(System.currentTimeMillis());
        notificationBuilder.setSmallIcon(getNotificationIcon(notificationBuilder), 1);
        notificationManager.notify(1, notificationBuilder.build());
    }
}