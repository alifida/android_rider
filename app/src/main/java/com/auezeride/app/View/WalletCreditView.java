package com.auezeride.app.View;

import com.auezeride.app.Model.WalletTransactionCreditModel;

import retrofit2.Response;

public interface WalletCreditView {
    void onTransactionSuccessfully(Response<WalletTransactionCreditModel> Response);

    void onTransactionFailure(Response<WalletTransactionCreditModel> Response);


}
