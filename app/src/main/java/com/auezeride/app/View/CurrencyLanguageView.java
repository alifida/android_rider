package com.auezeride.app.View;

import com.auezeride.app.Model.LanguageCurrencyModel;

import java.util.List;

public interface CurrencyLanguageView {

    void countriesReady(List<LanguageCurrencyModel> LanguageCurrencyModel);
}
