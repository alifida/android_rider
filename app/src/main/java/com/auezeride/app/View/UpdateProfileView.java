package com.auezeride.app.View;

import com.auezeride.app.Model.EditProfileModel;

import retrofit2.Response;

public interface UpdateProfileView {
    void OnSuccessfully(Response<EditProfileModel> Response);
    void OnFailure(Response<EditProfileModel> Response);
}
