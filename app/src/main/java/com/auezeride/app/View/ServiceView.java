package com.auezeride.app.View;

import com.auezeride.app.Model.ServiceModel;

import retrofit2.Response;

public interface ServiceView {
    void OnSuccess(Response<ServiceModel> response);
    void OnFailure(Response<ServiceModel> response);
}
