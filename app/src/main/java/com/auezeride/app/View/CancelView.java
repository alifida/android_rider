package com.auezeride.app.View;

import com.auezeride.app.Model.CancelTripModel;

import retrofit2.Response;

public interface CancelView {
    void OnSuccessfully(Response<CancelTripModel> Response);

    void OnFailure(Response<CancelTripModel> Response);
}
