package com.auezeride.app.View;

import com.auezeride.app.Model.EstimationModel;

import retrofit2.Response;

public interface EstimationView {
    void OnSuccess(Response<EstimationModel> response);

    void OnFailure(Response<EstimationModel> response);
}
