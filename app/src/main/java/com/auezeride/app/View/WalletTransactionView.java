package com.auezeride.app.View;

import com.auezeride.app.Model.AllwalletTransactionmodel;

import retrofit2.Response;

public interface WalletTransactionView {
    void onTransactionSuccessfully(Response<AllwalletTransactionmodel> Response);

    void onTransactionFailure(Response<AllwalletTransactionmodel> Response);


}
