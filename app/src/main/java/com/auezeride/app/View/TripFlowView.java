package com.auezeride.app.View;


import com.auezeride.app.Model.TripFlowModel;

import retrofit2.Response;

public interface TripFlowView {

    void OnTripSuccessfully(Response<TripFlowModel> Response);

    void OnTripFailure(Response<TripFlowModel> Response);
}
