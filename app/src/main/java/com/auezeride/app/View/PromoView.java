package com.auezeride.app.View;

import com.auezeride.app.Model.PromoCodeModel;

import retrofit2.Response;

public interface PromoView {
    void OnSuccessfully(Response<PromoCodeModel> Response);

    void OnFailure(Response<PromoCodeModel> Response);
}
