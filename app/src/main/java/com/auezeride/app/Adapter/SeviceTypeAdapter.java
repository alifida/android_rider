package com.auezeride.app.Adapter;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.R;

import butterknife.ButterKnife;

public class SeviceTypeAdapter extends RecyclerView.Adapter<SeviceTypeAdapter.MyViewHolder> {


    Activity activity;
    public SeviceTypeAdapter(Activity activity) {
        this.activity = activity;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.service_type_adapter, parent, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), activity.getString(R.string.app_font));
        fontChanger.replaceFonts((ViewGroup) activity.findViewById(android.R.id.content));
        return new MyViewHolder(view);

    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 3;
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {


        MyViewHolder(View view) {
            super(view);

            ButterKnife.bind(this, view);
        }
    }
    public interface RemoveContact{
     void  RemoveContact(String contactid, int Position);
    }
}
