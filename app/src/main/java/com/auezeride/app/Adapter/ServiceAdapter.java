package com.auezeride.app.Adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.FlowInterface.CallRequest;
import com.auezeride.app.Model.ServiceModel;

import java.util.List;

import com.auezeride.app.CommonClass.CommonData;
import com.auezeride.app.R;
import com.auezeride.app.Retrofit.RetrofitGenerator;


import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by com on 21-Jun-18.
 */

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.MyViewHolder> {
    private List<ServiceModel.VehicleCategory> serviceModels;
    private Activity activity;
    private boolean clickable = false;
    private int click = 0;
    private SelectedServiceType selectedServiceType;

    public ServiceAdapter(Activity activity, List<ServiceModel.VehicleCategory> serviceModels, SelectedServiceType selectedServiceType) {
        this.activity = activity;
        this.serviceModels = serviceModels;
        this.selectedServiceType = selectedServiceType;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.serviceadapter, parent, false);
        return new ServiceAdapter.MyViewHolder(view);
    }

    @SuppressLint("RecyclerView")
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        System.out.println("enter the vehicle image"+ RetrofitGenerator.imagepath + serviceModels.get(position).getFile());
        holder.txt_car_name.setText(serviceModels.get(position).getType());
        if (serviceModels.get(position).getAvailable()) {
            Glide.with(activity).load(RetrofitGenerator.imagepath + serviceModels.get(position).getFile()).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(false)
                    .override(100, 100)
                    .into(holder.img_cartype);



        } else {
            Glide.with(activity).load(R.drawable.soon).diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(false)
                    .override(100, 100)
                    .into(holder.img_cartype);


        }

        if (!clickable) {
            if (position == 0) {
                holder.img_cartype.setBackground(activity.getResources().getDrawable(R.drawable.ic_select_car));
                CommonData.strServiceType = serviceModels.get(position).getType();
                CommonData.ServiceID = serviceModels.get(position).getId();
                //CommonData.strVehicleCode = serviceModels.get(position).getVehicleTypeCode();
                final int newColor = activity.getResources().getColor(R.color.car_select);
                CommonData.isRiderLater = serviceModels.get(position).getIsRideLater();
           //     holder.img_cartype.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);
                int height = activity.getResources().getDimensionPixelSize(R.dimen._55sdp);
                int width = activity.getResources().getDimensionPixelSize(R.dimen._55sdp);
                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
                holder.img_cartype.setLayoutParams(parms);
                try {
                    CallRequest callRequest = (CallRequest) activity;
                    callRequest.SelectedCategory(CommonData.strServiceType);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                holder.img_cartype.setBackground(activity.getResources().getDrawable(R.drawable.car_bg));
                final int newColor = activity.getResources().getColor(R.color.car_unselect);
              //  holder.img_cartype.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);
                int height = activity.getResources().getDimensionPixelSize(R.dimen._50sdp);
                int width = activity.getResources().getDimensionPixelSize(R.dimen._50sdp);
                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
                holder.img_cartype.setLayoutParams(parms);
            }
        } else {
            if (position == click) {
                holder.img_cartype.setBackground(activity.getResources().getDrawable(R.drawable.ic_select_car));
                final int newColor = activity.getResources().getColor(R.color.car_select);
              //  holder.img_cartype.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);
                int height = activity.getResources().getDimensionPixelSize(R.dimen._55sdp);
                int width = activity.getResources().getDimensionPixelSize(R.dimen._55sdp);
                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
                holder.img_cartype.setLayoutParams(parms);
            } else {
                holder.img_cartype.setBackground(activity.getResources().getDrawable(R.drawable.car_bg));
                final int newColor = activity.getResources().getColor(R.color.car_unselect);
               // holder.img_cartype.setColorFilter(newColor, PorterDuff.Mode.SRC_IN);
                int height = activity.getResources().getDimensionPixelSize(R.dimen._50sdp);
                int width = activity.getResources().getDimensionPixelSize(R.dimen._50sdp);
                LinearLayout.LayoutParams parms = new LinearLayout.LayoutParams(width, height);
                holder.img_cartype.setLayoutParams(parms);
            }
        }


        holder.itemView.setOnClickListener(v -> {
            if (serviceModels.get(position).getAvailable()) {
                CommonData.strServiceType = serviceModels.get(position).getType();
                CommonData.ServiceID = serviceModels.get(position).getId();
                CommonData.isRiderLater = serviceModels.get(position).getIsRideLater();
                //CommonData.strVehicleCode = serviceModels.get(position).getVehicleTypeCode();
                clickable = true;

                if (click == position) {
                    selectedServiceType.SelectService();
                }
                try {
                    CallRequest callRequest = (CallRequest) activity;
                    callRequest.SelectedCategory(CommonData.strServiceType);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                notifyDataSetChanged();
                click = position;

            }else {
                Utiles.displayMessage(activity.getCurrentFocus(), activity, activity.getResources().getString(R.string.coming_sooon));

            }


        });
    }

    @Override
    public int getItemCount() {
        return serviceModels.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.img_service_car)
        ImageView img_cartype;

        @BindView(R.id.txt_car_name)
        TextView txt_car_name;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }

    public interface SelectedServiceType {
        void SelectService();
    }


}
