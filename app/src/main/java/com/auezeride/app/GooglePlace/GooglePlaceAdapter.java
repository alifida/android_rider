package com.auezeride.app.GooglePlace;

import android.app.Activity;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Typeface;
import android.text.style.CharacterStyle;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.libraries.places.api.model.AutocompletePrediction;

import org.jetbrains.annotations.NotNull;

import com.auezeride.app.CommonClass.FontChangeCrawler;


import java.util.List;

import com.auezeride.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GooglePlaceAdapter extends RecyclerView.Adapter<GooglePlaceAdapter.MyViewHolder> {
    private List<AutocompletePrediction> predictions;
    private Activity activity;
    private Callback callback;


    public GooglePlaceAdapter(Activity activity, List<AutocompletePrediction> predictions, Callback callback) {
        this.callback = callback;
        this.activity = activity;
        this.predictions = predictions;

    }

    public void UpdateSearch(List<AutocompletePrediction> predictions){
        this.predictions = predictions;
        notifyDataSetChanged();
    }

    public interface Callback {
        void SelectedAddress(AutocompletePrediction Address);
    }

    @NotNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.googleplaceadapter, parent, false);
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), activity.getString(R.string.app_font));
        fontChanger.replaceFonts((ViewGroup) activity.findViewById(android.R.id.content));
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        CharacterStyle STYLE_BOLD = new StyleSpan(Typeface.BOLD);
        CharacterStyle STYLE_NORMAL = new StyleSpan(Typeface.NORMAL);
        final AutocompletePrediction prediction = predictions.get(position);
        holder.placeName.setText(prediction.getPrimaryText(STYLE_BOLD).toString());
        holder.placeDetail.setText(prediction.getSecondaryText(STYLE_NORMAL).toString());
        holder.addressLayout.setTag(position);
        holder.addressLayout.setOnClickListener(v -> {
            int position1 = (int) v.getTag();
            callback.SelectedAddress(predictions.get(position1));

        });

    }

    @Override
    public int getItemCount() {
        return predictions.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.place_name)
        TextView placeName;
        @BindView(R.id.place_detail)
        TextView placeDetail;

        @BindView(R.id.address_layout)
        LinearLayout addressLayout;

        MyViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
