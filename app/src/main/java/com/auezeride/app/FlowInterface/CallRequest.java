package com.auezeride.app.FlowInterface;

import com.auezeride.app.Model.ServiceModel;
import com.auezeride.app.Model.TripFlowModel;

import java.util.List;

import retrofit2.Response;

public interface CallRequest {
    void callReuest();

    void ClearServiceFragment();

    void CallsummaryFragment();

    void CallEstimationfare();

    void Ridelater();

    void FareDetailFragment(List<ServiceModel> serviceModels);

    void FlowDetails(Response<TripFlowModel> Response);

    void SelectedCategory(String ServiceType);

}
