package com.auezeride.app.TripFlowScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.auezeride.app.CommonClass.SharedHelper;
import com.google.gson.Gson;
import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.CommonClass.CommonData;
import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.FlowInterface.CallRequest;
import com.auezeride.app.Model.ScheduleTripModel;
import com.auezeride.app.Presenter.ScheduleRequestPresenter;

import com.auezeride.app.R;
import com.auezeride.app.View.ScheduleRequestView;

import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.TimeZone;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class RiderLaterFragment extends BaseFragment implements DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, ScheduleRequestView {


    @BindView(R.id.date_txt)
    TextView dateTxt;
    @BindView(R.id.time_txt)
    TextView timeTxt;
    @BindView(R.id.schude_trip)
    Button schudeTrip;
    private Unbinder unbinder;

    private CallRequest callRequest;
    private DatePickerDialog dpd;
    private TimePickerDialog tpd;
    private String strDate = "", strTime = "";

    public RiderLaterFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private Activity activity;
    private Context context;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rider_later, container, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        context = getContext();
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts((ViewGroup) activity.findViewById(android.R.id.content));

        Calendar now = Calendar.getInstance();
        dpd = DatePickerDialog.newInstance(
                this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)


        );
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }


    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        System.out.println("monthofyear=" + monthOfYear);
        String month = "", day = "";
        if (dayOfMonth < 10) {
            day = "0" + dayOfMonth;
            System.out.println("day=" + day);
        } else {
            day = String.valueOf(dayOfMonth);
        }
        if (monthOfYear < 9) {
            month = "0" + (++monthOfYear);
            System.out.println("month=" + month);
        } else {
            month = String.valueOf(++monthOfYear);
        }
        //    String date = dayOfMonth+"-"+(++monthOfYear)+"-"+year;
        String date = day + "-" + month + "-" + year;
        System.out.println("new date of settext==" + date);
        dateTxt.setText(date);
        strDate = date;
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        int hour = hourOfDay;
        int minutes = minute;
        int hours = hourOfDay;
        String timeSet = "";
        if (hours > 12) {
            hour -= 12;
            timeSet = "PM";
        } else if (hours == 0) {
            hour += 12;
            timeSet = "AM";
        } else if (hours == 12)
            timeSet = "PM";
        else
            timeSet = "AM";

        String min = "";
        if (minutes < 10)
            min = "0" + minutes;
        else
            min = String.valueOf(minutes);
        String hr = "";
        if (hour < 10)
            hr = "0" + hour;
        else
            hr = String.valueOf(hour);


        System.out.println("hour====" + hr);

        // Append in a StringBuilder
        String aTime = new StringBuilder().append(hr).append(':')
                .append(min).append(" ").append(timeSet).toString();
        //et1.setText(aTime);

        System.out.println("Current Country==>" + aTime);
        timeTxt.setText(aTime);
        strTime = aTime;
    }

    @OnClick({R.id.date_txt, R.id.time_txt, R.id.schude_trip})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.date_txt:
                Calendar minDate = Calendar.getInstance();
                //  DatePickerDialog datePickerDialog = DatePickerDialog.newInstance(RideLater.this, getcalendar.get(Calendar.YEAR), getcalendar.get(Calendar.MONTH), getcalendar.get(Calendar.DAY_OF_MONTH));
                if (dpd != null && !dpd.isAdded()) {
                    minDate.add(Calendar.DATE, 0);
                    dpd.setMinDate(minDate);
                    minDate.add(Calendar.DATE, 7);
                    dpd.setMaxDate(minDate);
                    assert getFragmentManager() != null;
                    dpd.show(getFragmentManager(), "datePicker");
                }
                break;
            case R.id.time_txt:
                if (dateTxt.getText().toString().equalsIgnoreCase("Set Date")) {
                    Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.please_select_date));
                } else {
                    TimePicker();
                }

                break;
            case R.id.schude_trip:
                if (strDate == null || strDate.isEmpty()) {
                    Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.please_select_date));

                } else if (strTime == null || strTime.isEmpty()) {
                    Utiles.displayMessage(getView(), context,context.getResources().getString(R.string.please_select_time));

                } else {
                    SentScheduleRide();
                }
                break;
        }
    }

    public void SentScheduleRide() {
        TimeZone tz = TimeZone.getDefault();
        System.out.println("TimeZone time  " + tz.getDisplayName(false, TimeZone.SHORT) + " Timezon id :: " + tz.getID());
        HashMap<String, String> map = new HashMap<>();
        map.put("pickupLat", String.valueOf(CommonData.Pickuplat));
        map.put("pickupLng", String.valueOf(CommonData.Pickuplng));
        map.put("dropLat", String.valueOf(CommonData.Droplat));
        map.put("dropLng", String.valueOf(CommonData.Droplng));
        map.put("serviceid", CommonData.ServiceID);
        map.put("pickupAddress", CommonData.strPickupAddress);
        map.put("dropAddress", CommonData.strDropAddresss);
        map.put("serviceType", CommonData.strServiceType);
        map.put("serviceTypeId", Utiles.NullPointer(CommonData.ServiceID));
      /*  map.put("distance", CommonData.strDistance);
        map.put("distanceFare", CommonData.strDistanceFare);
        map.put("basefare", CommonData.strBaseFare);
        map.put("time", CommonData.strTime);*/
        //     map.put("cityLimitCalculation", strCityLimit);
        map.put("vehicleDetailsAndFare", Utiles.returnString(CommonData.strEstimationResponse, false));
        map.put("distanceDetails", Utiles.returnString(CommonData.strEstimationResponse, true));
        if(SharedHelper.getKey(context,"paymentType") != null){
            CommonData.strPaymentType = SharedHelper.getKey(context,"paymentType");
        }else {
            CommonData.strPaymentType = "cash";
        }
        map.put("paymentMode", CommonData.strPaymentType);
        map.put("timeFare", CommonData.strTimeFare);
        map.put("promo", CommonData.strpromocode);
        map.put("pickupCity", CommonData.strPickupCity);
        map.put("requestFrom", "app");
        map.put("bookingType", "rideLater");
        map.put("vehicleTypeCode", Utiles.NullPointer(CommonData.strVehicleCode));
        map.put("utc", tz.getDisplayName(false, TimeZone.SHORT));
        map.put("tripDate", strDate);
        map.put("tripTime", strTime);
        map.put("tripType", "daily");
        ScheduleRequestPresenter scheduleRequestPresenter = new ScheduleRequestPresenter(this);
        scheduleRequestPresenter.setScheduleRequest(map, activity);
    }

    public void TimePicker() {
        Calendar c = Calendar.getInstance(TimeZone.getDefault());
        System.out.println("Current time => " + c.getTime());

        @SuppressLint("SimpleDateFormat")
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c.getTime());
        System.out.println("formattedDate date==" + formattedDate);

        tpd = TimePickerDialog.newInstance(
                this,
                c.get(Calendar.HOUR_OF_DAY),
                c.get(Calendar.MINUTE),
                false
        );
        if (dateTxt.getText().toString().trim().length() == 0) {
            Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.please_select_date));
        } else {
            if (tpd != null && !tpd.isAdded()) {
                if (dateTxt.getText().toString().trim().equals(formattedDate)) {
                    tpd.setMinTime(c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE) + 2,
                            c.get(Calendar.SECOND));
                    assert getFragmentManager() != null;
                    tpd.show(getFragmentManager(), "Timepickerdialog");
                } else {
                    tpd.setMinTime(0, 0,
                            0);
                    assert getFragmentManager() != null;
                    tpd.show(getFragmentManager(), "Timepickerdialog");
                }
            }
        }


    }

    @Override
    public void OnSuccessfully(Response<ScheduleTripModel> Response) {
        try {
            assert Response.body() != null;
            Utiles.displayMessage(getView(), context, Response.body().getMessage());
            callRequest = (CallRequest) getActivity();
            assert callRequest != null;
            callRequest.ClearServiceFragment();

        } catch (Exception e) {
            Log.e("tag", "Eception of request screen" + e.getMessage());
        }
    }

    @Override
    public void OnRequestFailure(Response<ScheduleTripModel> Response) {
        Utiles.showErrorMessage(new Gson().toJson(Response.errorBody()), activity, getView());
    }
}
