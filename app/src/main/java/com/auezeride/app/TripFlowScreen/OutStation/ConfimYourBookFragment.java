package com.auezeride.app.TripFlowScreen.OutStation;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.R;

public class ConfimYourBookFragment extends BaseFragment {


    public ConfimYourBookFragment() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_confim_your_book, container, false);
    }


}
