package com.auezeride.app.TripFlowScreen.OutStation;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.auezeride.app.Adapter.SeviceTypeAdapter;
import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;


public class OutStationFragment extends BaseFragment {

    @BindView(R.id.back_img)
    ImageButton backImg;
    @BindView(R.id.pickup_txt)
    TextView pickupTxt;
    @BindView(R.id.drop_address_txt)
    TextView dropAddressTxt;
    @BindView(R.id.radioonway)
    RadioButton radioonway;
    @BindView(R.id.radioRoundtrip)
    RadioButton radioRoundtrip;
    @BindView(R.id.radiotrip)
    RadioGroup radiotrip;
    @BindView(R.id.leave_on_date_txt)
    TextView leaveOnDateTxt;
    @BindView(R.id.view_return_above)
    View viewReturnAbove;
    @BindView(R.id.return_by_date_txt)
    TextView returnByDateTxt;
    @BindView(R.id.service_recycleview)
    RecyclerView serviceRecycleview;
    Unbinder unbinder;

    Activity activity;
    Context context;
    FragmentManager fragmentManager;

    public OutStationFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_out_station, container, false);
        unbinder = ButterKnife.bind(this, view);

        activity = getActivity();
        context  = getContext();
        fragmentManager = getFragmentManager();
        Setadapter();
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick(R.id.back_img)
    public void onViewClicked() {
        fragmentManager.popBackStackImmediate();
    }

    public void Setadapter() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        serviceRecycleview.setLayoutManager(linearLayoutManager);
        serviceRecycleview.setItemAnimator(new DefaultItemAnimator());
        serviceRecycleview.setHasFixedSize(true);
        SeviceTypeAdapter serviceAdapter = new SeviceTypeAdapter(activity);
        serviceRecycleview.setAdapter(serviceAdapter);
    }
}
