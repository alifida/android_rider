package com.auezeride.app.TripFlowScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.CommonClass.CommonData;
import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.EventBus.RequestStatus;
import com.auezeride.app.FlowInterface.CallRequest;
import com.auezeride.app.Model.EstimationModel;
import com.auezeride.app.Model.RequestModel;
import com.auezeride.app.Presenter.EstimationFarePresenter;
import com.auezeride.app.Presenter.RequestFlowPresenter;
import com.auezeride.app.R;
import com.auezeride.app.View.EstimationView;
import com.auezeride.app.View.SetrequestView;
import com.auezeride.app.CommonClass.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class EstimationFareFragment extends BaseFragment implements EstimationView, SetrequestView {


    @BindView(R.id.base_fare_txt)
    TextView baseFareTxt;
    @BindView(R.id.distance_txt)
    TextView distanceTxt;
    @BindView(R.id.distance_fare_txt)
    TextView distanceFareTxt;
    @BindView(R.id.time_txt)
    TextView timeTxt;
    @BindView(R.id.time_fare_txt)
    TextView timeFareTxt;
    @BindView(R.id.schude_trip)
    Button schudeTrip;
    @BindView(R.id.request_now)
    Button requestNow;
    Unbinder unbinder;

    CallRequest callRequest;
    @BindView(R.id.total_amount_txt)
    TextView totalAmountTxt;
    @BindView(R.id.base_fare_detail_txt)
    TextView baseFareDetailTxt;


    @BindView(R.id.fare_title_txt)
    TextView fareTitleTxt;
    @BindView(R.id.Cancel_fee_txt)
    TextView CancelFeeTxt;
    @BindView(R.id.Discount_fee_txt)
    TextView DiscountFeeTxt;
    @BindView(R.id.cancel_linear_layout)
    LinearLayout cancelLinearLayout;
    @BindView(R.id.discount_linear_layout)
    LinearLayout discountLinearLayout;
    @BindView(R.id.fare_linear_layout)
    LinearLayout fareLinearLayout;
    @BindView(R.id.night_charge_txt)
    TextView nightChargeTxt;
    @BindView(R.id.picku_charge_layout)
    LinearLayout pickuChargeLayout;
    @BindView(R.id.access_fee_layout)
    LinearLayout accessFeeLayout;

    @BindView(R.id.use_wallet)
    CheckBox useWallet;
    @BindView(R.id.time_fare_detail_txt)
    TextView timeFareDetailTxt;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private Activity activity;
    private Context context;

    public EstimationFareFragment() {

    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_estimation_fare, container, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        context = getContext();


        //  Calendar c = Calendar.getInstance();

        /*int Hr24 = c.get(Calendar.HOUR_OF_DAY);
        int Min = c.get(Calendar.MINUTE);
*/
        if (CommonData.isRiderLater) {
            schudeTrip.setVisibility(View.GONE);// hidden by mudassar-nazir for temporary
        } else {
            schudeTrip.setVisibility(View.GONE);
        }

        if (SharedHelper.getKey(context, "wallet_amout") == null || SharedHelper.getKey(context, "wallet_amout").isEmpty() || SharedHelper.getKey(context, "wallet_amout").equalsIgnoreCase("0")) {
            useWallet.setVisibility(View.GONE);
        } else {
            useWallet.setVisibility(View.GONE); // changed for removing wallet
        }
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));
        EstimationFarePresenter estimationFarePresenter = new EstimationFarePresenter(this);
        HashMap<String, String> map = new HashMap<>();
        map.put("pickupLat", String.valueOf(CommonData.Pickuplat));
        map.put("pickupLng", String.valueOf(CommonData.Pickuplng));
        map.put("dropLat", String.valueOf(CommonData.Droplat));
        map.put("dropLng", String.valueOf(CommonData.Droplng));
        map.put("serviceType", CommonData.strServiceType);
        //map.put("time", String.valueOf(Hr24) + ":" + String.valueOf(Min));

        map.put("serviceTypeId", Utiles.NullPointer(CommonData.ServiceID));
        map.put("tripType", "daily");
        map.put("time", "");
        /*  map.put("cityLimitCalculation", strCityLimit);*/
        map.put("promoCode", CommonData.strpromocode);
        map.put("pickupCity", CommonData.strPickupCity);
        estimationFarePresenter.getEstimationFare(map, activity);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();
    }

    @OnClick({R.id.schude_trip, R.id.request_now})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.schude_trip:
                try {
                    if (useWallet.isChecked()) {
                        CommonData.strPaymentType = "Wallet";
                    }

                    callRequest = (CallRequest) getActivity();
                    callRequest.Ridelater();
                } catch (Exception e) {
                    Log.e("tag", "Eception of request screen" + e.getMessage());
                }
                break;
            case R.id.request_now:
                if (useWallet.isChecked()) {
                    CommonData.strPaymentType = "Wallet";
                }
                Constants.CheckRiderStatus = "Processing";
                HashMap<String, String> map = new HashMap<>();
                if(SharedHelper.getKey(context,"paymentType") != null){
                    CommonData.strPaymentType = SharedHelper.getKey(context,"paymentType");
                }else {
                    CommonData.strPaymentType = "cash";
                }
                map.put("paymentMode", CommonData.strPaymentType);
                map.put("vehicleDetailsAndFare", Utiles.returnString(CommonData.strEstimationResponse, false));
                //     map.put("cityLimitCalculation", strCityLimit);
                map.put("distanceDetails", Utiles.returnString(CommonData.strEstimationResponse, true));
                map.put("timeFare", CommonData.strTimeFare);
                map.put("promoAmt", "");
                map.put("serviceType", CommonData.strServiceType);
                map.put("serviceTypeId", Utiles.NullPointer(CommonData.ServiceID));
                map.put("time", "");
                map.put("promo", CommonData.strpromocode);
                map.put("tripTime", "");
                map.put("pickupCity", CommonData.strPickupCity);
                map.put("requestFrom", "app");
                map.put("bookingType", "rideNow");
                map.put("vehicleTypeCode", Utiles.NullPointer(CommonData.strVehicleCode));
                map.put("tripType", "daily");
                RequestFlowPresenter requestFlowPresenter = new RequestFlowPresenter(this);
                requestFlowPresenter.setRequesApi(map, activity);
                break;
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void OnSuccess(Response<EstimationModel> response) {
        CommonData.strEstimationResponse = new Gson().toJson(response.body());
        try {
            assert response.body() != null;
            if (response.body().getSuccess()) {
                try {
                    DiscountFeeTxt.setText("$0.00");
                    if (response.body().getVehicleDetailsAndFare().getFareDetails().getFareType().equalsIgnoreCase("kmrate")) {
                        fareLinearLayout.setVisibility(View.VISIBLE);
                    } else {
                        fareLinearLayout.setVisibility(View.GONE);
                    }
                    if (!response.body().getVehicleDetailsAndFare().getFareDetails().getOldCancellationAmt().equalsIgnoreCase("0")) {
                        cancelLinearLayout.setVisibility(View.VISIBLE);
                    } else {
                        cancelLinearLayout.setVisibility(View.GONE);
                    }
                    if (!response.body().getVehicleDetailsAndFare().getFareDetails().getComisonAmt().equalsIgnoreCase("0")) {
                        discountLinearLayout.setVisibility(View.VISIBLE);
                    } else {
                        discountLinearLayout.setVisibility(View.GONE);
                    }
                    if (response.body().getVehicleDetailsAndFare().getApplyValues().getApplyNightCharge()) {
                        if (response.body().getVehicleDetailsAndFare().getFareDetails().getNightObj().getIsApply()) {
                            nightChargeTxt.setVisibility(View.VISIBLE);
                            nightChargeTxt.setText(response.body().getVehicleDetailsAndFare().getFareDetails().getNightObj().getAlertLable());
                        } else {
                            nightChargeTxt.setVisibility(View.GONE);
                        }
                    } else {
                        nightChargeTxt.setVisibility(View.GONE);
                    }

                    fareTitleTxt.setText("Estimated Fare (Distance " + response.body().getDistanceDetails().getDistanceLable() + ")");
                    CancelFeeTxt.setText("$" + response.body().getVehicleDetailsAndFare().getFareDetails().getOldCancellationAmt());
                    baseFareTxt.setText("$" + response.body().getVehicleDetailsAndFare().getFareDetails().getTax());

                    if(response.body().getVehicleDetailsAndFare().getFareDetails().getDetuctedFare() != null) {
                        DiscountFeeTxt.setText("$" + response.body().getVehicleDetailsAndFare().getFareDetails().getDetuctedFare());
                    }
                    totalAmountTxt.setText("$" + response.body().getVehicleDetailsAndFare().getFareDetails().getTotalFare());
                    distanceFareTxt.setText("$" + response.body().getVehicleDetailsAndFare().getFareDetails().getKMFare());

                    timeFareTxt.setText("$" + response.body().getVehicleDetailsAndFare().getFareDetails().getPickupCharge());
                    baseFareDetailTxt.setText("$" + response.body().getVehicleDetailsAndFare().getFareDetails().getBaseFare());
                    timeFareDetailTxt.setText("$" + response.body().getVehicleDetailsAndFare().getFareDetails().getTravelFare());

                    //isValidation(response.body().getVehicleDetailsAndFare().getFareDetails().getTravelFare(),timeFareDetailTxt);
                   // isValidation(response.body().getVehicleDetailsAndFare().getFareDetails().getBaseFare(),baseFareDetailTxt);
                   // isValidation(response.body().getVehicleDetailsAndFare().getFareDetails().getPickupCharge(),timeFareTxt);

                    // distanceTxt.setText("Distance (" + response.body().getDistanceDetails().getDistanceLable() + ")");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
            }
        } catch (Exception e) {
            Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
            requestNow.setEnabled(false);
            e.printStackTrace();
        }


    }

    @Override
    public void OnFailure(Response<EstimationModel> response) {
        try {
            assert response.errorBody() != null;
            Utiles.showErrorMessage(response.errorBody().string(), activity, getView());
        } catch (Exception e) {
            e.printStackTrace();
            Utiles.displayMessage(getView(), context, context.getString(R.string.poor_network));
        }

    }

    @Override
    public void OnSuccessfully(Response<RequestModel> Response) {
        assert Response.body() != null;
        if (Response.body().getSuccess()) {
            try {
                callRequest = (CallRequest) getActivity();
                assert callRequest != null;
                callRequest.callReuest();
                CommonData.strpromocode = "";
                //CheckRiderStatus = "Processing";
                CommonData.strRequestId = Response.body().getRequestDetails();
            } catch (Exception e) {
                Log.e("tag", "Eception of request screen" + e.getMessage());
            }
        } else {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }

    }

    @Override
    public void OnRequestFailure(Response<RequestModel> Response) {

        try {
            assert Response.errorBody() != null;
            JSONObject errorbody = new JSONObject(Response.errorBody().string());
            System.out.println("enter the server details" + errorbody);
            if (errorbody.has("message")) {
                Utiles.displayMessage(getView(), context, errorbody.optString("message"));
            }
           /* try {
                callRequest = (CallRequest) getActivity();
                callRequest.callReuest();
            } catch (Exception e) {
                Log.e("tag", "Eception of request screen" + e.getMessage());
            }*/
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }

    }

    public void isValidation(String value,View view){
        if(value!=null && !value.equalsIgnoreCase("0"))
            view.setVisibility(View.VISIBLE);
        else
            view.setVisibility(View.GONE);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    @SuppressLint("SetTextI18n")
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void Onmessage(RequestStatus event) {
        try {
            String status = event.getMessage();
            if(status.equalsIgnoreCase("No Vehicle")){
                if(requestNow.isEnabled()){
                    requestNow.setEnabled(false);
                    requestNow.setAlpha(0.5f);
                    requestNow.setText(activity.getResources().getString(R.string.no_vehicle));
                }
            }else {
                if(!requestNow.isEnabled()){
                    requestNow.setEnabled(true);
                    requestNow.setAlpha(1.0f);
                    requestNow.setText(activity.getResources().getString(R.string.confirm_booking));
                }
            }

            Log.d("Tag", "Test status" + status);
        } catch (Exception e) {
            e.printStackTrace();
        }
        EventBus.getDefault().removeStickyEvent(event); // don't forget to remove the sticky event if youre done with it
    }

}
