package com.auezeride.app.TripFlowScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.FlowInterface.CallRequest;
import com.auezeride.app.Model.FeedbackModel;
import com.auezeride.app.Presenter.FeedBackPresenter;
import com.auezeride.app.Presenter.PaypalPresenter;
import com.auezeride.app.R;

import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Objects;

import com.auezeride.app.CommonClass.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class SummaryFragment extends BaseFragment implements PaypalPresenter.paymentView {

    DatabaseReference databaseReference;
    ValueEventListener valueEventListener;

    CallRequest callRequest;
    @BindView(R.id.total_amount_txt)
    TextView totalAmountTxt;
    @BindView(R.id.date_time_txt)
    TextView dateTimeTxt;
    @BindView(R.id.discount_amount_txt)
    TextView discountAmountTxt;
    @BindView(R.id.pickup_txt)
    TextView pickupTxt;
    @BindView(R.id.drop_address_txt)
    TextView dropAddressTxt;
    @BindView(R.id.driver_rating)
    RatingBar driverRating;
    @BindView(R.id.feedback_txt)
    EditText feedbackTxt;
    @BindView(R.id.submit_txt)
    Button submitTxt;
    Unbinder unbinder;
    Context context;
    @BindView(R.id.title_txt)
    TextView titleTxt;
    @BindView(R.id.base_fare_txt)
    TextView baseFareTxt;
    @BindView(R.id.distance_fare_txt)
    TextView distanceFareTxt;
    @BindView(R.id.time_fare_txt)
    TextView timeFareTxt;
    @BindView(R.id.balance_fare_txt)
    TextView balanceFareTxt;
    @BindView(R.id.payment_type_txt)
    TextView paymentTypeTxt;
    @BindView(R.id.detect_fare_txt)
    TextView detectFareTxt;
    @BindView(R.id.distance_txt)
    TextView distanceTxt;
    @BindView(R.id.time_txt)
    TextView timeTxt;
    @BindView(R.id.ordinary)
    LinearLayout ordinary;
    @BindView(R.id.cancellation_layout)
    LinearLayout cancellationLayout;
    @BindView(R.id.view_cancel)
    View viewCancel;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.night_charge_txt)
    TextView nightChargeTxt;
    @BindView(R.id.night_view)
    View nightView;
    @BindView(R.id.Ride_fare_txt)
    TextView RideFareTxt;
    @BindView(R.id.ride_fare_layout)
    LinearLayout rideFareLayout;
    @BindView(R.id.ride_view)
    View rideView;
    @BindView(R.id.waiting_layout)
    LinearLayout waitingLayout;
    @BindView(R.id.pickup_layout)
    LinearLayout pickupLayout;
    @BindView(R.id.taxi_layout)
    LinearLayout taxiLayout;
    @BindView(R.id.WaiingTimt_txt)
    TextView WaiingTimtTxt;
    @BindView(R.id.Waiting_fare_txt)
    TextView WaitingFareTxt;
    @BindView(R.id.bases_fare_txt)
    TextView basesFareTxt;

    public SummaryFragment() {
        // Required empty public constructor
    }

    private String paymentAmount = "0";
    Activity activity;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_summary, container, false);
        unbinder = ButterKnife.bind(this, view);
        activity = getActivity();
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts((ViewGroup) activity.findViewById(android.R.id.content));
        context = getContext();
        titleTxt.setSelected(true);

      /*  Intent intent = new Intent(activity, PayPalService.class);
        intent.putExtra(PayPalService.EXTRA_PAYPAL_CONFIGURATION, Paypal.config);
        activity.startService(intent);
*/
        Calendar c = Calendar.getInstance();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyy hh:mm a"); // 12 hours formate
        String formattedDate = df.format(c.getTime());
        dateTimeTxt.setText(formattedDate);
        FirebaseTripStatus("datetime", formattedDate);
        FareCalculation();
        return view;
    }

    public void FirebaseTripStatus(String key, String value) {
        DatabaseReference Accept = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        HashMap<String, Object> updatestatus = new HashMap<>();
        updatestatus.put(key, value);
        Accept.updateChildren(updatestatus);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();
    }

    @OnClick(R.id.submit_txt)
    public void onViewClicked() {
        try {
            Utiles.hideKeyboard(activity);
                    sentFeedBack();
                    Utiles.ClearFirebase(SharedHelper.getKey(context, "userid"));
                    callRequest = (CallRequest) getActivity();
                    callRequest.ClearServiceFragment();
                    Constants.TripFlowFragmant = null;
                    SharedHelper.putKey(context, "trip_id", "null");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void FareCalculation() {
        databaseReference = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        valueEventListener = databaseReference.addValueEventListener(new ValueEventListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                try {
                    if (dataSnapshot.child("datetime").getValue() != null) {
                        dateTimeTxt.setText(dataSnapshot.child("datetime").getValue().toString());
                    }
                    if (dataSnapshot.child("pickup_address").getValue() != null) {
                        pickupTxt.setText(dataSnapshot.child("pickup_address").getValue().toString());
                    }
                    if (dataSnapshot.child("Drop_address").getValue() != null) {
                        dropAddressTxt.setText(dataSnapshot.child("Drop_address").getValue().toString());
                    }
                    if (dataSnapshot.child("total_fare").getValue() != null) {
                        totalAmountTxt.setText("$" + dataSnapshot.child("total_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("discount").getValue() != null) {
                        discountAmountTxt.setText("$" + dataSnapshot.child("discount").getValue().toString());

                    }
                    if (dataSnapshot.child("convance_fare").getValue() != null) {
                        baseFareTxt.setText("$" + dataSnapshot.child("convance_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("discount").getValue() != null) {
                        discountAmountTxt.setText("$" + dataSnapshot.child("discount").getValue().toString());

                    }
                    if (dataSnapshot.child("time_fare").getValue() != null) {
                        timeFareTxt.setText("$" + dataSnapshot.child("time_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("distance_fare").getValue() != null) {
                        distanceFareTxt.setText("$" + dataSnapshot.child("distance_fare").getValue().toString());
                        RideFareTxt.setText("$" + dataSnapshot.child("distance_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("cancel_fare").getValue() != null) {
                        balanceFareTxt.setText("$" + dataSnapshot.child("cancel_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("pay_type").getValue() != null && !dataSnapshot.child("pay_type").getValue().toString().equalsIgnoreCase("0")) {
                        paymentTypeTxt.setText(dataSnapshot.child("pay_type").getValue().toString());

                    }
                    if (dataSnapshot.child("tax").getValue() != null) {
                        detectFareTxt.setText("$" + dataSnapshot.child("tax").getValue().toString());

                    }
                    if (dataSnapshot.child("distance").getValue() != null) {
                        distanceTxt.setText(dotLimitation(Double.parseDouble(dataSnapshot.child("distance").getValue().toString())) + " KM");

                    }
                    if (dataSnapshot.child("time").getValue() != null) {
                        timeTxt.setText(dataSnapshot.child("time").getValue().toString() + " Min");

                    }
                    if (dataSnapshot.child("time").getValue() != null) {
                        paymentAmount = dataSnapshot.child("ispay").getValue().toString();

                    }
                    if (dataSnapshot.child("isNight").getValue() != null && !Objects.equals(dataSnapshot.child("isNight").getValue(), "0")) {
                        nightChargeTxt.setText(dataSnapshot.child("isNight").getValue().toString());
                        nightChargeTxt.setVisibility(View.VISIBLE);
                        nightView.setVisibility(View.VISIBLE);
                    } else {
                        nightChargeTxt.setVisibility(View.GONE);
                        nightView.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("waitingTime").getValue() != null) {
                        WaiingTimtTxt.setText( dataSnapshot.child("waitingTime").getValue().toString()+" Mint");

                    }
                    if (dataSnapshot.child("waiting_fare").getValue() != null) {
                        WaitingFareTxt.setText("$" + dataSnapshot.child("waiting_fare").getValue().toString());

                    }
                    if (dataSnapshot.child("isTax").getValue() != null && !dataSnapshot.child("isTax").getValue().toString().equalsIgnoreCase("0")) {
                        taxiLayout.setVisibility(View.VISIBLE);

                    } else {
                        taxiLayout.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("isWaiting").getValue() != null && !dataSnapshot.child("isWaiting").getValue().toString().equalsIgnoreCase("0")) {
                        waitingLayout.setVisibility(View.VISIBLE);

                    } else {
                        waitingLayout.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("isPickup").getValue() != null && !dataSnapshot.child("isPickup").getValue().toString().equalsIgnoreCase("0")) {
                        pickupLayout.setVisibility(View.VISIBLE);

                    } else {
                        pickupLayout.setVisibility(View.GONE);
                    }
                    if (dataSnapshot.child("basefare").getValue() != null) {
                        basesFareTxt.setText("$" + dataSnapshot.child("basefare").getValue().toString());

                    }
                    if (dataSnapshot.child("trip_type").getValue() != null) {
                        if (Objects.requireNonNull(dataSnapshot.child("trip_type").getValue()).toString().equalsIgnoreCase("flatrate")) {
                            ordinary.setVisibility(View.GONE);
                            rideFareLayout.setVisibility(View.VISIBLE);
                            rideView.setVisibility(View.VISIBLE);
                        } else {
                            ordinary.setVisibility(View.VISIBLE);
                            rideFareLayout.setVisibility(View.GONE);
                            rideView.setVisibility(View.GONE);
                        }

                    }
                    if (dataSnapshot.child("cancel_fare").getValue() != null) {
                        if (Objects.requireNonNull(dataSnapshot.child("cancel_fare").getValue()).toString().equalsIgnoreCase("0")) {
                            cancellationLayout.setVisibility(View.GONE);
                            viewCancel.setVisibility(View.GONE);
                        } else {
                            cancellationLayout.setVisibility(View.VISIBLE);
                            viewCancel.setVisibility(View.VISIBLE);
                        }

                    }


                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void sentFeedBack() {
        HashMap<String, String> map = new HashMap<>();
        map.put("rating", String.valueOf(driverRating.getRating()));
        map.put("tripId", SharedHelper.getKey(context, "trip_id"));
        map.put("comments", feedbackTxt.getText().toString());
        FeedBackPresenter feedBackPresenter = new FeedBackPresenter();
        feedBackPresenter.getFeedBack(activity, map);

    }

    public String dotLimitation(double values) {
        return new DecimalFormat("##.##").format(values);
    }



    @Override
    public void onSuccesPayment(Response<FeedbackModel> response) {

        try {
            sentFeedBack();
            Utiles.ClearFirebase(SharedHelper.getKey(context, "userid"));
            callRequest = (CallRequest) getActivity();
            callRequest.ClearServiceFragment();
            Constants.TripFlowFragmant = null;
            SharedHelper.putKey(context, "trip_id", "null");
            assert response.body() != null;
            Utiles.CommonToast(activity, response.body().getMessage());
        } catch (Exception e) {
            Log.e("tag", "Eception of request screen" + e.getMessage());
        }

    }

    @Override
    public void OnFailurePayment(Response<FeedbackModel> response) {
        try {
            assert response.errorBody() != null;
            String message = response.errorBody().string();
            Utiles.showErrorMessage(message, activity, getView());
        } catch (IOException e) {
            Utiles.displayMessage(getView(), context, context.getString(R.string.poor_network));
        }

    }
}
