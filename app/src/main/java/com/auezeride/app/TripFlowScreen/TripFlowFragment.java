package com.auezeride.app.TripFlowScreen;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.auezeride.app.CommonClass.BaseFragment;
import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.EventBus.TripStatus;
import com.auezeride.app.FlowInterface.CallRequest;
import com.auezeride.app.Model.CancelTripModel;
import com.auezeride.app.Model.TripFlowModel;
import com.auezeride.app.Presenter.CancelTripPresenter;
import com.auezeride.app.Presenter.TripFlowPresenter;
import com.auezeride.app.R;
import com.auezeride.app.View.CancelView;
import com.auezeride.app.View.TripFlowView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import com.auezeride.app.CommonClass.Constants;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;


public class TripFlowFragment extends BaseFragment implements CancelView, TripFlowView {


    @BindView(R.id.driver_profile_image)
    ImageView driverProfileImage;
    @BindView(R.id.driver_name)
    TextView driverName;
    @BindView(R.id.driver_rating)
    RatingBar driverRating;
    @BindView(R.id.car_category_rating)
    TextView carCategoryRating;
    @BindView(R.id.number_plate_txt)
    TextView numberPlateTxt;
    @BindView(R.id.call_layout)
    LinearLayout callLayout;
    @BindView(R.id.Message_layout)
    LinearLayout MessageLayout;
    @BindView(R.id.trip_cancel_layout)
    LinearLayout tripCancelLayout;
    @BindView(R.id.share_layout)
    LinearLayout shareLayout;
    @BindView(R.id.common_layout)
    LinearLayout commonLayout;
    Unbinder unbinder;
    CallRequest callRequest;
    Response<TripFlowModel> Response;
    @BindView(R.id.otp_txt)
    TextView otpTxt;
    @BindView(R.id.overall_layout)
    LinearLayout overallLayout;
    @BindView(R.id.call_img)
    ImageButton callImg;
    @BindView(R.id.call_btn)
    TextView callBtn;
    @BindView(R.id.message_imgbtn)
    ImageButton messageImgbtn;
    @BindView(R.id.message_txt)
    TextView messageTxt;
    @BindView(R.id.cancel_img)
    ImageButton cancelImg;
    @BindView(R.id.cancel_imgbtn)
    TextView cancelImgbtn;

    public TripFlowFragment() {
        // Required empty public constructor
    }

    LinearLayout overall_layout;
    Activity activity;
    Context context;
    String riderRating;
    AlertDialog cancelAlert;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    String strPhoneNumber;
    View view;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null) {
                parent.removeView(view);
            }

        }
        view = inflater.inflate(R.layout.fragment_trip_flow, container, false);
        unbinder = ButterKnife.bind(this, view);
        overall_layout = view.findViewById(R.id.overall_layout);
        activity = getActivity();
        context = getContext();
        FontChangeCrawler fontChanger = new FontChangeCrawler(activity.getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts(activity.findViewById(android.R.id.content));
        TripFlowPresenter tripFlowPresenter = new TripFlowPresenter(this);
        tripFlowPresenter.TripFlowApi(SharedHelper.getKey(context, "trip_id"), activity);
        return view;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Utiles.clearInstance();
        unbinder.unbind();
        try {
            if (cancelAlert != null && cancelAlert.isShowing()) {
                cancelAlert.dismiss();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @OnClick({R.id.call_layout, R.id.Message_layout, R.id.trip_cancel_layout, R.id.share_layout, R.id.message_txt, R.id.message_imgbtn, R.id.cancel_img, R.id.cancel_imgbtn, R.id.call_img, R.id.call_btn, R.id.share_img, R.id.share_txt})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.call_layout:
                CalltoDriver();
                break;
            case R.id.Message_layout:
                DriverMessage();
                break;
            case R.id.trip_cancel_layout:
                Alertdialog("Are You Sure Wanna Cancel Trip");
                break;
            case R.id.share_layout:
                SocialShare();
                break;
            case R.id.share_txt:
                SocialShare();
                break;
            case R.id.share_img:
                SocialShare();
                break;
            case R.id.call_img:
                CalltoDriver();
                break;
            case R.id.call_btn:
                CalltoDriver();
                break;
            case R.id.message_imgbtn:
                DriverMessage();
                break;
            case R.id.message_txt:
                DriverMessage();
                break;
            case R.id.cancel_img:
                Alertdialog("Are You Sure Wanna Cancel Trip");
                break;
            case R.id.cancel_imgbtn:
                Alertdialog("Are You Sure Wanna Cancel Trip");
                break;
        }
    }

    @Override
    public void OnSuccessfully(Response<CancelTripModel> Response) {
        assert Response.body() != null;
        if (Response.body().getSuccess()) {
            CancelTrip();
            Utiles.ClearFirebase(SharedHelper.getKey(context, "userid"));
            callRequest = (CallRequest) getActivity();
            callRequest.ClearServiceFragment();
            Constants.TripFlowFragmant = null;
            SharedHelper.putKey(context, "trip_id", "null");
        } else {

            Utiles.displayMessage(getView(), context, context.getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void OnFailure(Response<CancelTripModel> Response) {
        try {
            Utiles.showErrorMessage(Response.errorBody().string(), activity, getView());
        } catch (Exception e) {
            e.printStackTrace();
            Utiles.displayMessage(getView(), context, context.getString(R.string.poor_network));
        }
    }

    public void Alertdialog(String Message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle("Cancel Trip");
        builder1.setMessage(Message);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                "Yes",
                (dialog, id) -> {
                    dialog.dismiss();
                    CancelTripPresenter();
                });
        builder1.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        cancelAlert = builder1.create();
        try {
            cancelAlert.show();
        } catch (Exception e) {
            e.printStackTrace();
        }


        /*try {
            callRequest = (CallRequest) getActivity();
            callRequest.CallsummaryFragment();
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public void CancelTripPresenter() {
        CancelTripPresenter cancelTripPresenter = new CancelTripPresenter(this);
        cancelTripPresenter.CancelTrip(SharedHelper.getKey(context, "trip_id"), activity, "");
    }

    public void CancelTrip() {
        DatabaseReference CancelReference = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        HashMap<String, Object> map = new HashMap<>();
        map.put("status", "5");
        map.put("cancelby", "Rider");
        CancelReference.updateChildren(map);

    }


    @SuppressLint("SetTextI18n")
    @Override
    public void OnTripSuccessfully(Response<TripFlowModel> Response) {

        assert Response.body() != null;
        if (Response.body().getSuccess()) {

            try {
                this.Response = Response;
                carCategoryRating.setText(Utiles.NullPointer(Response.body().getServiceType()));
                driverName.setText(Utiles.NullPointer(Response.body().getDriver().get(0).getProfile().getFname()) + "  " + Utiles.NullPointer(Response.body().getDriver().get(0).getProfile().getLname()));
                Utiles.CircleImageView(Response.body().getDriver().get(0).getProfile().getProfileurl(), driverProfileImage, context);
                numberPlateTxt.setText(Utiles.NullPointer(Response.body().getDriver().get(1).getCurrentActiveTaxi().getLicence()));
                strPhoneNumber = Response.body().getDriver().get(0).getProfile().getPhcode() + Response.body().getDriver().get(0).getProfile().getPhone();
                riderRating = Response.body().getDriver().get(0).getProfile().getRating();
                if (riderRating != null) {
                    driverRating.setRating(Float.parseFloat(riderRating));
                }

                SetOtp();
                callRequest = (CallRequest) getActivity();
                assert callRequest != null;
                callRequest.FlowDetails(Response);
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            Utiles.displayMessage(getView(), context, activity.getResources().getString(R.string.something_went_wrong));
        }

    }

    @Override
    public void OnTripFailure(Response<TripFlowModel> Response) {
        try {
            assert Response.errorBody() != null;
            Utiles.showErrorMessage(Response.errorBody().string(), activity, getView());
        } catch (Exception e) {
            e.printStackTrace();
            Utiles.displayMessage(getView(), context, context.getString(R.string.poor_network));
        }
    }

    String tripStatus = "";

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onMessage(TripStatus event) {
        String status = event.getMessage();
        tripStatus = event.getMessage();
        if (status.equalsIgnoreCase("3")) {
            commonLayout.setWeightSum(3f);
            tripCancelLayout.setVisibility(View.GONE);

        } else {
            tripCancelLayout.setVisibility(View.VISIBLE);
            commonLayout.setWeightSum(4f);
        }
        Log.d("Tag", "Test status" + status);
        SetOtp();
        EventBus.getDefault().removeStickyEvent(TripStatus.class); // don't forget to remove the sticky event if youre done with it
    }

    @SuppressLint("SetTextI18n")
    public void SetOtp() {

        if (Response != null) {
            assert Response.body() != null;
            if (!tripStatus.isEmpty() && tripStatus.equalsIgnoreCase("3")) {
                otpTxt.setVisibility(View.GONE);
                otpTxt.setText("OTP : " + Response.body().getEndOTP());
            } else {

                otpTxt.setText("OTP : " + Response.body().getStartOTP());
            }
        }

    }

    public void SocialShare() {
        final String appPackageName = activity.getPackageName(); // getPackageName() from Context or Activity object

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(Intent.EXTRA_TEXT, activity.getResources().getString(R.string.app_name) + " Referal code " + SharedHelper.getKey(context, "referal_code") + "  " + "https://play.google.com/store/apps/details?id=" + appPackageName);
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, activity.getResources().getString(R.string.app_name));
        startActivity(Intent.createChooser(intent, "Share"));

    }

    @Override
    public void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        if (EventBus.getDefault().isRegistered(this))
            EventBus.getDefault().unregister(this);
        super.onStop();
    }


    public void CalltoDriver() {
        if (strPhoneNumber != null && !strPhoneNumber.isEmpty()) {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + strPhoneNumber));
            if (intent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivity(intent);
            }
        } else {
            Toast.makeText(activity, "Number not register", Toast.LENGTH_SHORT).show();
        }

    }

    public void DriverMessage() {
        if (strPhoneNumber != null && !strPhoneNumber.isEmpty()) {
            Intent sendIntent = new Intent(Intent.ACTION_VIEW);
            sendIntent.setData(Uri.parse("sms:" + strPhoneNumber));
            activity.startActivity(sendIntent);
        } else {
            Toast.makeText(activity, "Number not register", Toast.LENGTH_SHORT).show();
        }
    }

}
