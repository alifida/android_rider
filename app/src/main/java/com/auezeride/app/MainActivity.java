package com.auezeride.app;

import android.Manifest;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.akexorcist.googledirection.DirectionCallback;
import com.akexorcist.googledirection.GoogleDirection;
import com.akexorcist.googledirection.constant.TransportMode;
import com.akexorcist.googledirection.model.Direction;
import com.akexorcist.googledirection.util.DirectionConverter;
import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStates;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.maps.android.SphericalUtil;

import com.auezeride.app.Activity.GooglePlaceSearch;
import com.auezeride.app.Activity.ProfileActivity;
import com.auezeride.app.Activity.SetPinLocationActivity;
import com.auezeride.app.Activity.WelcomeActivity;

import com.auezeride.app.CommonClass.CommonData;
import com.auezeride.app.CommonClass.Constants;
import com.auezeride.app.CommonClass.MapAnimator;
import com.auezeride.app.CommonClass.MyAddress;
import com.auezeride.app.CommonClass.PolyUtils;
import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.EventBus.RequestStatus;
import com.auezeride.app.EventBus.TripStatus;
import com.auezeride.app.FlowInterface.CallRequest;
import com.auezeride.app.ForceToUpdate.inApp.InAppUpdateManager;
import com.auezeride.app.Fragment.ContactFragment;
import com.auezeride.app.Fragment.FavoriteLocationFragment;
import com.auezeride.app.Fragment.InviteFriends;
import com.auezeride.app.Fragment.OffersFragment;
import com.auezeride.app.Fragment.PaymentFragment;
import com.auezeride.app.Fragment.SupportFragment;
import com.auezeride.app.Fragment.WalletFragment;
import com.auezeride.app.Fragment.YourTripFragment;
import com.auezeride.app.GooglePlace.GooglePlcaeModel.GeocoderModel;
import com.auezeride.app.Model.AddWalletModel;
import com.auezeride.app.Model.ServiceModel;
import com.auezeride.app.Model.TripFlowModel;
import com.auezeride.app.Model.WalletBalanceModel;
import com.auezeride.app.Navigationdrawer.FragmentDrawer;
import com.auezeride.app.Presenter.GoogleGeocoderPresenter;
import com.auezeride.app.Presenter.LogoutPresenter;
import com.auezeride.app.Presenter.SentEmegencyPresenter;
import com.auezeride.app.Presenter.WalletBalancePresenter;


import com.auezeride.app.Presenter.updateLocationPresenter;

import com.auezeride.app.R;

import com.auezeride.app.TripFlowScreen.CancelFragment;
import com.auezeride.app.TripFlowScreen.EstimationFareFragment;
import com.auezeride.app.TripFlowScreen.FareDetailsFragment;
import com.auezeride.app.TripFlowScreen.RequestFragment;
import com.auezeride.app.TripFlowScreen.RiderLaterFragment;
import com.auezeride.app.TripFlowScreen.ServiceFragment;
import com.auezeride.app.TripFlowScreen.SummaryFragment;
import com.auezeride.app.TripFlowScreen.TripFlowFragment;
import com.auezeride.app.View.GoogleGeoCoderView;
import com.auezeride.app.View.WalletView;

import org.greenrobot.eventbus.EventBus;
import org.reactivestreams.Subscription;

import java.io.IOException;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

import com.auezeride.app.CommonClass.CommonFirebaseListoner;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.inflationx.viewpump.ViewPumpContextWrapper;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener, CallRequest, OnMapReadyCallback, LocationSource, LocationListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, DirectionCallback, GeoQueryEventListener, WalletView, GoogleGeoCoderView, View.OnLongClickListener {
    DrawerLayout mDrawerLayout;
    FragmentDrawer drawerFragment;
    ImageView userProfileImage;
    RelativeLayout logout_layout;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    TextView txtUserName;
    @BindView(R.id.menu_img)
    ImageButton menuImg;
    boolean Tripstartstatus = true;
    Context context = MainActivity.this;
    Activity activity = MainActivity.this;
    AlertDialog logoutAler = null;
    String Tag = "mapactivity";
    static Fragment fragment, ServiceFagment;
    static int GOOGLESEARCHCODE = 0;
    static boolean tripStatus = false;
    @BindView(R.id.pickup_address_txt)
    TextView pickupAddressTxt;
    @BindView(R.id.drop_address_txt)
    TextView dropAddressTxt;
    @BindView(R.id.trip_status)
    TextView TripTitle;
    @BindView(R.id.flow_layout)
    RelativeLayout flowLayout;
    @BindView(R.id.work_imgbtn)
    ImageButton workImgbtn;
    @BindView(R.id.home_imgbtn)
    ImageButton homeImgbtn;
    @BindView(R.id.emegency_imgbtn)
    ImageButton emegencyImgbtn;
    private OnLocationChangedListener mMapLocationListener = null;
    LocationManager locationManager;
    GoogleMap mMap;
    GoogleApiClient mGoogleApiClient;
    Marker pickupmarker, dropmarker;
    Location mCurrentLocation;
    @BindView(R.id.where_to_txt)
    TextView whereToTxt;
    @BindView(R.id.pickup_drop_layout)
    LinearLayout pickupDropLayout;
    @BindView(R.id.currentlocation_imgbtn)
    ImageButton currentlocationImgbtn;
    public static ArrayList<Fragment> fragmentslist = new ArrayList<>();

    private ArrayList<LatLng> listLatLng = new ArrayList<>();

    public static DatabaseReference LastTripDatabase, TripFlowReference, TripFlowCarDataBase;
    static ValueEventListener LastTripValueEvent, TripFlowValue, TripFlowCarValue;
    //Firebase
    GeoQuery geoQuery;
    GeoFire geoFire;
    String carcategory = "auto";


    BitmapDescriptor bitmapDescriptor;
    public Map<String, Marker> markers;

    private float driverBearing = 0.0f, getBearing = 0.0f;


    int count = 0;
    Location filterLocation;
    public static LatLng startLatLng, destLatLng, destinationLatLng, pickupLatLng;
    Polyline mPolyline;
    Marker pickUPrDropMarker, myMarker;
    public static Boolean isFirstTime = true;

    LatLng prevLatLng = new LatLng(0, 0);
    static int flowstatus = 0;
    public TextView wallet_balance_txt;
    public static GoogleGeocoderPresenter googleGeocoderPresenter;
    public static FragmentManager FragmentManage;
    AlertDialog logoutAlert;
    PolyUtils polyUtils;
    private Subscription sendStateSubscription;
    private InAppUpdateManager inAppUpdateManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        findviewById();
        FragmentManage = getSupportFragmentManager();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        pickupAddressTxt.setSelected(true);
        dropAddressTxt.setSelected(true);

        LocationManager lm = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;
        googleGeocoderPresenter = new GoogleGeocoderPresenter(this, compositeDisposable);
        this.markers = new HashMap<>();
        try {
            assert lm != null;
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (!gps_enabled) {

            GPSTurnOnAlert();
        }
        pickupAddressTxt.setOnLongClickListener(this);
        dropAddressTxt.setOnLongClickListener(this);

        if (SharedHelper.getKey(context, "trip_id") != null && !SharedHelper.getKey(context, "trip_id").equalsIgnoreCase("null") && !SharedHelper.getKey(context, "trip_id").isEmpty()) {
            FirebaseTripFlow();
            tripStatus = true;
        } else {
            LastTripStatusCheck();
        }
        Constants.CheckRiderStatus = "";


//        walletPResenter();
        if(!BuildConfig.DEBUG){
            inAppUpdateManager = InAppUpdateManager.Builder(this, 1001);
            inAppUpdateManager.resumeUpdates(true);
            inAppUpdateManager.mode(com.auezeride.app.ForceToUpdate.inApp.Constants.UpdateMode.IMMEDIATE);
            inAppUpdateManager.checkForAppUpdate();
        }

    }

  /*  private void settingStatusBarTransparent() {

        if (Build.VERSION.SDK_INT == Build.VERSION_CODES.KITKAT) {
            Window w = getWindow(); // in Activity's onCreate() for instance
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION, WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
            w.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(Color.BLACK);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            //setStatusBarTranslucent(true);
        }
    }*/

    public void walletPResenter() {
        WalletBalancePresenter walletBalancePresenter = new WalletBalancePresenter(activity, this);
        walletBalancePresenter.getWalletBalance(false);
    }

    public void GPSTurnOnAlert() {

        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(Constants.SET_INTERVAL); //5 seconds
        locationRequest.setFastestInterval(Constants.SET_FASTESTINTERVAL); //3 seconds
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest);
        builder.setAlwaysShow(true); //this is the key ingredient

        PendingResult<LocationSettingsResult> result =
                LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
        result.setResultCallback(result1 -> {
            final Status status = result1.getStatus();
            final LocationSettingsStates state = result1.getLocationSettingsStates();
            switch (status.getStatusCode()) {
                case LocationSettingsStatusCodes.SUCCESS:
                    // All location settings are satisfied. The client can initialize location
                    // requests here.
                    setCurrentlocation();
                    break;
                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:
                    // Location settings are not satisfied. But could be fixed by showing the app
                    // a dialog.
                    try {
                        // Show the dialog by calling startResolutionForResult(),
                        status.startResolutionForResult(activity, Constants.REQUEST_CHECK_SETTINGS);

                    } catch (IntentSender.SendIntentException e) {
                        // Ignore the error.
                    }
                    break;
                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:
                    // Location settings are not satisfied. However, we have no way to fix the
                    // settings so we won't show the dialog.

                    break;
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void findviewById() {

        //map
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        /*Navigation Drawer Layout*/
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerFragment = (FragmentDrawer)
                getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, mDrawerLayout, null);
        drawerFragment.setDrawerListener(this);
        TripTitle.setSelected(true);
        userProfileImage = (ImageView) mDrawerLayout.findViewById(R.id.rider_profile_image);
        txtUserName = (TextView) mDrawerLayout.findViewById(R.id.userName);
        txtUserName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ProfileActivity.class));
            }
        });
        wallet_balance_txt = (TextView) mDrawerLayout.findViewById(R.id.wallet_balance_txt);
        logout_layout = (RelativeLayout) mDrawerLayout.findViewById(R.id.logout_layout);
        logout_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Alertdialog("Are you sure you want to logout ?");
            }
        });

        Utiles.CircleImageView(SharedHelper.getKey(context, "profile"), userProfileImage, context);
        txtUserName.setText(Utiles.NullPointer(SharedHelper.getKey(context, "fname")) + Utiles.NullPointer(SharedHelper.getKey(context, "lname")));
    }

    @Override
    public void onDrawerItemSelected(View view, int position) {
        switch (position) {
            case 0:
                startActivity(new Intent(context, ProfileActivity.class));
                mDrawerLayout.closeDrawer(GravityCompat.START);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

                break;
            case 1:
                fragment = new PaymentFragment();
                moveToFragment(fragment);

                break;
            /*case 2:
                fragment = new WalletFragment();
                moveToFragment(fragment);
                break;*/
            case 2:
                fragment = new YourTripFragment();
                moveToFragment(fragment);

                break;
            case 3:
                fragment = new InviteFriends();
                moveToFragment(fragment);
                break;
            case 4:
                fragment = new ContactFragment();
                moveToFragment(fragment);

                break;
            case 5:
                fragment = new SupportFragment();
                moveToFragment(fragment);

                break;
            case 6:
                fragment = new OffersFragment();
                moveToFragment(fragment);
                break;
            case 7:
                fragment = new FavoriteLocationFragment();
                moveToFragment(fragment);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                break;
            default:
                break;

        }

    }

    private void moveToFragment(Fragment fragment) {
        Reintialize();
        try {
            if (!isFinishing()) {
                runOnUiThread(() -> {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    FragmentManage.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.drawer_layout, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commitAllowingStateLoss();

                });
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void FLowoverlay(Fragment fragment) {
        Reintialize();
        try {
            if (!isFinishing()) {

                runOnUiThread(() -> {
                    FragmentManage.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.container, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commitAllowingStateLoss();
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                });

            } else {
                fragmenAddissue(fragment);
            }
        } catch (Exception e) {
            e.printStackTrace();
            fragmenAddissue(fragment);
        }


    }

    public void fragmenAddissue(Fragment fragment) {
        if (fragment instanceof RequestFragment) {
            FLowoverlay(fragment);
        }

    }

    int countfragment = 0;

    private void ServiceFow(Fragment fragment) {
        Reintialize();
        try {
            if (!isFinishing()) {
                runOnUiThread(() -> {
                    mDrawerLayout.closeDrawer(GravityCompat.START);
                    mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                    FragmentManage.beginTransaction().setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                            .replace(R.id.Servicecontainter, fragment, fragment.getClass().getSimpleName()).addToBackStack(null).commitAllowingStateLoss();

                });
                countfragment = 0;
            } else {
                if (countfragment < 3) {
                    countfragment++;
                    ServiceFow(fragment);
                } else {
                    Constants.TripFlowFragmant = null;
                }

            }
        } catch (Exception e) {
            if (countfragment < 3) {
                countfragment++;
                ServiceFow(fragment);
            } else {
                Constants.TripFlowFragmant = null;
            }
            e.printStackTrace();
        }

    }


    @OnClick(R.id.menu_img)
    public void onViewClicked() {
      /*  Drawable fDraw = menuImg.getBackground();
        Drawable sDraw = activity.getResources().getDrawable(R.drawable.ic_back_layer);
        if (fDraw != null) {
            if (fDraw.getState().equals(sDraw.getState())) {

                super.onBackPressed();

                if (!fragmentslist.isEmpty()) {
                    RemoveFragment(fragmentslist.get(fragmentslist.size() - 1));
                    fragmentslist.remove(fragmentslist.get(fragmentslist.size() - 1));
                }
                if (fragmentslist.size() == 0) {
                    flowstatus = 0;
                    RemovePolyline();
                    menuImg.setBackground(getResources().getDrawable(R.drawable.ic_menu_layer));
                    NormalMode();
                    setCurrentlocation();
                    if (mCurrentLocation != null) {
                        Geofire(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), "basic");

                    }
                    CheckFavoriteLocation();
                }
            } else {
                mDrawerLayout.openDrawer(Gravity.START);

            }

        }*/
        if (!fragmentslist.isEmpty() && fragmentslist.size() != 0) {
            if (!fragmentslist.isEmpty()) {
                RemoveFragment(fragmentslist.get(fragmentslist.size() - 1));
                fragmentslist.remove(fragmentslist.get(fragmentslist.size() - 1));
            }

            if (fragmentslist.size() == 0) {
                flowstatus = 0;
                stopAnim();
                menuImg.setImageResource(R.drawable.ic_menu_layer);
                NormalMode();
                setCurrentlocation();
                if (mCurrentLocation != null) {
                    Geofire(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), "auto");
                }
                CheckFavoriteLocation();
            }
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);

        }

    }

    @Override
    public void onBackPressed() {
        if (Constants.TripFlowFragmant == null) {
            super.onBackPressed();
        }
        if (!fragmentslist.isEmpty()) {
            RemoveFragment(fragmentslist.get(fragmentslist.size() - 1));
            fragmentslist.remove(fragmentslist.get(fragmentslist.size() - 1));
        }
        if (fragmentslist.size() == 0 && !tripStatus) {
            flowstatus = 0;
            RemovePolyline();
            NormalMode();
            stopAnim();
            setCurrentlocation();
            menuImg.setImageResource(R.drawable.ic_menu_layer);
            if (mCurrentLocation != null) {
                Geofire(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), "auto");
            }
            CheckFavoriteLocation();
        }
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);

    }

    public void RemovePolyline() {
        // stopAnim();
   /*     if (pickupmarker != null) {
            pickupmarker.remove();
        }
        if (dropmarker != null) {
            dropmarker.remove();
        }*/

    }

    public void RemoveflowPolyline() {
        if (mPolyline != null) {
            mPolyline.remove();
            mPolyline = null;
        }
        carcategory = "auto";
        if (myMarker != null) {
            myMarker.remove();
        }
    }

    @Override
    public void callReuest() {
        removeAllFragments(FragmentManage);
        Constants.TripFlowFragmant = new RequestFragment();
        FLowoverlay(new RequestFragment());
        Constants.CheckRiderStatus = "Processing";
        if (LastTripDatabase == null) {
            LastTripStatusCheck();
        }

        //  Clearfragmen();


    }

    @Override
    public void ClearServiceFragment() {
        flowLayout.setVisibility(View.GONE);
        flowstatus = 0;
        stopAnim();
        menuImg.setImageResource(R.drawable.ic_menu_layer);
        if (polyUtils != null) {
            polyUtils.clrearAll();
        }
        NormalMode();

        CheckFavoriteLocation();
        removeAllFragments(FragmentManage);
        setCurrentlocation();
        // Clearfragmen();
        RemoveFragment(Constants.TripFlowFragmant);
        fragmentslist.clear();


    }

    private void removeAllFragments(FragmentManager fragmentManager) {
        Reintialize();
        runOnUiThread(() -> {
            if (fragmentManager != null) {
                try {
                    while (fragmentManager.getBackStackEntryCount() > 0) {
                        fragmentManager.popBackStackImmediate();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


    }

    private void Clearfragmen() {
        System.out.println("enter the fragment list" + fragmentslist.size());
        if (!isFinishing()) {
            if (!fragmentslist.isEmpty()) {
                for (Fragment fragment : fragmentslist) {
                    RemoveFragment(fragment);
                    fragmentslist.remove(fragment);
                }
            }
        }


    }

    @Override
    public void CallsummaryFragment() {

        FLowoverlay(new CancelFragment());
    }

    @Override
    public void CallEstimationfare() {
        // RemoveFragment(ServiceFagment);
        ServiceFagment = new EstimationFareFragment();
        fragmentslist.add(ServiceFagment);
        FLowoverlay(ServiceFagment);
    }

    public void CheckFavoriteLocation() {
        if (flowstatus == 0) {
            if (Utiles.isNull(SharedHelper.getKey(context, "homeaddress"))) {
                homeImgbtn.setVisibility(View.VISIBLE);
            } else {
                homeImgbtn.setVisibility(View.GONE);

            }
            if (Utiles.isNull(SharedHelper.getKey(context, "workaddress"))) {
                workImgbtn.setVisibility(View.VISIBLE);
            } else {
                workImgbtn.setVisibility(View.GONE);

            }

        } else {
            homeImgbtn.setVisibility(View.GONE);
            workImgbtn.setVisibility(View.GONE);
        }


    }


    @Override
    public void Ridelater() {
        // RemoveFragment(ServiceFagment);
        ServiceFagment = new RiderLaterFragment();
        fragmentslist.add(ServiceFagment);
        FLowoverlay(ServiceFagment);
    }

    @Override
    public void FareDetailFragment(List<ServiceModel> serviceModels) {
        ServiceFagment = new FareDetailsFragment();
        fragmentslist.add(ServiceFagment);
        Bundle bundle = new Bundle();
        bundle.putSerializable("serviceModel", (Serializable) serviceModels);
        ServiceFagment.setArguments(bundle);
        FLowoverlay(ServiceFagment);
    }

    @Override
    public void FlowDetails(Response<TripFlowModel> Response) {

        pickupLatLng = new LatLng(Double.parseDouble(Response.body().getPickupdetails().getStartcoords().get(1)), Double.parseDouble(Response.body().getPickupdetails().getStartcoords().get(0)));
        destinationLatLng = new LatLng(Double.parseDouble(Response.body().getPickupdetails().getEndcoords().get(1)), Double.parseDouble(Response.body().getPickupdetails().getEndcoords().get(0)));
        destLatLng = destinationLatLng;

        CommonData.Pickuplat = pickupLatLng.latitude;
        CommonData.Pickuplng = pickupLatLng.longitude;

        CommonData.Droplat = destinationLatLng.latitude;
        CommonData.Droplng = destinationLatLng.longitude;
        CommonData.strPickupAddress = Response.body().getPickupdetails().getStart();
        CommonData.strDropAddresss = Response.body().getPickupdetails().getEnd();

        pickupAddressTxt.setText(Response.body().getPickupdetails().getStart());
        dropAddressTxt.setText(Response.body().getPickupdetails().getEnd());
        updateMarker(Response.body().getServiceType().toLowerCase());
    }

    @Override
    public void SelectedCategory(String ServiceType) {
        RemoveCarMarker();
        if (mCurrentLocation != null) {
            Geofire(new LatLng(CommonData.Pickuplat, CommonData.Pickuplng), ServiceType.toLowerCase());
        }
        carcategory = ServiceType.toLowerCase();
        EventBus.getDefault().postSticky(new RequestStatus("No Vehicle"));
    }

    private void updateMarker(String category) {
        if (SharedHelper.getKey(context, "driver_id") != null) {
            TripFlowCarDataBase = FirebaseDatabase.getInstance().getReference().child("drivers_location").child("trip_location").child(SharedHelper.getKey(context, "driver_id"));
            TripFlowCarValue = TripFlowCarDataBase.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        String drivBearing;
                        if (dataSnapshot.child("bearing").getValue() != null)
                            drivBearing = Objects.requireNonNull(dataSnapshot.child("bearing").getValue()).toString(); //get bearing child
                        else
                            drivBearing = "0";

                        String status = Objects.requireNonNull(dataSnapshot.child("l").getValue()).toString(); //get location child
                        System.out.println("Status==>" + status);
                        System.out.println("Bearing" + status);
                        if (drivBearing != null) {
                            if (isFloat(drivBearing)) {
                                getBearing = Float.parseFloat(drivBearing);
                            } else {
                                getBearing = (float) Integer.parseInt(drivBearing);
                            }
                        }
                        String[] lat1ong = status.split(",");

                        System.out.println("length of the latlong==>" + lat1ong.length);

                        String latitude = lat1ong[0];
                        String longitude = lat1ong[1];


                        String latreplace = latitude.replaceAll("\\[", "");
                        String longreplace = longitude.replaceAll("\\]", "");
                        Double laat = Double.parseDouble(latreplace);
                        Double lngg = Double.parseDouble(longreplace);


                        LatLng driverLocation = new LatLng(laat, lngg);
                        if (isFirstTime) {
                            startLatLng = driverLocation;
                            DrawPolyline();

                        }

                        if (startLatLng != null && destLatLng != null && mMap != null) {
                            startLatLng = driverLocation;
                            onDirectionSuccessPlaceMarker();
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }
    }

    public void DrawPolyline() {
        if (Constants.FlowStatus.equals("1") || Constants.FlowStatus.equals("2")) {
            if (startLatLng != null && pickupLatLng != null) {
                isFirstTime = false;
                try {
                    GoogleDirection.withServerKey(Constants.GoogleDirectionkey)
                            .from(startLatLng)
                            .to(pickupLatLng)
                            .transportMode(TransportMode.DRIVING)
                            .execute(MainActivity.this);

                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

        } else {

            if (startLatLng != null && destinationLatLng != null) {
                isFirstTime = false;
                try {
                    GoogleDirection.withServerKey(Constants.GoogleDirectionkey)
                            .from(startLatLng)
                            .to(destinationLatLng)
                            .transportMode(TransportMode.DRIVING)
                            .execute(MainActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }


        }
    }

    @OnClick({R.id.where_to_txt, R.id.pickup_address_txt, R.id.drop_address_txt,R.id.emegency_imgbtn})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.where_to_txt:
                if (getFusedLocation() != null && CommonData.Pickuplat == 0.0) {
                    CommonData.Pickuplat = getFusedLocation().getLatitude();
                    CommonData.Pickuplng = getFusedLocation().getLongitude();
                }
                Intent intent = new Intent(context, GooglePlaceSearch.class);
                startActivityForResult(intent, GOOGLESEARCHCODE);
                break;
            case R.id.pickup_address_txt:
             /*   GOtoFavoriteLocation();
                isPickup = true;*/
                break;
            case R.id.drop_address_txt:
                Intent intent2 = new Intent(this, GooglePlaceSearch.class);
                intent2.putExtra("setpin", "setpin");
                startActivityForResult(intent2, 500);
                break;
            case R.id.emegency_imgbtn:
                SentEmegencyPresenter emegencyPresenter = new SentEmegencyPresenter();
                emegencyPresenter.getsentEmercency(activity,SharedHelper.getKey(context, "trip_id"));
                //Utiles.CommonToast(activity,"Testing going on..");
                break;
        }


    }

    public void GOtoFavoriteLocation() {
        CommonData.HeaderTitle = "favorite";
        Intent intents = new Intent(context, SetPinLocationActivity.class);
        startActivityForResult(intents, GOOGLESEARCHCODE);
    }

    public void RemoveFragment(Fragment fragment) {
        try {
            if (!isFinishing()) {
                runOnUiThread(() -> {
                    if (fragment != null) {
                        FragmentManage.beginTransaction()
                                .remove(fragment)
                                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                                .commitAllowingStateLoss();
                    }
                });

            }
        } catch (Exception e) {
            Log.i("tag", Objects.requireNonNull(e.getMessage()));

        }

    }

    @Override
    public void onLocationChanged(Location location) {

        if (mCurrentLocation == null && location != null) {
            mCurrentLocation = location;
            CommonData.CurrentLocation = location;
            if (mCurrentLocation != null) {
                Geofire(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), "auto");
            }
            getAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
            LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

            System.out.println("INSIDE LOCAION CHANGE" + mCurrentLocation.getLatitude() + mCurrentLocation.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)                              // Sets the center of the map to current location
                    .zoom(Constants.MAP_ZOOM_SIZE)
                    .tilt(0)                                     // Sets the tilt of the camera to 0 degrees
                    .build();

            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        if (mMapLocationListener != null) {
            mMapLocationListener.onLocationChanged(location);
        }
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the app grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        final Location mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null && count == 0) {
            System.out.println("The Last Known Location " + mLastLocation);
            filterLocation = mLastLocation;
            count = count + 1;
            float zoomPosition = Constants.MAP_ZOOM_SIZE_ONTRIP;


            if (!tripStatus) {

                zoomPosition = Constants.MAP_ZOOM_SIZE;
            }
            if (mMap != null) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                        new LatLng(mLastLocation.getLatitude(),
                                mLastLocation.getLongitude()),
                        zoomPosition));
            }

        }
        getFusedLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void activate(OnLocationChangedListener onLocationChangedListener) {
        mMapLocationListener = onLocationChangedListener;
    }

    @Override
    public void deactivate() {
        mMapLocationListener = null;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the app grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        setCurrentlocation();
        try {
          /*  MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                    this, R.raw.maps_style);
            googleMap.setMapStyle(style);*/

        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        mMap.setOnMarkerClickListener(marker -> {
            if (marker.getTag() != null) {
                switch (marker.getTag().toString()) {
                    case "pickup":
                        CommonData.HeaderTitle = "pickup";
                        startActivityForResult(new Intent(getApplicationContext(), SetPinLocationActivity.class), 101);
                        break;
                    case "drop":
                        CommonData.HeaderTitle = "drop";
                        startActivityForResult(new Intent(getApplicationContext(), SetPinLocationActivity.class), 102);
                        break;
                }

            }
            return false;
        });
        bitmapDescriptor = BitmapDescriptorFactory.fromResource(R.mipmap.ic_auto);

    }

    public void setCurrentlocation() {
        mCurrentLocation = getFusedLocation();
        if (mCurrentLocation != null) {
            Geofire(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), "auto");
            getAddress(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()));
            LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

            System.out.println("INSIDE LOCAION CHANGE" + mCurrentLocation.getLatitude() + mCurrentLocation.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)                              // Sets the center of the map to current location
                    .zoom(Constants.MAP_ZOOM_SIZE)
                    .tilt(0)                                     // Sets the tilt of the camera to 0 degrees
                    .build();

            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        }
    }

    public Location getFusedLocation() {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the app grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return null;
        }

        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        System.out.println("Location Provoider:" + " Fused Location");

        if (mCurrentLocation == null) {

            locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            System.out.println("Location Provoider:" + " Fused Location Fail: GPS Location");

            if (locationManager != null) {

                //To avoid duplicate listener
                try {
                    locationManager.removeUpdates(this);
                    System.out.print("remove location listener success");
                } catch (Exception e) {
                    e.printStackTrace();
                    System.out.print("remove location listener failed");
                }

                locationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER,
                        Constants.MIN_TIME_BW_UPDATES,
                        Constants.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                    mCurrentLocation = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                }

                if (mCurrentLocation == null) {

                    System.out.println("Location Provoider:" + " GPS Location Fail: Network Location");

                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            Constants.MIN_TIME_BW_UPDATES,
                            Constants.MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)) {
                        mCurrentLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
            }
        }
        CommonData.CurrentLocation = mCurrentLocation;
        return mCurrentLocation;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        if (compositeDisposable != null) {
            compositeDisposable.clear();
        }

        try {
            Utiles.compositeClreate(compositeDisposable);
            Utiles.clearInstance();
            if (logoutAlert != null && logoutAlert.isShowing()) {
                logoutAlert.dismiss();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @OnClick(R.id.currentlocation_imgbtn)
    public void onViewClicklocation() {
        mCurrentLocation = getFusedLocation();
        if (mCurrentLocation != null) {

            LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());

            System.out.println("INSIDE LOCAION CHANGE" + mCurrentLocation.getLatitude() + mCurrentLocation.getLongitude());

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(latLng)                              // Sets the center of the map to current location
                    .zoom(Constants.MAP_ZOOM_SIZE)
                    .tilt(0)                                     // Sets the tilt of the camera to 0 degrees
                    .build();

            mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
    }

    public void getAddress(final LatLng LatLng) {
        try {
            CommonData.Pickuplat = LatLng.latitude;
            CommonData.Pickuplng = LatLng.longitude;
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());
            addresses = geocoder.getFromLocation(LatLng.latitude, LatLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            CommonData.strPickupAddress = address;
            for (Address addresss: addresses){
                CommonData.strCountryCode =addresss.getCountryCode();
                if(CommonData.strCountryCode!=null && !CommonData.strCountryCode.isEmpty()){
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            googleGeocoderPresenter.getAddressFromLocation(LatLng);

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case 0:
                if (resultCode == Activity.RESULT_OK) {
                    StartRequestFlow(true);

                }
                break;
            case 101:
                if (resultCode == Activity.RESULT_OK) {
                    StartRequestFlow(false);
                }

                break;
            case 102:
                if (resultCode == Activity.RESULT_OK) {
                    StartRequestFlow(false);
                }

                break;
            case 500:
                if (resultCode == Activity.RESULT_OK) {
                    Bundle b = data.getExtras();
                    LatLng latLng = new LatLng(b.getDouble("droplat"), b.getDouble("droplng"));
                    CommonData.Droplat = latLng.latitude;
                    CommonData.Droplng = latLng.longitude;
                    dropAddressTxt.setText(CommonData.strSetpinAddress);
                    destinationLatLng = latLng;
                    String droplatlng = String.valueOf(CommonData.Droplat) + "," + String.valueOf(CommonData.Droplng);
                    DatabaseReference trips_data = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
                    HashMap<String, Object> createTrip = new HashMap<>();
                    createTrip.put("Drop_address", CommonData.strSetpinAddress);
                    createTrip.put("Drop_latlng", droplatlng);
                    trips_data.updateChildren(createTrip);
                    DrawPolyline();
                    AsyncTask.execute(() -> {
                        updateLocationPresenter updateLocation = new updateLocationPresenter();
                        HashMap<String, String> map = new HashMap<>();
                        map.put("trip_id", SharedHelper.getKey(context, "trip_id"));
                        map.put("dropLat", String.valueOf(CommonData.Droplat));
                        map.put("dropLng", String.valueOf(CommonData.Droplng));
                        updateLocation.getUpdateDestinationLocation(activity, map);
                    });
                }

                break;
            case 1001:
                if (resultCode == Activity.RESULT_CANCELED) {
                    inAppUpdateManager.checkForAppUpdate();
                }
                break;
        }

    }


    public void StartRequestFlow(Boolean isFirstTime) {
        if (isFirstTime) {
            if (fragmentslist != null && !fragmentslist.isEmpty()) {
                fragmentslist.clear();
            }
        }
        double distanceInKiloMeters = (LatLngToLocation("A", CommonData.Pickuplat, CommonData.Pickuplng).distanceTo(LatLngToLocation("A", CommonData.Droplat, CommonData.Droplng))) / 1000; // as distance is in meter

     /*   if (distanceInKiloMeters <= 0.5) {
            Utiles.displayMessage(getCurrentFocus(), context, "Please set The Request  above 500 meters");

        } else {*/
        if (getCountryCode(context, CommonData.Pickuplat, CommonData.Pickuplng).equalsIgnoreCase(getCountryCode(context, CommonData.Droplat, CommonData.Droplng))) {
            flowstatus = 1;
            if (isFirstTime) {
                ServiceFagment = new ServiceFragment();
                fragmentslist.add(ServiceFagment);
                ServiceFow(ServiceFagment);
            }
            CheckTheRadius(new LatLng(CommonData.Pickuplat, CommonData.Pickuplng), new LatLng(CommonData.Droplat, CommonData.Droplng));
            setGoogleDirection(new LatLng(CommonData.Pickuplat, CommonData.Pickuplng), new LatLng(CommonData.Droplat, CommonData.Droplng));
            //   pickupmarker = mMap.addMarker(new MarkerOptions().position(new LatLng(CommonData.Pickuplat, CommonData.Pickuplng)).icon(BitmapDescriptorFactory.fromResource(R.drawable.pickup_marker)));
                 /*   CustomInfoWindowAdapter adapter = new CustomInfoWindowAdapter(MainActivity.this);
                    mMap.setInfoWindowAdapter(adapter);*/
            //   dropmarker = mMap.addMarker(new MarkerOptions().position(new LatLng(CommonData.Droplat, CommonData.Droplng)).icon(BitmapDescriptorFactory.fromResource(R.drawable.drop_marker)));
            FlowMode(false);
            RemoveCarMarker();
            pickupAddressTxt.setText(CommonData.strPickupAddress);
            dropAddressTxt.setText(CommonData.strDropAddresss);
            menuImg.setImageResource(R.drawable.ic_back_layer);
            CheckFavoriteLocation();
        } else {
            Utiles.displayMessage(getCurrentFocus(), context, activity.getResources().getString(R.string.please_set_the_valid_destionation));
        }
        /* }
         */
    }

    public Location LatLngToLocation(String locatin, Double lat, Double lng) {
        final Location location = new Location(locatin);
        location.setLatitude(lat);
        location.setLongitude(lng);
        return location;
    }


    public void CheckTheRadius(LatLng pickupLatLng, LatLng destinationLatLng) {
        SphericalUtil.computeDistanceBetween(pickupLatLng, destinationLatLng);
        System.out.println("enter the radius" + SphericalUtil.computeDistanceBetween(pickupLatLng, destinationLatLng));
    }

    public void RemoveCarMarker() {
        for (Marker marker : markers.values()) {
            marker.remove();
        }
        markers.clear();
    }

    public void FlowMode(boolean starttrip) {
        whereToTxt.setVisibility(View.GONE);
        if (starttrip) {
            pickupDropLayout.setVisibility(View.VISIBLE);
        } else {
            pickupDropLayout.setVisibility(View.GONE);
        }

    }

    @Override
    public void onDirectionSuccess(Direction direction, String rawBody) {
        Log.e("tage", "on direction status" + direction.getStatus());
        if (direction.isOK()) {
            ArrayList<LatLng> directionPositionList = direction.getRouteList().get(0).getLegList().get(0).getDirectionPoint();

            if (flowstatus != 0) {
                if (!tripStatus) {
                    if (listLatLng != null && !listLatLng.isEmpty()) {
                        listLatLng.clear();
                    }
                    try {
                        assert listLatLng != null;
                        listLatLng.addAll(directionPositionList);
                        if (polyUtils != null) {
                            polyUtils.clrearAll();
                            polyUtils = new PolyUtils(this, mMap, listLatLng);
                            polyUtils.setSourceAddress(new MyAddress(CommonData.strPickupAddress, direction.getRouteList().get(0).getLegList().get(0).getDuration().getText()));
                            polyUtils.setDestinationAddress(new MyAddress(CommonData.strDropAddresss, null));
                            polyUtils.start();
                            Geofire(new LatLng(CommonData.Pickuplat, CommonData.Pickuplng), "auto");
                        } else {
                            polyUtils = new PolyUtils(this, mMap, listLatLng);
                            polyUtils.setSourceAddress(new MyAddress(CommonData.strPickupAddress, direction.getRouteList().get(0).getLegList().get(0).getDuration().getText()));
                            polyUtils.setDestinationAddress(new MyAddress(CommonData.strDropAddresss, null));
                            polyUtils.start();
                            Geofire(new LatLng(CommonData.Pickuplat, CommonData.Pickuplng), "auto");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    if (destLatLng == null) {
                        if (filterLocation != null)
                            destLatLng = new LatLng(filterLocation.getLatitude(), filterLocation.getLongitude());
                    }

                    if (mPolyline == null) {

                        mPolyline = mMap.addPolyline(DirectionConverter.createPolyline(this, directionPositionList, 5, Color.BLUE));
                    } else {

                        mPolyline.setPoints(directionPositionList);
                    }

                }

            } else {
                stopAnim();
            }

        }

    }

    public void onDirectionSuccessPlaceMarker() {
        if (Constants.FlowStatus.equalsIgnoreCase("3")) {

            final CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(startLatLng)      // Sets the center of the map to Mountain View
                    .zoom(Constants.MAP_ZOOM_SIZE_ONTRIP)      // Sets the zoom
                    .bearing(getBearing)                // Sets the orientation of the camera to east
                    .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }

        LatLng curPos = new LatLng(startLatLng.latitude, startLatLng.longitude);

        if (curPos.latitude != 0 && curPos.longitude != 0) {

            zoomCameraToPosition(curPos);
        }

        if (myMarker == null) {

            myMarker = mMap.addMarker(new MarkerOptions().position(startLatLng).icon(bitmapDescriptor).flat(true));
            myMarker.setAnchor(0.5f, 0.5f);
            myMarker.setRotation(getBearing);

        } else {
            if (prevLatLng != new LatLng(0, 0)) {

                if (!prevLatLng.equals(startLatLng)) {

                    double[] startValues = new double[]{prevLatLng.latitude, prevLatLng.longitude};
                    double[] endValues = new double[]{startLatLng.latitude, startLatLng.longitude};

                    this.animateMarkerTo(myMarker, startValues, endValues, getBearing);

                } else {
                    myMarker.setRotation(getBearing);
                }
            } else {
                myMarker.setPosition(startLatLng);
                myMarker.setRotation(getBearing);
            }


            prevLatLng = new LatLng(startLatLng.latitude, startLatLng.longitude);
        }
        if (Constants.FlowStatus.equals("1") || Constants.FlowStatus.equals("2")) {


            if (pickUPrDropMarker != null)
                pickUPrDropMarker.remove();

            System.out.println("pick location ===>" + pickupLatLng);
            pickUPrDropMarker = mMap.addMarker(new MarkerOptions().position(pickupLatLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ub__ic_pin_pickup)));


        } else {


            if (pickUPrDropMarker != null)
                pickUPrDropMarker.remove();

            System.out.println("dest location ===>" + destinationLatLng);
            pickUPrDropMarker = mMap.addMarker(new MarkerOptions().position(destinationLatLng).icon(BitmapDescriptorFactory.fromResource(R.mipmap.ub__ic_pin_dropoff)));


        }

    }

    public void zoomCameraToPosition(LatLng curPos) {

        System.out.println("map location===>" + curPos.latitude + "  " + curPos.longitude);

        boolean contains = mMap.getProjection().getVisibleRegion().latLngBounds.contains(curPos);

        if (!contains) {
            // MOVE CAMERA
            // mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(animatedValue[0],animatedValue[1]),17.0f));

            float zoomPosition;
            if (tripStatus) {
                zoomPosition = Constants.MAP_ZOOM_SIZE_ONTRIP;
            } else {
                zoomPosition = Constants.MAP_ZOOM_SIZE;
            }

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(curPos)                              // Sets the center of the map to current location
                    .zoom(zoomPosition)
                    .tilt(0)                                     // Sets the tilt of the camera to 0 degrees
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }


    }


    private void stopAnim() {
        if (mMap != null) {
            MapAnimator.getInstance().stopAnim();
            if (polyUtils != null) {
                polyUtils.clrearAll();
            }
            if(myMarker!=null){
                myMarker.remove();
                myMarker=null;
            }
        }
    }

    @Override
    public void onDirectionFailure(Throwable t) {
        Log.e("tage", "on direction failure" + t.getMessage());
    }


    public void setGoogleDirection(LatLng startLatLng, LatLng destionation) {
        Log.e("pickup", "pickup location" + startLatLng);
        Log.e("drop", "drop location" + destionation);

        GoogleDirection.withServerKey(Constants.GoogleDirectionkey)
                .from(startLatLng)
                .to(destionation)
                .transportMode(TransportMode.DRIVING)
                .execute(this);
        try {
            LatLngBounds.Builder builder = new LatLngBounds.Builder();
            builder.include(startLatLng);
            builder.include(destionation);
            LatLngBounds bounds = builder.build();
            mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 80));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onKeyEntered(String key, GeoLocation location) {
        Log.e(Tag, "enter the location" + location);
        System.out.println("Driver ID call listion key enter===>" + key);
        if (location != null && !tripStatus) {
            EventBus.getDefault().postSticky(new RequestStatus("Request Now"));
            // Add a new marker to the map

            //marker.setRotation(getDriverBearing(key));

            if (!markers.containsKey(key)) {

                Marker marker = mMap.addMarker(new MarkerOptions().position(new LatLng(location.latitude, location.longitude))
                        .icon(bitmapDescriptor).flat(true));
                marker.setFlat(true);
                marker.setVisible(true);
                marker.setAnchor(0.5f, 0.5f);
                this.markers.put(key, marker);
            }


        }

    }

    @Override
    public void onKeyExited(String key) {
        Marker marker = this.markers.get(key);
        if (marker != null) {
            this.markers.remove(key);
            if (markers.size() == 0) {
                EventBus.getDefault().postSticky(new RequestStatus("No Vehicle"));
            }
            marker.remove();

            System.out.println("Check marker is empty" + markers.isEmpty());
        }
    }

    @Override
    public void onKeyMoved(String key, GeoLocation location) {
        System.out.println("Driver ID call listion key call check===>" + key);
        // Move the marker
        Marker marker = this.markers.get(key);

        if (marker != null) {

            //new moveKeyAsync(marker, location.latitude, location.longitude,keyMovedBear).execute();


            LatLng curPos, prevPos;

            prevPos = new LatLng(marker.getPosition().latitude, marker.getPosition().latitude);
            curPos = new LatLng(location.latitude, location.latitude);


            if (!prevPos.equals(curPos)) {

                double[] startValues = new double[]{marker.getPosition().latitude, marker.getPosition().longitude};
                double[] endValues = new double[]{location.latitude, location.longitude};
                System.out.println("Driver ID call listion key moved===>" + key);
                this.animateMarkerTo(marker, startValues, endValues, getDriverBearing(key));

            }

        }
    }

    @Override
    public void onGeoQueryReady() {
        Log.e(Tag, "geofire start");
    }

    @Override
    public void onGeoQueryError(DatabaseError error) {
        Log.e(Tag, "geofire error" + error.getMessage());
    }

    private void animateMarkerTo(final Marker marker, double[] startValues, double[] endValues, float rotate) {

        ValueAnimator latLngAnimator = ValueAnimator.ofObject(new DoubleArrayEvaluator(), startValues, endValues);
        latLngAnimator.setDuration(1000);
        latLngAnimator.setInterpolator(new DecelerateInterpolator());
        latLngAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                double[] animatedValue = (double[]) animation.getAnimatedValue();
                marker.setPosition(new LatLng(animatedValue[0], animatedValue[1]));
            }
        });
        latLngAnimator.start();
        //float rotate = getBearing(new LatLng(startValues[0], startValues[1]), new LatLng(endValues[0], endValues[1]));
        System.out.println("Rotate===>" + rotate);
        //marker.setRotation(rotate + mMap.getCameraPosition().bearing);
        //marker.setRotation(getDriverBearing(driverID));
        marker.setRotation(rotate);
       /* if (headerButton.getText().toString().matches("ACCEPTED") || headerButton.getText().toString().matches("ARRIVING") || headerButton.getText().toString().matches("ON TRIP")) {

            float zoomPosition;
            if(tripStatus){
                zoomPosition=Constants.MAP_ZOOM_SIZE_ONTRIP;
            }else {
                zoomPosition=Constants.MAP_ZOOM_SIZE;
            }

            final CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(endValues[0], endValues[1]))      // Sets the center of the map to Mountain View
                    .zoom(zoomPosition)                   // Sets the zoom
                    .bearing(rotate)                // Sets the orientation of the camera to east
                    .tilt(30)                   // Sets the tilt of the camera to 30 degrees
                    .build();
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }*/

    }

    @OnClick(R.id.work_imgbtn)
    public void onWorkImgbtnClicked() {
        CommonData.Droplat = Double.parseDouble(SharedHelper.getKey(context, "worklat"));
        CommonData.Droplng = Double.parseDouble(SharedHelper.getKey(context, "worklng"));
        CommonData.strDropAddresss = SharedHelper.getKey(context, "workaddress");
        StartRequestFlow(true);
    }

    @OnClick(R.id.home_imgbtn)
    public void onHomeImgbtnClicked() {
        CommonData.Droplat = Double.parseDouble(SharedHelper.getKey(context, "homelat"));
        CommonData.Droplng = Double.parseDouble(SharedHelper.getKey(context, "homelng"));
        CommonData.strDropAddresss = SharedHelper.getKey(context, "homeaddress");
        StartRequestFlow(true);
    }

    @Override
    public void onwalletSuccessfully(Response<WalletBalanceModel> Response) {
        if (Response.body().getSuccess()) {
            SharedHelper.putKey(context, "wallet_amout", Response.body().getBalance());
            setTextWalletBalance();
        }

    }

    @Override
    public void onwalletFailure(Response<WalletBalanceModel> Response) {

    }

    @Override
    public void onaddwalletSuccessfully(Response<AddWalletModel> Response) {

    }

    @Override
    public void onaddwalletFailure(Response<AddWalletModel> Response) {

    }

    @Override
    public void geocoderOnSucessful(GeocoderModel geocoderModel) {
        if (geocoderModel.getStatus().equalsIgnoreCase("OK")) {
            CommonData.strPickupAddress = geocoderModel.getResults().get(0).getFormattedAddress();

        } else {
            googleGeocoderPresenter.getAddressFromLocation(new LatLng(CommonData.CurrentLocation.getLatitude(), CommonData.CurrentLocation.getLongitude()));
        }

    }

    @Override
    public void geocoderOnFailure(Throwable throwable) {

    }

    @Override
    public boolean onLongClick(View v) {
        switch (v.getId()) {
            case R.id.pickup_address_txt:
                GOtoFavoriteLocation();
                CommonData.isPickup = true;
                break;
            case R.id.drop_address_txt:
                GOtoFavoriteLocation();
                CommonData.isPickup = false;
                break;

        }
        return false;
    }

    public class DoubleArrayEvaluator implements TypeEvaluator<double[]> {

        private double[] mArray;

        /**
         * Create a DoubleArrayEvaluator that does not reuse the animated value. Care must be taken
         * when using this option because on every evaluation a new <code>double[]</code> will be
         * allocated.
         *
         * @see #DoubleArrayEvaluator(double[])
         */
        public DoubleArrayEvaluator() {
        }

        /**
         * Create a DoubleArrayEvaluator that reuses <code>reuseArray</code> for every evaluate() call.
         * Caution must be taken to ensure that the value returned from
         * {@link ValueAnimator#getAnimatedValue()} is not cached, modified, or
         * used across threads. The value will be modified on each <code>evaluate()</code> call.
         *
         * @param reuseArray The array to modify and return from <code>evaluate</code>.
         */
        public DoubleArrayEvaluator(double[] reuseArray) {
            mArray = reuseArray;
        }

        /**
         * Interpolates the value at each index by the fraction. If
         * {@link #DoubleArrayEvaluator(double[])} was used to construct this object,
         * <code>reuseArray</code> will be returned, otherwise a new <code>double[]</code>
         * will be returned.
         *
         * @param fraction   The fraction from the starting to the ending values
         * @param startValue The start value.
         * @param endValue   The end value.
         * @return A <code>double[]</code> where each element is an interpolation between
         * the same index in startValue and endValue.
         */
        @Override
        public double[] evaluate(float fraction, double[] startValue, double[] endValue) {
            double[] array = mArray;
            if (array == null) {
                array = new double[startValue.length];
            }

            for (int i = 0; i < array.length; i++) {
                double start = startValue[i];
                double end = endValue[i];
                array[i] = start + (fraction * (end - start));
            }
            return array;
        }
    }

    public synchronized float getDriverBearing(String key) {

        DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference().child("drivers_location").child(carcategory).child(key).child("bearing");
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    String status = dataSnapshot.getValue().toString();
                    if (isFloat(status)) {
                        driverBearing = Float.parseFloat(status);
                    } else {
                        driverBearing = (float) Integer.parseInt(status);
                    }

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        return driverBearing;
        //return 0;
    }

    boolean isFloat(String str) {
        try {

            Float.parseFloat(str);

            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public void setGeofire(GeoFire geofire) {
        this.geoFire = geofire;
    }


    public GeoFire getGeofire() {
        return geoFire;
    }

    public void Geofire(LatLng latLng, String ServiceType) {
        geoFire = new GeoFire(FirebaseDatabase.getInstance().getReference().child("drivers_location").child(ServiceType));
        setGeofire(geoFire);
        try {
            if (markers != null && !markers.isEmpty()) {
                for (Marker marker : this.markers.values()) {
                    marker.setVisible(false);
                    marker.remove();
                }
                markers.clear();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        geoQuery = getGeofire().queryAtLocation(new GeoLocation(latLng.latitude, latLng.longitude), 35);
        if (this.geoQuery != null) {
            this.geoQuery.setCenter(new GeoLocation(latLng.latitude, latLng.longitude));
            // radius in km Dynamic_Radious
            this.geoQuery.setRadius(35);
            this.geoQuery.addGeoQueryEventListener(this);
            System.out.println("geo status: geo queery started in on connected");
        } else {
            Toast.makeText(this, "Geoquery Null", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
        // add an event listener to start updating locations again
        if (!tripStatus) {
            if (geoQuery != null) {
                try {
                    this.geoQuery.addGeoQueryEventListener(this);
                    System.out.println("geo status: geo queery started in on start");
                } catch (NullPointerException e) {
                    System.out.print("Geo query event listener Null Point exception" + e);
                } catch (IllegalArgumentException e) {
                    System.out.print("Geo query event listener Illegal Argument exception" + e);
                }
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

        // remove all event listeners to stop updating in the background
        if (geoQuery != null) {
            this.geoQuery.removeAllListeners();
            if (markers != null) {
                for (Marker marker : this.markers.values()) {
                    marker.remove();
                }
                this.markers.clear();
            }

        }
    }

    public void Alertdialog(String Message) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(context);
        builder1.setTitle(activity.getResources().getString(R.string.logout));
        builder1.setMessage(Message);
        builder1.setCancelable(true);
        builder1.setPositiveButton(
                activity.getResources().getString(R.string.yes),
                (dialog, id) -> {
                    dialog.dismiss();
                    try {
                        CommonData.addressList.clear();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    LogoutPresenter logoutPresenter = new LogoutPresenter();
                    logoutPresenter.LogoutData(activity);
                   /* new Thread(() -> {
                        try {
                            FirebaseInstanceId.getInstance().deleteInstanceId();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }).start();*/
                    SharedHelper.clearSharedPreferences(context);
                    Intent intent = new Intent(context, WelcomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                    finish();

                });
        builder1.setNegativeButton("No", (dialog, which) -> dialog.dismiss());
        logoutAlert = builder1.create();
        logoutAlert.show();


    }


    public void LastTripStatusCheck() {
        LastTripDatabase = FirebaseDatabase.getInstance().getReference().child("riders_data").child(SharedHelper.getKey(context, "userid"));
        LastTripValueEvent = LastTripDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    if (!Constants.CheckRiderStatus.equalsIgnoreCase(dataSnapshot.child("tripstatus").getValue().toString()) && !dataSnapshot.child("tripstatus").getValue().toString().equalsIgnoreCase("0")) {
                        Constants.CheckRiderStatus = dataSnapshot.child("tripstatus").getValue().toString();
                        Checkstatus(dataSnapshot.child("tripstatus").getValue().toString(), dataSnapshot);
                    }
                    RemoveView(dataSnapshot.child("tripstatus").getValue().toString());
                    Object cancelExceeds = dataSnapshot.child("cancelExceeds").getValue();
                    Object oldBalances = dataSnapshot.child("oldBalance").getValue();
                    Object lastCanceledDate = dataSnapshot.child("lastCanceledDate").getValue();
                    String currentdate = getDateFormate();
                    if (cancelExceeds != null && cancelExceeds.toString().equals("1") && lastCanceledDate.toString().equalsIgnoreCase(currentdate)) {
                        CommonFirebaseListoner.cancelExceedss = cancelExceeds.toString();
                        CommonFirebaseListoner.riderAlert(activity, CommonFirebaseListoner.riderCancelLimiitExceeds);
                    } else {
                        CommonFirebaseListoner.cancelExceedss = "0";
                        if (cancelExceeds == null) {
                            Utiles.ClearFirabae(SharedHelper.getKey(context, "userid"));
                        } else if (!cancelExceeds.equals("0") && !lastCanceledDate.equals("0")) {
                            Utiles.ClearFirabae(SharedHelper.getKey(context, "userid"));
                        }

                    }
                    if (oldBalances != null && !oldBalances.toString().isEmpty()) {
                        CommonFirebaseListoner.oldBalance = oldBalances.toString();
                    } else {
                        CommonFirebaseListoner.oldBalance = "0";
                    }


                } else {

                    Utiles.UpDateRiders(SharedHelper.getKey(context, "userid"), context);
                }


            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("tag", databaseError.getMessage());
            }
        });
    }


    public String getDateFormate() {
        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        String formattedDate = df.format(c);
        System.out.println("Current date formate => " + formattedDate);
        return formattedDate.replaceAll(" ", "");
    }

    public void RemoveView(String status) {
        if (status != null && status.equalsIgnoreCase("No Driver Found") | status.equalsIgnoreCase("Accepted")) {
            compositeDisposable.add(Observable.timer(3, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                    .subscribe(aLong -> {
                        tripStatus = false;
                        if (status.equalsIgnoreCase("No Driver Found")) {
                            NormalMode();
                            menuImg.setImageResource(R.drawable.ic_menu_layer);
                            // RemovePolyline();

                            Constants.TripFlowFragmant = null;
                            Utiles.ClearFirebase(SharedHelper.getKey(context, "userid"));
                        }
                        stopAnim();
                        flowstatus = 0;
                        Utiles.CommonToast(activity, status);
                        CheckFavoriteLocation();
                        //     setCurrentlocation();
                        RemoveFragment(new RequestFragment());
                    }));
        }
    }

    @SuppressLint("SetTextI18n")
    public void Checkstatus(String status, DataSnapshot dataSnapshot) {
        switch (status) {
            case "Processing":
                // removeAllFragments(FragmentManage);
                isFirstTime = true;
                flowstatus = 1;
                Constants.TripFlowFragmant = new RequestFragment();
                Constants.TripFlowSearchScreen = Constants.TripFlowFragmant;
                FLowoverlay(Constants.TripFlowFragmant);
                CommonData.strRequestId = Objects.requireNonNull(dataSnapshot.child("requestId").getValue()).toString();
                CheckFavoriteLocation();
                //removeAllFragments(FragmentManage);
                //Clearfragmen();
                break;
            case "No Driver Found":
                tripStatus = false;
                NormalMode();
                menuImg.setImageResource(R.drawable.ic_menu_layer);
                // RemovePolyline();
                stopAnim();
                flowstatus = 0;
               compositeDisposable.add( Observable.timer(2, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                        .subscribe(aLong -> {
                            removeAllFragments(FragmentManage);
                            RemoveFragment(Constants.TripFlowFragmant);
                        },Throwable::printStackTrace));
               if(!fragmentslist.isEmpty()){
                   fragmentslist.clear();
               }

                Constants.TripFlowFragmant = null;
                Utiles.ClearFirebase(SharedHelper.getKey(context, "userid"));
                Utiles.CommonToast(activity, status);
                CheckFavoriteLocation();
                break;
            case "Accepted":
                compositeDisposable.add(Observable.timer(3, TimeUnit.SECONDS, AndroidSchedulers.mainThread())
                        .subscribe(aLong -> {
                            isFirstTime = true;
                            flowstatus = 1;
                            mMap.clear();
                            myMarker=null;
                            menuImg.setImageResource(R.drawable.ic_menu_layer);
                            flowLayout.setVisibility(View.VISIBLE);
                            TripTitle.setText(activity.getResources().getString(R.string.accept_your_trip_request));
                            SharedHelper.putKey(context, "trip_id", Objects.requireNonNull(dataSnapshot.child("current_tripid").getValue()).toString());
                            SharedHelper.putKey(context, "driver_id", Objects.requireNonNull(dataSnapshot.child("tripdriver").getValue()).toString());
                            RemovePolyline();
                            if (Constants.TripFlowFragmant instanceof RequestFragment) {
                                RemoveFragment(Constants.TripFlowFragmant);
                            }
                            removeAllFragments(FragmentManage);
                            FirebaseTripFlow();
                            FlowMode(true);
                            Constants.TripFlowFragmant = new TripFlowFragment();
                            ServiceFow(Constants.TripFlowFragmant);
                            Constants.FlowStatus = "1";
                            tripStatus = true;
                            stopAnim();
                            RemoveGoeFire();
                            CheckFavoriteLocation();
                            fragmentslist.clear();
                            if(SharedHelper.getStatus(context,"isEmergency")){
                                if(emegencyImgbtn.getVisibility()==View.GONE){
                                    emegencyImgbtn.setVisibility(View.VISIBLE);
                                }
                            }
                        }, Throwable::printStackTrace));
                break;
            case "canceled":
                flowstatus = 0;
                CommonData.Droplng = 0.0;
                CommonData.Droplat = 0.0;
                menuImg.setImageResource(R.drawable.ic_menu_layer);
                RemoveFragment(Constants.TripFlowFragmant);
                tripStatus = false;
                RemovePolyline();
                flowLayout.setVisibility(View.GONE);
                stopAnim();
                if (mCurrentLocation != null) {
                    Geofire(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), "auto");

                }
                stopAnim();
                setCurrentlocation();
                RemoveflowPolyline();
                CheckFavoriteLocation();
                break;

        }

    }


    public void NormalMode() {
        whereToTxt.setVisibility(View.VISIBLE);
        pickupDropLayout.setVisibility(View.GONE);
    }

    public void RemoveFragment() {

        try {
            if (FragmentManage != null) {
                if (FragmentManage.getBackStackEntryCount() > 0) {
                    FragmentManage.popBackStackImmediate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void Reintialize() {
        if (FragmentManage == null) {
            FragmentManage = getSupportFragmentManager();
        }
    }

    public void FirebaseTripFlow() {
        TripFlowReference = FirebaseDatabase.getInstance().getReference().child("trips_data").child(SharedHelper.getKey(context, "trip_id"));
        TripFlowValue = TripFlowReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if (dataSnapshot.getValue() != null) {
                    Object status = dataSnapshot.child("status").getValue();
                    if (status != null) {
                        if (!Constants.FlowStatus.equalsIgnoreCase(status.toString())) {
                            Constants.FlowStatus = status.toString();
                            TripFlowSwitch(status.toString());
                        }

                    }
                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                Log.e("tag", "enter the error response" + databaseError.getMessage());
            }
        });
    }

    @SuppressLint("SetTextI18n")
    public void TripFlowSwitch(final String status) {

        switch (status) {
            case "1":
                if(SharedHelper.getStatus(context,"isEmergency")){
                    if(emegencyImgbtn.getVisibility()==View.GONE){
                        emegencyImgbtn.setVisibility(View.VISIBLE);
                    }
                }
                flowstatus = 1;
                tripStatus = true;
                TripTitle.setText(activity.getResources().getString(R.string.your_driver_is_on_the_way));
                flowLayout.setVisibility(View.VISIBLE);
                FlowMode(true);
                RemoveGoeFire();
                CheckFavoriteLocation();
                CallFlowFragment();
                EventBus.getDefault().postSticky(new TripStatus(status));
                menuImg.setImageResource(R.drawable.ic_menu_layer);
                break;
            case "2":
                tripStatus = true;
                if(SharedHelper.getStatus(context,"isEmergency")){
                    if(emegencyImgbtn.getVisibility()==View.GONE){
                        emegencyImgbtn.setVisibility(View.VISIBLE);
                    }
                }
                flowstatus = 1;
                TripTitle.setText(activity.getResources().getString(R.string.driver_has_arrived));
                flowLayout.setVisibility(View.VISIBLE);
                FlowMode(true);
                //  RemovePolyline();
                RemoveGoeFire();
                CallFlowFragment();
                DrawPolyline();
                CheckFavoriteLocation();
                EventBus.getDefault().postSticky(new TripStatus(status));
                break;
            case "3":
                if(SharedHelper.getStatus(context,"isEmergency")){
                    if(emegencyImgbtn.getVisibility()==View.GONE){
                        emegencyImgbtn.setVisibility(View.VISIBLE);
                    }
                }
                tripStatus = true;
                isFirstTime = true;
                flowstatus = 1;
                TripTitle.setText(activity.getResources().getString(R.string.welcome_happy_ride));
                flowLayout.setVisibility(View.VISIBLE);
                FlowMode(true);
                // RemovePolyline();
                RemoveGoeFire();
                DrawPolyline();
                CheckFavoriteLocation();
                CallFlowFragment();
                EventBus.getDefault().postSticky(new TripStatus(status));
                break;
            case "4":
                startLatLng = null;
                if(emegencyImgbtn.getVisibility()==View.VISIBLE){
                    emegencyImgbtn.setVisibility(View.GONE);
                }

                mMap.clear();
                myMarker=null;
                CommonData.Droplng = 0.0;
                CommonData.Droplat = 0.0;
                RemoveFLowCarLister();
                tripStatus = false;
                flowstatus = 0;
                flowLayout.setVisibility(View.VISIBLE);
                TripTitle.setText(activity.getResources().getString(R.string.your_trip_has_ended));
                // RemovePolyline();
                FlowMode(true);
                RemoveFragment(Constants.TripFlowFragmant);
                Constants.TripFlowFragmant = new SummaryFragment();
                FLowoverlay(Constants.TripFlowFragmant);
                if (mCurrentLocation != null) {
                    Geofire(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), "auto");
                }
                setCurrentlocation();
                RemoveflowPolyline();
                carcategory = "auto";
                CheckFavoriteLocation();
                walletPResenter();
                stopAnim();

                TripFlowReference.removeEventListener(TripFlowValue);
                break;
            case "5":
                flowstatus = 0;
                mMap.clear();
                myMarker=null;
                if(emegencyImgbtn.getVisibility()==View.VISIBLE){
                    emegencyImgbtn.setVisibility(View.GONE);
                }
                Constants.TripFlowFragmant = null;
                carcategory = "auto";
                CheckFavoriteLocation();
                flowLayout.setVisibility(View.GONE);
                RemoveFragment(Constants.TripFlowFragmant);
                removeAllFragments(FragmentManage);
                startLatLng = null;
                tripStatus = false;
                CommonData.Droplng = 0.0;
                CommonData.Droplat = 0.0;
                NormalMode();
                // RemovePolyline();
                stopAnim();
                RemoveFLowCarLister();
                Utiles.ClearFirebase(SharedHelper.getKey(context, "userid"));
                SharedHelper.putKey(context, "trip_id", "null");
                if (mCurrentLocation != null) {
                    Geofire(new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), "auto");
                }
                setCurrentlocation();
                RemoveflowPolyline();
                TripFlowReference.removeEventListener(TripFlowValue);
                break;
        }

    }

    public void RemoveFLowCarLister() {
        if (TripFlowCarDataBase != null) {
            TripFlowCarDataBase.removeEventListener(TripFlowCarValue);
        }
        if (pickUPrDropMarker != null) {
            pickUPrDropMarker.remove();
            pickUPrDropMarker = null;

        }
        if (myMarker != null) {
            myMarker.remove();
            myMarker = null;
        }
    }

    public void CallFlowFragment() {
        if (Constants.TripFlowFragmant == null) {
            Constants.TripFlowFragmant = new TripFlowFragment();
            ServiceFow(Constants.TripFlowFragmant);
        }
    }

    public void RemoveGoeFire() {
        System.out.println("enter the marker size" + markers.size());
        for (Marker marker : markers.values()) {
            marker.remove();
            System.out.println("enter the marker remove in android");
        }
        markers.clear();
        if (geoQuery != null) {
            geoQuery.removeAllListeners();
        }
    }

    @SuppressLint("SetTextI18n")
    @Override
    protected void onResume() {
        super.onResume();
        setTextWalletBalance();
        CheckFavoriteLocation();
        if(Constants.CheckRiderStatus.equalsIgnoreCase("")){

        }

        Utiles.CircleImageView(SharedHelper.getKey(context, "profile"), userProfileImage, context);
        txtUserName.setText(Utiles.NullPointer(SharedHelper.getKey(context, "fname")) + " " + Utiles.NullPointer(SharedHelper.getKey(context, "lname")));

    }

    @SuppressLint("SetTextI18n")
    public void setTextWalletBalance() {
        /*if (!SharedHelper.getKey(context, "wallet_amout").isEmpty()) {
            wallet_balance_txt.setText("Wallet Balance : $" + SharedHelper.getKey(context, "wallet_amout"));
        }*/

    }

    public static String getCountryCode(Context context, double latitude, double longitude) {
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        List<Address> addresses;
        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1);
            if (addresses != null && !addresses.isEmpty()) {
                if (addresses.get(0).getCountryCode() != null) {
                    return addresses.get(0).getCountryCode();
                } else {
                    return "";
                }

            }
        } catch (IOException ioe) {
            return "";
        }
        return "";
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase));
    }


}


