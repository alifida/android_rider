package com.auezeride.app.Activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.TripDetailsModel;
import com.auezeride.app.Presenter.TripDetailsPresenter;
import com.auezeride.app.R;
import com.auezeride.app.View.TripDetailView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;


public class TripDetailsActivity extends AppCompatActivity implements TripDetailView {


    @BindView(R.id.back_img)
    ImageButton backImg;
    @BindView(R.id.map_img)
    ImageView mapImg;
    @BindView(R.id.profile_image)
    ImageView profileImage;
    @BindView(R.id.user_name)
    TextView userName;
    @BindView(R.id.pickup_txt)
    TextView pickupTxt;
    @BindView(R.id.drop_address_txt)
    TextView dropAddressTxt;
    @BindView(R.id.date_time_txt)
    TextView dateTimeTxt;
    @BindView(R.id.total_amount_txt)
    TextView totalAmountTxt;
    @BindView(R.id.service_type_txt)
    TextView serviceTypeTxt;
    @BindView(R.id.trip_status_txt)
    TextView tripStatusTxt;
    @BindView(R.id.payment_type_img)
    ImageView paymentTypeImg;
    @BindView(R.id.payment_type_txt)
    TextView paymentTypeTxt;
    String strTrip_id;

    Context context = TripDetailsActivity.this;
    Activity activity = TripDetailsActivity.this;
    @BindView(R.id.grant_amount_txt)
    TextView grantAmountTxt;
    @BindView(R.id.kmfare_txt)
    TextView kmfareTxt;
    @BindView(R.id.Waiting_txt)
    TextView WaitingTxt;
    @BindView(R.id.Pickup_txt)
    TextView PickupTxt;
    @BindView(R.id.access_txt)
    TextView accessTxt;
    @BindView(R.id.Cancelllation_txt)
    TextView CancelllationTxt;
    @BindView(R.id.relative_cancel)
    RelativeLayout relativeCancel;
    @BindView(R.id.view_cancel)
    View viewCancel;
    @BindView(R.id.nightpeakapply_txx)
    TextView nightpeakapplyTxx;
    @BindView(R.id.nightpeakapply_cancel)
    RelativeLayout nightpeakapplyCancel;
    @BindView(R.id.nightpeakapply_cancel_view)
    View nightpeakapplyCancelView;
    @BindView(R.id.header)
    RelativeLayout header;
    @BindView(R.id.waiting_layout)
    RelativeLayout waitingLayout;
    @BindView(R.id.waiting_view)
    View waitingView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_trip_details);
        ButterKnife.bind(this);
        Intent intent = getIntent();
        strTrip_id = intent.getStringExtra("trip_id");

        CallTripDetailsFragment();


    }

    public void CallTripDetailsFragment() {
        TripDetailsPresenter tripDetailsPresenter = new TripDetailsPresenter(this);
        tripDetailsPresenter.getTripDetails(this, strTrip_id);
    }


    @OnClick(R.id.back_img)
    public void onViewClicked() {
        finish();
    }


    @Override
    public void onSuccess(Response<TripDetailsModel> Response) {
        System.out.println("Enter new json file" + new Gson().toJson(Response.body()));
        assert Response.body() != null;
        setextTextView(userName, Response.body().getTripDetail().getDvr());
        setextTextView(pickupTxt, Response.body().getTripDetail().getAdsp().getFrom());
        setextTextView(dropAddressTxt, Response.body().getTripDetail().getAdsp().getTo());
        setextTextView(dateTimeTxt, Response.body().getTripDetail().getDate());
        setextTextView(paymentTypeTxt, Response.body().getTripDetail().getAcsp().getVia());
        if (Response.body().getTripDetail().getAcsp().getVia().equalsIgnoreCase("cash")) {
            paymentTypeImg.setImageResource(R.drawable.ic_money);
            paymentTypeTxt.setText(accessTxt.getResources().getString(R.string.cash));
        } else if (Response.body().getTripDetail().getAcsp().getVia().equalsIgnoreCase("card")){
            paymentTypeTxt.setText(accessTxt.getResources().getString(R.string.card));
            paymentTypeImg.setImageResource(R.drawable.ic_stripe);
        }else
        {
            paymentTypeImg.setImageResource(R.drawable.ic_wallet);
            paymentTypeTxt.setText(accessTxt.getResources().getString(R.string.wallet));
        }
        try {
            if (Response.body().getTripDetail().getAcsp().getOldBalance() != null && !Response.body().getTripDetail().getAcsp().getOldBalance().equalsIgnoreCase("null")) {
                if (Response.body().getTripDetail().getAcsp().getOldBalance().equalsIgnoreCase("0")) {
                    relativeCancel.setVisibility(View.GONE);
                    viewCancel.setVisibility(View.GONE);
                } else {
                    relativeCancel.setVisibility(View.VISIBLE);
                    viewCancel.setVisibility(View.VISIBLE);
                }
            } else {
                relativeCancel.setVisibility(View.GONE);
                viewCancel.setVisibility(View.GONE);
            }

            if (Response.body().getTripDetail().getApplyValues().getApplyNightCharge()) {
                nightpeakapplyCancel.setVisibility(View.VISIBLE);
            } else {
                nightpeakapplyCancel.setVisibility(View.GONE);

            }
            if (Response.body().getTripDetail().getApplyValues().getApplyNightCharge()) {
                nightpeakapplyCancel.setVisibility(View.VISIBLE);
            } else {
                nightpeakapplyCancel.setVisibility(View.GONE);
            }
            if (Response.body().getTripDetail().getApplyValues().getApplyWaitingTime()) {
                waitingLayout.setVisibility(View.VISIBLE);
                waitingView.setVisibility(View.VISIBLE);
            } else {
                waitingLayout.setVisibility(View.GONE);
                waitingView.setVisibility(View.GONE);
            }
            if (Response.body().getTripDetail().getAcsp().getIsNight()) {
                nightpeakapplyCancel.setVisibility(View.VISIBLE);
                setextTextView(nightpeakapplyTxx, activity.getResources().getString(R.string.night_caharge_apply) + Response.body().getTripDetail().getAcsp().getNightPer());

            } else if (Response.body().getTripDetail().getAcsp().getIsPeak()) {
                nightpeakapplyCancel.setVisibility(View.VISIBLE);
                setextTextView(nightpeakapplyTxx, activity.getResources().getString(R.string.peak_charge_apply) + Response.body().getTripDetail().getAcsp().getPeakPer());
            } else {
                nightpeakapplyCancel.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        setextTextView(accessTxt, "$" + Response.body().getTripDetail().getAcsp().getTax());
        setextTextView(kmfareTxt, "$" + Response.body().getTripDetail().getAcsp().getDistfare());
        setextTextView(CancelllationTxt, "$" + Response.body().getTripDetail().getAcsp().getOldBalance());
        setextTextView(WaitingTxt, "$" + Response.body().getTripDetail().getAcsp().getWaitingCharge());
        setextTextView(PickupTxt, "$" + Response.body().getTripDetail().getAcsp().getConveyance());

        setextTextView(totalAmountTxt, "$ " + Response.body().getTripDetail().getAcsp().getCost());
        setextTextView(grantAmountTxt, "$ " + Response.body().getTripDetail().getAcsp().getCost());
        setextTextView(serviceTypeTxt, Response.body().getTripDetail().getVehicle());
        setextTextView(tripStatusTxt, Response.body().getTripDetail().getStatus());

        // Utiles.LocalImage()
        Utiles.Documentimg(Response.body().getTripDetail().getAdsp().getMap(), mapImg, activity);
        Utiles.CircleImageView(Response.body().getProfileDetail().getProfile(), profileImage, activity);

    }

    @Override
    public void onFailure(Response<TripDetailsModel> Response) {
        try {
            Utiles.showErrorMessage(Response.errorBody().string(), activity, getCurrentFocus());
        } catch (Exception e) {
            e.printStackTrace();
            Utiles.displayMessage(getCurrentFocus(), context, context.getString(R.string.poor_network));
        }
    }

    public void setextTextView(TextView textView, String Values) {
        try {
            textView.setText(Utiles.NullPointer(Values));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utiles.clearInstance();
    }
}