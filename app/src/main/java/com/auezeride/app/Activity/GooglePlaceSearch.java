package com.auezeride.app.Activity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.style.CharacterStyle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.tasks.Tasks;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.AutocompleteSessionToken;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsRequest;
import com.google.android.libraries.places.api.net.FindAutocompletePredictionsResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.gson.Gson;
import com.auezeride.app.Adapter.FavouriteAdapter;
import com.auezeride.app.CommonClass.CommonData;
import com.auezeride.app.CommonClass.FontChangeCrawler;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.GooglePlace.GooglePlaceAdapter;
import com.auezeride.app.GooglePlace.GooglePlcaeModel.GeocoderModel;
import com.auezeride.app.GooglePlace.GooglePlcaeModel.PlacesResults;
import com.auezeride.app.GooglePlace.GooglePlcaeModel.Prediction;
import com.auezeride.app.GooglePlace.ReverseGeoCoderModel.ReverseGeocoderModel;
import com.auezeride.app.Presenter.GoogleGeocoderPresenter;
import com.auezeride.app.Presenter.GooglePlcaePresenter;

import com.auezeride.app.R;
import com.auezeride.app.View.GoogleAutoPlaceView;
import com.auezeride.app.View.GoogleGeoCoderView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.reactivex.disposables.CompositeDisposable;
import okhttp3.ResponseBody;
import retrofit2.Response;

public class GooglePlaceSearch extends AppCompatActivity implements GoogleAutoPlaceView, GooglePlaceAdapter.Callback, GoogleGeoCoderView, FavouriteAdapter.FavoriteLocation {

    @BindView(R.id.search_et)
    EditText searchEt;
    @BindView(R.id.close_btn)
    ImageButton closeBtn;

    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @BindView(R.id.place_recycleview)
    RecyclerView placeRecycleview;
    GooglePlaceAdapter googlePlaceAdapter;

    List<AutocompletePrediction> predictions = new ArrayList<>();
    @BindView(R.id.back_img)
    ImageButton backImg;
    @BindView(R.id.pickup_txt)
    EditText pickupTxt;
    @BindView(R.id.picku_close_btn)
    ImageButton pickuCloseBtn;
    String strSetPin = null, strAddress = null;
    @BindView(R.id.pickup_relative)
    RelativeLayout pickupRelative;
    @BindView(R.id.search_layout)
    LinearLayout searchLayout;
    @BindView(R.id.parent)
    LinearLayout parent;

    private Handler handler;
    private boolean blnAddress = true;

    GooglePlcaePresenter googlePlcaePresenter;
    GoogleGeocoderPresenter googleGeocoderPresenter;

    Context context = GooglePlaceSearch.this;
    Activity activity = GooglePlaceSearch.this;

    private AutocompleteSessionToken token;

    private PlacesClient placesClient;

    private CharacterStyle STYLE_BOLD;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_google_place_search);
        ButterKnife.bind(this);
        token = AutocompleteSessionToken.newInstance();
        placesClient = Places.createClient(activity);
        Intent intent = getIntent();
        strSetPin = intent.getStringExtra("setpin");
        googlePlcaePresenter = new GooglePlcaePresenter(this);
        if (strSetPin != null) {
            pickupRelative.setVisibility(View.GONE);
        } else {
            pickupRelative.setVisibility(View.VISIBLE);
        }
        googleGeocoderPresenter = new GoogleGeocoderPresenter(this, compositeDisposable);
        if (!CommonData.strPickupAddress.isEmpty()) {
            pickupTxt.setText(CommonData.strPickupAddress);
            searchEt.requestFocus();
        } else {
            pickupTxt.requestFocus();
            if (CommonData.CurrentLocation != null && CommonData.strPickupAddress.isEmpty()) {
                strAddress = getCompleteAddressString(CommonData.Pickuplat, CommonData.Pickuplng);
                if (strAddress != null && !strAddress.isEmpty()) {
                    pickupTxt.setText(strAddress);
                    CommonData.strPickupAddress = strAddress;
                    searchEt.requestFocus();
                } else {
                    googleGeocoderPresenter.getAddressFromLocation(new LatLng(CommonData.CurrentLocation.getLatitude(), CommonData.CurrentLocation.getLongitude()));

                }
            }

        }

        FontChangeCrawler fontChanger = new FontChangeCrawler(getAssets(), getString(R.string.app_font));
        fontChanger.replaceFonts((ViewGroup) this.findViewById(android.R.id.content));

        searchEt.addTextChangedListener(watcher);
        pickupTxt.addTextChangedListener(watcher);
        searchEt.setOnFocusChangeListener(focusListener);
        pickupTxt.setOnFocusChangeListener(focusListener);
        setAdapter();
    }

    private View.OnFocusChangeListener focusListener = new View.OnFocusChangeListener() {
        public void onFocusChange(View v, boolean hasFocus) {
            switch (v.getId()) {
                case R.id.search_et:
                    blnAddress = true;
                    SetendFocus(searchEt);
                    break;
                case R.id.pickup_txt:
                    blnAddress = false;
                    SetendFocus(pickupTxt);
                    break;
            }

        }
    };
    TextWatcher watcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        @Override
        public void onTextChanged(final CharSequence charSequence, int i, int i1, int i2) {
            if (charSequence.hashCode() == searchEt.getText().hashCode()) {
                blnAddress = true;
            }

            if (charSequence.hashCode() == pickupTxt.getText().hashCode()) {
                blnAddress = false;
            }
            if (charSequence.toString().isEmpty()) {
                placeRecycleview.clearDisappearingChildren();
                setAdapter();

            } else {


                Runnable runnable = () -> getAutoCompletionText(String.valueOf(charSequence));
                if (handler != null) {
                    handler.removeCallbacksAndMessages(null);
                } else {
                    handler = new Handler();
                }
                handler.postDelayed(runnable, 100);
            }

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    public void SetendFocus(EditText editText) {
        editText.setSelection(editText.getText().length());
    }

    public void getAutoCompletionText(String inputtype) {

        AsyncTask.execute(() -> {
            List<AutocompletePrediction> list = getAutocomplete(placesClient, inputtype, token);
            activity.runOnUiThread(() -> {

                /*if ( !predictions.isEmpty()) {
                    if (favoritesLayout.getVisibility() == View.VISIBLE) {
                        favoritesLayout.setVisibility(View.GONE);
                    }

                }*/

                predictions = list;
                //  googlePlaceAdapter.UpdateSearch(predictions);
                googlePlaceAdapter = new GooglePlaceAdapter(this, predictions, this);
                placeRecycleview.setAdapter(googlePlaceAdapter);

            });

        });



    }

    @OnClick({R.id.close_btn, R.id.back_img, R.id.picku_close_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.close_btn:
                searchEt.getText().clear();
                break;
            case R.id.back_img:
                finish();
                break;
            case R.id.picku_close_btn:
                pickupTxt.getText().clear();
                break;
        }
    }

    @Override
    public void GooglePlacee(List<Prediction> prediction) {
     /*   if (predictions != null && !predictions.isEmpty()) {
            predictions.clear();
        }
        predictions.addAll(prediction);

        placeRecycleview.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
        placeRecycleview.setItemAnimator(new DefaultItemAnimator());
        placeRecycleview.setHasFixedSize(true);
        googlePlaceAdapter = new GooglePlaceAdapter(this, predictions, this);
        placeRecycleview.setAdapter(googlePlaceAdapter);*/

    }

    public void setAdapter() {
        if (CommonData.addressList != null ) {
            placeRecycleview.setLayoutManager(new LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false));
            placeRecycleview.setItemAnimator(new DefaultItemAnimator());
            FavouriteAdapter favouriteAdapter = new FavouriteAdapter(this, this);
            placeRecycleview.setAdapter(favouriteAdapter);
        }

    }


    @Override
    public void GooglePlaceError(Response<PlacesResults> resultsResponse) {
        Log.e("TAG", "google autocompletion error body " + new Gson().toJson(resultsResponse.errorBody()));

    }

    @Override
    public void GoogleReverseGoecoder(Response<ReverseGeocoderModel> responsebody) {
        if (strSetPin == null) {
            if (blnAddress) {
                CommonData.Droplat = responsebody.body().getResults().get(0).getGeometry().getLocation().getLat();
                CommonData.Droplng = responsebody.body().getResults().get(0).getGeometry().getLocation().getLng();
            } else {
                CommonData.Pickuplat = responsebody.body().getResults().get(0).getGeometry().getLocation().getLat();
                CommonData.Pickuplng = responsebody.body().getResults().get(0).getGeometry().getLocation().getLng();
            }
            if (!searchEt.getText().toString().isEmpty() && !pickupTxt.getText().toString().isEmpty()) {
                Intent intent = new Intent();
                Bundle b = new Bundle();
                CommonData.strPickupAddress = pickupTxt.getText().toString();
                CommonData.strDropAddresss = searchEt.getText().toString();
                b.putDouble("droplat", responsebody.body().getResults().get(0).getGeometry().getLocation().getLat());
                b.putDouble("droplng", responsebody.body().getResults().get(0).getGeometry().getLocation().getLng());
                intent.putExtras(b);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        } else {
            Intent intent = new Intent();
            Bundle b = new Bundle();
            b.putDouble("droplat", responsebody.body().getResults().get(0).getGeometry().getLocation().getLat());
            b.putDouble("droplng", responsebody.body().getResults().get(0).getGeometry().getLocation().getLng());
            intent.putExtras(b);
            setResult(Activity.RESULT_OK, intent);
            finish();
        }


    }

    @Override
    public void OnSuccessfully(Response<ResponseBody> response) {
        try {
            String message = response.body().string();
            JSONObject jsonObject = new JSONObject(message);
            if (jsonObject.has("message")) {
                Utiles.displayMessage(getCurrentFocus(), context, jsonObject.optString("message"));
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void OnFailure(Response<ResponseBody> response) {

    }

    @Override
    public void SelectedAddress(AutocompletePrediction Address) {
        CommonData.strDropAddresss = Address.getFullText(STYLE_BOLD).toString();
        List<Place.Field> placeFields = Arrays.asList(Place.Field.ADDRESS,
                Place.Field.ID,
                Place.Field.LAT_LNG);
        // Construct a request object, passing the place ID and fields array.
        FetchPlaceRequest request = FetchPlaceRequest.builder(Address.getPlaceId(), placeFields)
                .build();
        //   LatLng droplocation = getLocationFromAddress(context, Address.getDescription());
        if (strSetPin == null) {
            if (blnAddress) {
                searchEt.setText(Address.getFullText(STYLE_BOLD).toString());
                SetendFocus(searchEt);
                CommonData.strDropAddresss = Address.getFullText(STYLE_BOLD).toString();
            } else {
                pickupTxt.setText(Address.getFullText(STYLE_BOLD).toString());
                SetendFocus(pickupTxt);
                CommonData.strPickupAddress = Address.getFullText(STYLE_BOLD).toString();
            }


            placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
                Place place = response.getPlace();
                LatLng queriedLocation = place.getLatLng();
                assert queriedLocation != null;
                if (blnAddress) {
                    CommonData.Droplat = queriedLocation.latitude;
                    CommonData.Droplng = queriedLocation.longitude;
                } else {
                    CommonData.Pickuplat = queriedLocation.latitude;
                    CommonData.Pickuplng = queriedLocation.longitude;
                }
                if (!searchEt.getText().toString().isEmpty() && !pickupTxt.getText().toString().isEmpty()) {
                    Utiles.hideKeyboard(activity);
                    CommonData.strPickupAddress = pickupTxt.getText().toString();
                    CommonData.strDropAddresss = searchEt.getText().toString();
                    Intent intent = new Intent();
                    Bundle b = new Bundle();
                    b.putDouble("droplat", queriedLocation.latitude);
                    b.putDouble("droplng", queriedLocation.longitude);
                    intent.putExtras(b);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            }).addOnFailureListener((exception) -> {
                if (exception instanceof ApiException) {
                    // Handle error with given status code.
                    googlePlcaePresenter.getReverseGeocoder(Address.getFullText(STYLE_BOLD).toString());
                }
            });
        } else {
            CommonData.strSetpinAddress = Address.getFullText(STYLE_BOLD).toString();
          /*  if (droplocation != null) {

            } else {
                googlePlcaePresenter.getReverseGeocoder(Address.getDescription());
            }*/
            placesClient.fetchPlace(request).addOnSuccessListener((response) -> {
                Place place = response.getPlace();
                LatLng queriedLocation = place.getLatLng();
                assert queriedLocation != null;
                Utiles.hideKeyboard(activity);

                Intent intent = new Intent();
                Bundle b = new Bundle();
                b.putDouble("droplat", queriedLocation.latitude);
                b.putDouble("droplng", queriedLocation.longitude);
                intent.putExtras(b);
                setResult(Activity.RESULT_OK, intent);
                finish();
            }).addOnFailureListener((exception) -> {
                if (exception instanceof ApiException) {
                    // Handle error with given status code.
                    googlePlcaePresenter.getReverseGeocoder(Address.getFullText(STYLE_BOLD).toString());
                }
            });

        }


    }

    public LatLng getLocationFromAddress(Context context, String strAddress) {

        Geocoder coder = new Geocoder(context);
        List<Address> address;
        LatLng p1 = null;

        try {
            // May throw an IOException
            address = coder.getFromLocationName(strAddress, 5);
            if (address == null) {
                return null;
            }
            if (address.isEmpty()) {
                return null;
            }
            Address location = address.get(0);
            p1 = new LatLng(location.getLatitude(), location.getLongitude());

        } catch (IOException ex) {

            ex.printStackTrace();
        }

        return p1;
    }

    @SuppressLint("LongLogTag")
    private String getCompleteAddressString(double LATITUDE, double LONGITUDE) {
        String strAdd = "";
        Geocoder geocoder = new Geocoder(context, Locale.getDefault());
        try {
            List<Address> addresses = geocoder.getFromLocation(LATITUDE, LONGITUDE, 1);
            if (addresses != null) {
                Address returnedAddress = addresses.get(0);
                StringBuilder strReturnedAddress = new StringBuilder("");

                for (int i = 0; i <= returnedAddress.getMaxAddressLineIndex(); i++) {
                    strReturnedAddress.append(returnedAddress.getAddressLine(i)).append("\n");
                }
                strAdd = strReturnedAddress.toString();
                Log.w("My Current loction address", strReturnedAddress.toString());
            } else {
                Log.w("My Current loction address", "No Address returned!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.w("My Current loction address", "Canont get Address!");
        }
        return strAdd;
    }

    @Override
    public void geocoderOnSucessful(GeocoderModel geocoderModel) {
        try {
            if (geocoderModel.getStatus().equalsIgnoreCase("OK")) {
                CommonData.strPickupAddress = geocoderModel.getResults().get(0).getFormattedAddress();
                pickupTxt.setText(CommonData.strPickupAddress);
                searchEt.requestFocus();
            } else {
                googleGeocoderPresenter.getAddressFromLocation(new LatLng(CommonData.CurrentLocation.getLatitude(), CommonData.CurrentLocation.getLongitude()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void geocoderOnFailure(Throwable throwable) {


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Utiles.compositeClreate(compositeDisposable);
        Utiles.clearInstance();
    }

    @Override
    public void favoriteLocation(int position, boolean isadded) {

        if (CommonData.addressList != null) {
            if (isadded) {
                if (blnAddress) {
                    CommonData.Droplat = Double.parseDouble(CommonData.addressList.get(position).getCoords().get(1));
                    CommonData.Droplng = Double.parseDouble(CommonData.addressList.get(position).getCoords().get(0));
                    CommonData.strDropAddresss = CommonData.addressList.get(position).getAddress();
                    searchEt.setText(CommonData.addressList.get(position).getAddress());
                    SetendFocus(searchEt);
                } else {
                    CommonData.Pickuplat = Double.parseDouble(CommonData.addressList.get(position).getCoords().get(1));
                    CommonData.Pickuplng = Double.parseDouble(CommonData.addressList.get(position).getCoords().get(0));
                    CommonData.strPickupAddress = CommonData.addressList.get(position).getAddress();
                    pickupTxt.setText(CommonData.addressList.get(position).getAddress());
                    SetendFocus(pickupTxt);
                }

                if (!searchEt.getText().toString().isEmpty() && !pickupTxt.getText().toString().isEmpty()) {
                    Utiles.hideKeyboard(activity);
                    CommonData.strPickupAddress = pickupTxt.getText().toString();
                    CommonData.strDropAddresss = searchEt.getText().toString();
                    Intent intent = new Intent();
                    Bundle b = new Bundle();
                    b.putDouble("droplat", CommonData.Droplat);
                    b.putDouble("droplng", CommonData.Droplng);
                    intent.putExtras(b);
                    setResult(Activity.RESULT_OK, intent);
                    finish();
                }
            } else {
                googlePlcaePresenter.getDeteleteAddress(activity, CommonData.addressList.get(position).getId());
            }


        }
    }

    private List<AutocompletePrediction> getAutocomplete(PlacesClient mPlacesClient, CharSequence constraint, AutocompleteSessionToken token) {

        List<AutocompletePrediction> list = new ArrayList<>();
        FindAutocompletePredictionsRequest request = FindAutocompletePredictionsRequest.builder()
                .setSessionToken(token).setCountry(CommonData.strCountryCode)
                .setQuery(constraint.toString()).build();
        Task<FindAutocompletePredictionsResponse> prediction = mPlacesClient.findAutocompletePredictions(request);
        try {
            Tasks.await(prediction, 3, TimeUnit.SECONDS);
        } catch (ExecutionException | InterruptedException | TimeoutException e) {
            e.printStackTrace();
        }
        if (prediction.isSuccessful()) {
            FindAutocompletePredictionsResponse result = prediction.getResult();
            assert result != null;
            list = result.getAutocompletePredictions();
            return list;
        }
        return list;
    }

}
