package com.auezeride.app.Presenter;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.PromoCodeModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.PromoView;

import java.util.logging.Logger;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PromocodePresenter {

    public RetrofitGenerator retrofitGenerator = null;
    public Activity activity;
    private PromoView promoView;

    public PromocodePresenter(Activity activity, PromoView promoView) {
        this.activity = activity;
        this.promoView = promoView;
    }

    public void getProcodeResult(String data) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<PromoCodeModel> call = service.getpromoAmount(SharedHelper.getKey(activity.getApplicationContext(), "token"), data.toUpperCase());
                call.enqueue(new Callback<PromoCodeModel>() {
                    @Override
                    public void onResponse(@NonNull Call<PromoCodeModel> call, @NonNull Response<PromoCodeModel> response) {
                        retrofitGenerator = null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            promoView.OnSuccessfully(response);
                        } else {
                            promoView.OnFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<PromoCodeModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });

            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }
}
