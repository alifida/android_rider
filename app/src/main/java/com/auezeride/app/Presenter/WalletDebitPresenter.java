package com.auezeride.app.Presenter;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.WalletTransactionCreditModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.WalletCreditView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletDebitPresenter {
    public RetrofitGenerator retrofitGenerator;
    public WalletCreditView walletCreditView;
    public Activity activity;

    public WalletDebitPresenter(Activity activity, WalletCreditView walletCreditView) {
        this.walletCreditView = walletCreditView;
        this.activity = activity;

    }

    public void getWalletBalance() {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<WalletTransactionCreditModel> call = service.getWalletDebitList(SharedHelper.getKey(activity.getApplicationContext(), "token"));
                call.enqueue(new Callback<WalletTransactionCreditModel>() {
                    @Override
                    public void onResponse(@NonNull Call<WalletTransactionCreditModel> call, @NonNull Response<WalletTransactionCreditModel> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            walletCreditView.onTransactionSuccessfully(response);
                        } else {
                            walletCreditView.onTransactionFailure(response);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<WalletTransactionCreditModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        Log.d("debit exception",t.getMessage());
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }


}
