package com.auezeride.app.Presenter;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.ScheduleTripModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.ScheduleRequestView;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ScheduleRequestPresenter {
    public RetrofitGenerator retrofitGenerator = null;
    public ScheduleRequestView setrequestView;

    public ScheduleRequestPresenter(ScheduleRequestView setrequestView) {
        this.setrequestView = setrequestView;

    }

    public void setScheduleRequest(HashMap<String, String> data, final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<ScheduleTripModel> call = service.setScheduleRequestapi(SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<ScheduleTripModel>() {
                    @Override
                    public void onResponse(@NonNull Call<ScheduleTripModel> call, @NonNull Response<ScheduleTripModel> response) {
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            setrequestView.OnSuccessfully(response);

                        } else {
                            setrequestView.OnRequestFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ScheduleTripModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        System.out.println("Enter error response" + t.getMessage());
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }
}
