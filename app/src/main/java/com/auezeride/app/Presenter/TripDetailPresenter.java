package com.auezeride.app.Presenter;

import android.app.Activity;
import android.util.Log;


import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.TripHistoryModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.TripDetailsView;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TripDetailPresenter {
    public RetrofitGenerator retrofitGenerator = null;
    TripDetailsView tripDetailsView;

    public TripDetailPresenter(TripDetailsView tripDetailsView) {
        this.tripDetailsView = tripDetailsView;
    }

    public void getTripHistory(final Activity activity, HashMap<String,String > map) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<List<TripHistoryModel>> call = service.getTripHistory(SharedHelper.getKey(activity.getApplicationContext(), "token"),map);
                call.enqueue(new Callback<List<TripHistoryModel>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<TripHistoryModel>> call, @NonNull Response<List<TripHistoryModel>> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            Utiles.DismissLoader();
                            tripDetailsView.Onsuccess(response);
                        } else {
                            tripDetailsView.onFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<List<TripHistoryModel>> call, @NonNull Throwable t) {
                        Log.e("tag", "trip history api exception" + t.getMessage());
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }

}
