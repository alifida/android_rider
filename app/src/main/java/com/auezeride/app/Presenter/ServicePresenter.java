package com.auezeride.app.Presenter;

import android.app.Activity;
import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.ServiceModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.ServiceView;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServicePresenter {
    public RetrofitGenerator retrofitGenerator = null;
    ServiceView serviceView;

    public ServicePresenter(ServiceView serviceView) {
        this.serviceView = serviceView;

    }

    public void getServiceFare(final Activity activity, HashMap<String, String> map) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<ServiceModel> call = service.getServiceList( SharedHelper.getKey(activity.getApplicationContext(), "token"), map);
                call.enqueue(new Callback<ServiceModel>() {
                    @Override
                    public void onResponse(@NonNull Call<ServiceModel> call, @NonNull Response<ServiceModel> response) {
                        Utiles.DismissLoader();
                        System.out.println("enter tyhe"+call.request().url());
                        if (response.isSuccessful() && response.body() != null) {
                            serviceView.OnSuccess(response);
                        } else {
                            serviceView.OnFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ServiceModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }

}
