package com.auezeride.app.Presenter;

import android.app.Activity;
import androidx.annotation.NonNull;
import android.util.Log;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.GooglePlace.GooglePlcaeModel.PlacesResults;
import com.auezeride.app.GooglePlace.ReverseGeoCoderModel.ReverseGeocoderModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.GoogleAutoPlaceView;

import com.auezeride.app.CommonClass.Constants;

import com.auezeride.app.CommonClass.CommonData;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class GooglePlcaePresenter {
    static String BASE_URL = "https://maps.googleapis.com";
    public GoogleAutoPlaceView googleAutoPlaceView;

    RetrofitGenerator retrofitGenerator;

    public GooglePlcaePresenter(GoogleAutoPlaceView googleAutoPlaceView) {
        this.googleAutoPlaceView = googleAutoPlaceView;

    }


    public void getAutoCompletionresult(String input, String Loction, String Key) {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<PlacesResults> call = service.getCityResults(input, Loction,"500", Key,"country:"+ CommonData.strCountryCode);
        call.enqueue(new Callback<PlacesResults>() {
            @Override
            public void onResponse(@NonNull Call<PlacesResults> call, @NonNull Response<PlacesResults> response) {
                if (response.isSuccessful() && response.body() != null) {
                    googleAutoPlaceView.GooglePlacee(response.body().getPredictions());
                } else {
                    googleAutoPlaceView.GooglePlaceError(response);
                }

            }

            @Override
            public void onFailure(@NonNull Call<PlacesResults> call, @NonNull Throwable t) {
                Log.e("tag", "google Place error response" + t.getMessage());

            }
        });
    }

    public void getReverseGeocoder(final String Address) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.GoogleGeocoderAPI)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        ApiInterface service = retrofit.create(ApiInterface.class);
        Call<ReverseGeocoderModel> call = service.getReverserGecoder(Address, Constants.GooglPlaceApikey);
        call.enqueue(new Callback<ReverseGeocoderModel>() {
            @Override
            public void onResponse(Call<ReverseGeocoderModel> call, Response<ReverseGeocoderModel> response) {
                if (response.isSuccessful() && response.body() != null) {
                    if (response.body().getStatus().equalsIgnoreCase("OK")) {
                        googleAutoPlaceView.GoogleReverseGoecoder(response);
                    } else {
                        getReverseGeocoder(Address);
                    }


                } else {
                    getReverseGeocoder(Address);
                }

            }

            @Override
            public void onFailure(Call<ReverseGeocoderModel> call, Throwable t) {

            }
        });
    }

    public void getDeteleteAddress(final Activity activity, String data) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<ResponseBody> call = service.getDeleteFavorite(SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        retrofitGenerator = null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            googleAutoPlaceView.OnSuccessfully(response);
                        } else {
                            googleAutoPlaceView.OnFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });

            }

        } else {
            Utiles.showNoNetwork(activity);
        }
    }
}

