package com.auezeride.app.Presenter;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.CancelRequestModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.RequestView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CancelRequestPresenter {
    public RetrofitGenerator retrofitGenerator = null;
    public RequestView requestView;

    public CancelRequestPresenter(RequestView requestView) {
        this.requestView = requestView;

    }

    public void cancelRequestApi(String data, final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<CancelRequestModel> call = service.CancelRequest( SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<CancelRequestModel>() {
                    @Override
                    public void onResponse(@NonNull Call<CancelRequestModel> call, @NonNull Response<CancelRequestModel> response) {
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            requestView.onSuccess(response);

                        } else {
                            requestView.onFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<CancelRequestModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }
}
