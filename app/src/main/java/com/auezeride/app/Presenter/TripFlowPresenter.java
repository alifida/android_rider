package com.auezeride.app.Presenter;

import android.app.Activity;
import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.TripFlowModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.TripFlowView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TripFlowPresenter {

    public RetrofitGenerator retrofitGenerator = null;
    public TripFlowView tripFlowView = null;

    public TripFlowPresenter(TripFlowView tripFlowView) {
        this.tripFlowView = tripFlowView;
    }

    public void TripFlowApi(String Tripid, final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<TripFlowModel> call = service.TripFlowApi(SharedHelper.getKey(activity.getApplicationContext(), "token"), Tripid);
                call.enqueue(new Callback<TripFlowModel>() {
                    @Override
                    public void onResponse(@NonNull Call<TripFlowModel> call, @NonNull Response<TripFlowModel> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            tripFlowView.OnTripSuccessfully(response);

                        } else {
                            tripFlowView.OnTripFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<TripFlowModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }
}
