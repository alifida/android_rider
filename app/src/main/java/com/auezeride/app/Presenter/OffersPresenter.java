package com.auezeride.app.Presenter;

import android.app.Activity;
import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.OfferModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OffersPresenter {

    public RetrofitGenerator retrofitGenerator = null;
    public offersView offersView;
    Activity activity;

    public OffersPresenter(offersView offersView, Activity activity) {
        this.offersView = offersView;
        this.activity = activity;

    }

    public void getOffers(HashMap<String, Double> data) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<List<OfferModel>> call = service.getOffersApi(SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<List<OfferModel>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<OfferModel>> call, @NonNull Response<List<OfferModel>> response) {
                        retrofitGenerator = null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            offersView.onSuccess(response);
                        } else {
                            offersView.onFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<List<OfferModel>> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });

            }

        } else {
            Utiles.showNoNetwork(activity);
        }
    }

    public interface offersView {
        void onSuccess(Response<List<OfferModel>> Response);

        void onFailure(Response<List<OfferModel>> Response);
    }
}
