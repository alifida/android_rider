package com.auezeride.app.Presenter;

import android.app.Activity;

import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.AllwalletTransactionmodel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.WalletTransactionView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WalletTransactionPresenter {
    public RetrofitGenerator retrofitGenerator;
    public WalletTransactionView walletTransactionView;
    public Activity activity;

    public WalletTransactionPresenter(Activity activity, WalletTransactionView walletTransactionView) {
        this.walletTransactionView = walletTransactionView;
        this.activity = activity;

    }

    public void getWalletBalance() {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {

                Utiles.ShowLoader(activity);

                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<AllwalletTransactionmodel> call = service.getAllwelletTrascation(SharedHelper.getKey(activity.getApplicationContext(), "token"));
                call.enqueue(new Callback<AllwalletTransactionmodel>() {
                    @Override
                    public void onResponse(@NonNull Call<AllwalletTransactionmodel> call, @NonNull Response<AllwalletTransactionmodel> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            walletTransactionView.onTransactionSuccessfully(response);
                        } else {
                            walletTransactionView.onTransactionFailure(response);
                        }
                    }

                    @Override
                    public void onFailure(@NonNull Call<AllwalletTransactionmodel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }


}
