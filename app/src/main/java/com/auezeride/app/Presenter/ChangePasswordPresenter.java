package com.auezeride.app.Presenter;

import android.app.Activity;
import android.content.Context;


import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.ChangePasswordModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.ChangePasswordView;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ChangePasswordPresenter {
    public RetrofitGenerator retrofitGenerator = null;
    public ChangePasswordView changePasswordView;

    public ChangePasswordPresenter(ChangePasswordView changePasswordView) {
        this.changePasswordView = changePasswordView;

    }

    public void ChangePasswor(HashMap<String, String> data, final Activity activity, Context context) {

        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<ChangePasswordModel> call = service.ChangePassword(SharedHelper.getKey(context, "token"), data);
                call.enqueue(new Callback<ChangePasswordModel>() {
                    @Override
                    public void onResponse(@NonNull Call<ChangePasswordModel> call, @NonNull Response<ChangePasswordModel> response) {
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            changePasswordView.OnSuccessfully(response);

                        } else {
                            changePasswordView.OnFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ChangePasswordModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }
}
