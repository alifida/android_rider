package com.auezeride.app.Presenter;

import android.app.Activity;
import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.RequestModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.SetrequestView;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class RequestFlowPresenter {
    public RetrofitGenerator retrofitGenerator = null;
    public SetrequestView setrequestView;

    public RequestFlowPresenter(SetrequestView setrequestView) {
        this.setrequestView = setrequestView;

    }

    public void setRequesApi(HashMap<String, String> data, final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<RequestModel> call = service.setRequestapi( SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<RequestModel>() {
                    @Override
                    public void onResponse(@NonNull Call<RequestModel> call, @NonNull Response<RequestModel> response) {
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            setrequestView.OnSuccessfully(response);

                        } else {
                            setrequestView.OnRequestFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<RequestModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        System.out.println("Enter error response"+t.getMessage());
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }



    }
}
