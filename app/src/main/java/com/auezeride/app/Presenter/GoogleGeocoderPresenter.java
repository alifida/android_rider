package com.auezeride.app.Presenter;



import com.google.android.gms.maps.model.LatLng;
import com.auezeride.app.GooglePlace.GooglePlcaeModel.GeocoderModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.GoogleGeoCoderView;

import com.auezeride.app.CommonClass.Constants;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by com on 18-May-18.
 */

public class GoogleGeocoderPresenter {
    public RetrofitGenerator retrofitGenerator = null;

    private CompositeDisposable disposable;
    private GoogleGeoCoderView googleGeoCoderView;

    public GoogleGeocoderPresenter(GoogleGeoCoderView googleGeoCoderView, CompositeDisposable disposable) {
        this.googleGeoCoderView = googleGeoCoderView;
        this.disposable = disposable;
    }

    public void getAddressFromLocation(LatLng latLng) {

        if (retrofitGenerator == null) {
            retrofitGenerator = new RetrofitGenerator();
            ApiInterface service = retrofitGenerator.getRxJavaRetrofit().create(ApiInterface.class);
            disposable.add(service.getAddressFromLocation(String.valueOf(latLng.latitude) + "," + String.valueOf(latLng.longitude), Constants.GooglPlaceApikey)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread()).subscribe(this::OnsucessFull, this::OnError));

        }
    }

    private void OnsucessFull(GeocoderModel geocoderModel) {
        retrofitGenerator = null;
        googleGeoCoderView.geocoderOnSucessful(geocoderModel);
    }

    private void OnError(Throwable throwable) {
        retrofitGenerator = null;
        googleGeoCoderView.geocoderOnFailure(throwable);
    }
}
