package com.auezeride.app.Presenter;

import android.app.Activity;
import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.OTPModel;
import com.auezeride.app.Model.RegisterModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.RegisterView;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter {
    private RetrofitGenerator retrofitGenerator = null;
    public RegisterView registerView;

    public RegisterPresenter(RegisterView registerView) {
        this.registerView = registerView;
    }

    public void getRegisterApi(HashMap<String, String> data,final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<RegisterModel> call = service.getRegister( data);
                call.enqueue(new Callback<RegisterModel>() {
                    @Override
                    public void onResponse(@NonNull Call<RegisterModel> call, @NonNull Response<RegisterModel> response) {
                        retrofitGenerator = null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            registerView.RegisterView(response);
                            retrofitGenerator = null;
                        } else {
                            if(response.errorBody()!=null){
                                registerView.Errorlogview(response);
                                retrofitGenerator = null;
                            }

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<RegisterModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        retrofitGenerator = null;
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }

    public void getOTP(String data,String email ,String phcode,final Activity activity) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<OTPModel> call = service.getOpt(data,email,phcode);
                call.enqueue(new Callback<OTPModel>() {
                    @Override
                    public void onResponse(@NonNull Call<OTPModel> call, @NonNull Response<OTPModel> response) {
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            registerView.onSuccessOTP(response);
                            retrofitGenerator = null;

                        } else {
                            if (response.errorBody() != null) {
                                registerView.onFailureOTP(response);
                                retrofitGenerator = null;
                            }

                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<OTPModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                        retrofitGenerator = null;
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }
    }

}
