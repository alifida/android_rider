package com.auezeride.app.Presenter;

import android.app.Activity;
import android.util.Log;

import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;

import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactAdminPresenter {

    public RetrofitGenerator retrofitGenerator = null;

    public ContactAdminPresenter() {

    }

    public void getAddminContact(final Activity activity, HashMap<String, String> data) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<ResponseBody> call = service.sentAdminMessage(SharedHelper.getKey(activity.getApplicationContext(), "token"), data);
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(@NonNull Call<ResponseBody> call, @NonNull Response<ResponseBody> response) {
                        retrofitGenerator = null;
                        Utiles.DismissLoader();
                        if (response.isSuccessful() && response.body() != null) {
                            try {
                                System.out.println("Rrespppppp--->" + response.body().string());
                                Log.e("response", "response------------------>" + response.body().string());


                                Utiles.CommonToast(activity, activity.getResources().getString(R.string.message_sended_successfully));

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseBody> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));                    }
                });

            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }
}
