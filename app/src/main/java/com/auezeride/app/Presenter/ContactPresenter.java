package com.auezeride.app.Presenter;

import android.app.Activity;
import android.content.Context;

import androidx.annotation.NonNull;

import com.auezeride.app.R;

import com.auezeride.app.CommonClass.SharedHelper;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.AddContactModel;
import com.auezeride.app.Model.ContactlistModel;
import com.auezeride.app.Model.DeleteContactModel;
import com.auezeride.app.Retrofit.ApiInterface;
import com.auezeride.app.Retrofit.RetrofitGenerator;
import com.auezeride.app.View.ContactView;

import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ContactPresenter {

    public RetrofitGenerator retrofitGenerator = null;
    public ContactView contactView;

    public ContactPresenter(ContactView contactView) {
        this.contactView = contactView;

    }

    public void getContactList(final Activity activity, Context context) {
        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<List<ContactlistModel>> call = service.getContactList(SharedHelper.getKey(context, "token"));
                call.enqueue(new Callback<List<ContactlistModel>>() {
                    @Override
                    public void onResponse(@NonNull Call<List<ContactlistModel>> call, @NonNull Response<List<ContactlistModel>> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            contactView.OnSuccessfully(response);

                        } else {
                            contactView.OnFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<List<ContactlistModel>> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }

    public void DeleteContact(HashMap<String, String> map, final Activity activity, final int position) {

        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<DeleteContactModel> call = service.getDeleteContact(SharedHelper.getKey(activity.getApplicationContext(), "token"), map);
                call.enqueue(new Callback<DeleteContactModel>() {
                    @Override
                    public void onResponse(@NonNull Call<DeleteContactModel> call, @NonNull Response<DeleteContactModel> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            contactView.DeleteSuccessfully(response, position);

                        } else {
                            contactView.DeleteFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<DeleteContactModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }

    }

    public void AddContactModel(HashMap<String, String> map, final Activity activity) {

        if (Utiles.isNetworkAvailable(activity)) {
            if (retrofitGenerator == null) {
                Utiles.ShowLoader(activity);
                retrofitGenerator = new RetrofitGenerator();
                ApiInterface service = retrofitGenerator.getRetrofitUrl().create(ApiInterface.class);
                Call<AddContactModel> call = service.getAddContact(SharedHelper.getKey(activity.getApplicationContext(), "token"), map);
                call.enqueue(new Callback<AddContactModel>() {
                    @Override
                    public void onResponse(@NonNull Call<AddContactModel> call, @NonNull Response<AddContactModel> response) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        if (response.isSuccessful() && response.body() != null) {
                            contactView.AddSuccessfully(response);
                        } else {
                            contactView.AddFailure(response);
                        }

                    }

                    @Override
                    public void onFailure(@NonNull Call<AddContactModel> call, @NonNull Throwable t) {
                        Utiles.DismissLoader();
                        retrofitGenerator = null;
                        Utiles.CommonToast(activity, activity.getResources().getString(R.string.something_went_wrong));
                    }
                });
            }

        } else {
            Utiles.showNoNetwork(activity);
        }


    }
}
