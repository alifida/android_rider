package com.auezeride.app.OTP;

/**
 * Created by com on 25-Jul-18.
 */

public interface OtpListener {
    void onOtpEntered(String otp);
}
