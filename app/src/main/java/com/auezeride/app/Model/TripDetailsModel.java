package com.auezeride.app.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripDetailsModel {
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("TripDetail")
    @Expose
    private TripDetail tripDetail;
    @SerializedName("ProfileDetail")
    @Expose
    private ProfileDetail profileDetail;
    @SerializedName("Mapurl")
    @Expose
    private String mapurl;

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public TripDetail getTripDetail() {
        return tripDetail;
    }

    public void setTripDetail(TripDetail tripDetail) {
        this.tripDetail = tripDetail;
    }

    public ProfileDetail getProfileDetail() {
        return profileDetail;
    }

    public void setProfileDetail(ProfileDetail profileDetail) {
        this.profileDetail = profileDetail;
    }

    public String getMapurl() {
        return mapurl;
    }

    public void setMapurl(String mapurl) {
        this.mapurl = mapurl;
    }


    public class Acsp {

        @SerializedName("via")
        @Expose
        private String via;
        @SerializedName("time")
        @Expose
        private Object time;
        @SerializedName("dist")
        @Expose
        private String dist;
        @SerializedName("base")
        @Expose
        private String base;
        @SerializedName("chId")
        @Expose
        private String chId;
        @SerializedName("fareType")
        @Expose
        private String fareType;
        @SerializedName("waitingTime")
        @Expose
        private String waitingTime;
        @SerializedName("outstanding")
        @Expose
        private String outstanding;
        @SerializedName("stripedebt")
        @Expose
        private String stripedebt;
        @SerializedName("walletdebt")
        @Expose
        private String walletdebt;
        @SerializedName("peakPer")
        @Expose
        private String peakPer;
        @SerializedName("nightPer")
        @Expose
        private String nightPer;
        @SerializedName("isPeak")
        @Expose
        private Boolean isPeak;
        @SerializedName("isNight")
        @Expose
        private Boolean isNight;
        @SerializedName("fareAmtBeforeSurge")
        @Expose
        private String fareAmtBeforeSurge;
        @SerializedName("totalFareWithOutOldBal")
        @Expose
        private String totalFareWithOutOldBal;
        @SerializedName("taxPercentage")
        @Expose
        private String taxPercentage;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("oldBalance")
        @Expose
        private String oldBalance;
        @SerializedName("safe")
        @Expose
        private String safe;
        @SerializedName("conveyance")
        @Expose
        private String conveyance;
        @SerializedName("conveyanceKM")
        @Expose
        private String conveyanceKM;
        @SerializedName("cost")
        @Expose
        private String cost;
        @SerializedName("actualcost")
        @Expose
        private String actualcost;
        @SerializedName("detect")
        @Expose
        private String detect;
        @SerializedName("bal")
        @Expose
        private String bal;
        @SerializedName("promoamt")
        @Expose
        private String promoamt;
        @SerializedName("comison")
        @Expose
        private String comison;
        @SerializedName("waitingCharge")
        @Expose
        private String waitingCharge;
        @SerializedName("timefare")
        @Expose
        private String timefare;
        @SerializedName("distfare")
        @Expose
        private String distfare;

        public String getVia() {
            return via;
        }

        public void setVia(String via) {
            this.via = via;
        }

        public Object getTime() {
            return time;
        }

        public void setTime(Object time) {
            this.time = time;
        }

        public String getDist() {
            return dist;
        }

        public void setDist(String dist) {
            this.dist = dist;
        }

        public String getBase() {
            return base;
        }

        public void setBase(String base) {
            this.base = base;
        }

        public String getChId() {
            return chId;
        }

        public void setChId(String chId) {
            this.chId = chId;
        }

        public String getFareType() {
            return fareType;
        }

        public void setFareType(String fareType) {
            this.fareType = fareType;
        }

        public String getWaitingTime() {
            return waitingTime;
        }

        public void setWaitingTime(String waitingTime) {
            this.waitingTime = waitingTime;
        }

        public String getOutstanding() {
            return outstanding;
        }

        public void setOutstanding(String outstanding) {
            this.outstanding = outstanding;
        }

        public String getStripedebt() {
            return stripedebt;
        }

        public void setStripedebt(String stripedebt) {
            this.stripedebt = stripedebt;
        }

        public String getWalletdebt() {
            return walletdebt;
        }

        public void setWalletdebt(String walletdebt) {
            this.walletdebt = walletdebt;
        }

        public String getPeakPer() {
            return peakPer;
        }

        public void setPeakPer(String peakPer) {
            this.peakPer = peakPer;
        }

        public String getNightPer() {
            return nightPer;
        }

        public void setNightPer(String nightPer) {
            this.nightPer = nightPer;
        }

        public Boolean getIsPeak() {
            return isPeak;
        }

        public void setIsPeak(Boolean isPeak) {
            this.isPeak = isPeak;
        }

        public Boolean getIsNight() {
            return isNight;
        }

        public void setIsNight(Boolean isNight) {
            this.isNight = isNight;
        }

        public String getFareAmtBeforeSurge() {
            return fareAmtBeforeSurge;
        }

        public void setFareAmtBeforeSurge(String fareAmtBeforeSurge) {
            this.fareAmtBeforeSurge = fareAmtBeforeSurge;
        }

        public String getTotalFareWithOutOldBal() {
            return totalFareWithOutOldBal;
        }

        public void setTotalFareWithOutOldBal(String totalFareWithOutOldBal) {
            this.totalFareWithOutOldBal = totalFareWithOutOldBal;
        }

        public String getTaxPercentage() {
            return taxPercentage;
        }

        public void setTaxPercentage(String taxPercentage) {
            this.taxPercentage = taxPercentage;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getOldBalance() {
            return oldBalance;
        }

        public void setOldBalance(String oldBalance) {
            this.oldBalance = oldBalance;
        }

        public String getSafe() {
            return safe;
        }

        public void setSafe(String safe) {
            this.safe = safe;
        }

        public String getConveyance() {
            return conveyance;
        }

        public void setConveyance(String conveyance) {
            this.conveyance = conveyance;
        }

        public String getConveyanceKM() {
            return conveyanceKM;
        }

        public void setConveyanceKM(String conveyanceKM) {
            this.conveyanceKM = conveyanceKM;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getActualcost() {
            return actualcost;
        }

        public void setActualcost(String actualcost) {
            this.actualcost = actualcost;
        }

        public String getDetect() {
            return detect;
        }

        public void setDetect(String detect) {
            this.detect = detect;
        }

        public String getBal() {
            return bal;
        }

        public void setBal(String bal) {
            this.bal = bal;
        }

        public String getPromoamt() {
            return promoamt;
        }

        public void setPromoamt(String promoamt) {
            this.promoamt = promoamt;
        }

        public String getComison() {
            return comison;
        }

        public void setComison(String comison) {
            this.comison = comison;
        }

        public String getWaitingCharge() {
            return waitingCharge;
        }

        public void setWaitingCharge(String waitingCharge) {
            this.waitingCharge = waitingCharge;
        }

        public String getTimefare() {
            return timefare;
        }

        public void setTimefare(String timefare) {
            this.timefare = timefare;
        }

        public String getDistfare() {
            return distfare;
        }

        public void setDistfare(String distfare) {
            this.distfare = distfare;
        }

    }

    public class Adsp {

        @SerializedName("dLng")
        @Expose
        private String dLng;
        @SerializedName("dLat")
        @Expose
        private String dLat;
        @SerializedName("pLng")
        @Expose
        private String pLng;
        @SerializedName("pLat")
        @Expose
        private String pLat;
        @SerializedName("to")
        @Expose
        private String to;
        @SerializedName("from")
        @Expose
        private String from;
        @SerializedName("end")
        @Expose
        private Object end;
        @SerializedName("start")
        @Expose
        private String start;
        @SerializedName("map")
        @Expose
        private String map;

        public String getDLng() {
            return dLng;
        }

        public void setDLng(String dLng) {
            this.dLng = dLng;
        }

        public String getDLat() {
            return dLat;
        }

        public void setDLat(String dLat) {
            this.dLat = dLat;
        }

        public String getPLng() {
            return pLng;
        }

        public void setPLng(String pLng) {
            this.pLng = pLng;
        }

        public String getPLat() {
            return pLat;
        }

        public void setPLat(String pLat) {
            this.pLat = pLat;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public Object getEnd() {
            return end;
        }

        public void setEnd(Object end) {
            this.end = end;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getMap() {
            return map;
        }

        public void setMap(String map) {
            this.map = map;
        }

    }

    public class ApplyValues {

        @SerializedName("applyPickupCharge")
        @Expose
        private Boolean applyPickupCharge;
        @SerializedName("applyCommission")
        @Expose
        private Boolean applyCommission;
        @SerializedName("applyTax")
        @Expose
        private Boolean applyTax;
        @SerializedName("applyWaitingTime")
        @Expose
        private Boolean applyWaitingTime;
        @SerializedName("applyPeakCharge")
        @Expose
        private Boolean applyPeakCharge;
        @SerializedName("applyNightCharge")
        @Expose
        private Boolean applyNightCharge;

        public Boolean getApplyPickupCharge() {
            return applyPickupCharge;
        }

        public void setApplyPickupCharge(Boolean applyPickupCharge) {
            this.applyPickupCharge = applyPickupCharge;
        }

        public Boolean getApplyCommission() {
            return applyCommission;
        }

        public void setApplyCommission(Boolean applyCommission) {
            this.applyCommission = applyCommission;
        }

        public Boolean getApplyTax() {
            return applyTax;
        }

        public void setApplyTax(Boolean applyTax) {
            this.applyTax = applyTax;
        }

        public Boolean getApplyWaitingTime() {
            return applyWaitingTime;
        }

        public void setApplyWaitingTime(Boolean applyWaitingTime) {
            this.applyWaitingTime = applyWaitingTime;
        }

        public Boolean getApplyPeakCharge() {
            return applyPeakCharge;
        }

        public void setApplyPeakCharge(Boolean applyPeakCharge) {
            this.applyPeakCharge = applyPeakCharge;
        }

        public Boolean getApplyNightCharge() {
            return applyNightCharge;
        }

        public void setApplyNightCharge(Boolean applyNightCharge) {
            this.applyNightCharge = applyNightCharge;
        }

    }

    public class Csp {

        @SerializedName("dist")
        @Expose
        private String dist;
        @SerializedName("distfare")
        @Expose
        private String distfare;
        @SerializedName("time")
        @Expose
        private String time;
        @SerializedName("timefare")
        @Expose
        private String timefare;
        @SerializedName("comison")
        @Expose
        private String comison;
        @SerializedName("promoamt")
        @Expose
        private String promoamt;
        @SerializedName("promo")
        @Expose
        private String promo;
        @SerializedName("cost")
        @Expose
        private String cost;
        @SerializedName("conveyance")
        @Expose
        private String conveyance;
        @SerializedName("tax")
        @Expose
        private String tax;
        @SerializedName("taxPercentage")
        @Expose
        private String taxPercentage;
        @SerializedName("via")
        @Expose
        private String via;
        @SerializedName("driverCancelFee")
        @Expose
        private String driverCancelFee;
        @SerializedName("riderCancelFee")
        @Expose
        private String riderCancelFee;
        @SerializedName("nightPer")
        @Expose
        private String nightPer;
        @SerializedName("peakPer")
        @Expose
        private String peakPer;
        @SerializedName("isPeak")
        @Expose
        private Boolean isPeak;
        @SerializedName("isNight")
        @Expose
        private Boolean isNight;

        public String getDist() {
            return dist;
        }

        public void setDist(String dist) {
            this.dist = dist;
        }

        public String getDistfare() {
            return distfare;
        }

        public void setDistfare(String distfare) {
            this.distfare = distfare;
        }

        public String getTime() {
            return time;
        }

        public void setTime(String time) {
            this.time = time;
        }

        public String getTimefare() {
            return timefare;
        }

        public void setTimefare(String timefare) {
            this.timefare = timefare;
        }

        public String getComison() {
            return comison;
        }

        public void setComison(String comison) {
            this.comison = comison;
        }

        public String getPromoamt() {
            return promoamt;
        }

        public void setPromoamt(String promoamt) {
            this.promoamt = promoamt;
        }

        public String getPromo() {
            return promo;
        }

        public void setPromo(String promo) {
            this.promo = promo;
        }

        public String getCost() {
            return cost;
        }

        public void setCost(String cost) {
            this.cost = cost;
        }

        public String getConveyance() {
            return conveyance;
        }

        public void setConveyance(String conveyance) {
            this.conveyance = conveyance;
        }

        public String getTax() {
            return tax;
        }

        public void setTax(String tax) {
            this.tax = tax;
        }

        public String getTaxPercentage() {
            return taxPercentage;
        }

        public void setTaxPercentage(String taxPercentage) {
            this.taxPercentage = taxPercentage;
        }

        public String getVia() {
            return via;
        }

        public void setVia(String via) {
            this.via = via;
        }

        public String getDriverCancelFee() {
            return driverCancelFee;
        }

        public void setDriverCancelFee(String driverCancelFee) {
            this.driverCancelFee = driverCancelFee;
        }

        public String getRiderCancelFee() {
            return riderCancelFee;
        }

        public void setRiderCancelFee(String riderCancelFee) {
            this.riderCancelFee = riderCancelFee;
        }

        public String getNightPer() {
            return nightPer;
        }

        public void setNightPer(String nightPer) {
            this.nightPer = nightPer;
        }

        public String getPeakPer() {
            return peakPer;
        }

        public void setPeakPer(String peakPer) {
            this.peakPer = peakPer;
        }

        public Boolean getIsPeak() {
            return isPeak;
        }

        public void setIsPeak(Boolean isPeak) {
            this.isPeak = isPeak;
        }

        public Boolean getIsNight() {
            return isNight;
        }

        public void setIsNight(Boolean isNight) {
            this.isNight = isNight;
        }

    }

    public class Driverfb {

        @SerializedName("cmts")
        @Expose
        private String cmts;
        @SerializedName("rating")
        @Expose
        private String rating;

        public String getCmts() {
            return cmts;
        }

        public void setCmts(String cmts) {
            this.cmts = cmts;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

    }

    public class Dsp {

        @SerializedName("distanceKM")
        @Expose
        private String distanceKM;
        @SerializedName("start")
        @Expose
        private String start;
        @SerializedName("end")
        @Expose
        private String end;
        @SerializedName("endcoords")
        @Expose
        private List<String> endcoords = null;
        @SerializedName("startcoords")
        @Expose
        private List<String> startcoords = null;

        public String getDistanceKM() {
            return distanceKM;
        }

        public void setDistanceKM(String distanceKM) {
            this.distanceKM = distanceKM;
        }

        public String getStart() {
            return start;
        }

        public void setStart(String start) {
            this.start = start;
        }

        public String getEnd() {
            return end;
        }

        public void setEnd(String end) {
            this.end = end;
        }

        public List<String> getEndcoords() {
            return endcoords;
        }

        public void setEndcoords(List<String> endcoords) {
            this.endcoords = endcoords;
        }

        public List<String> getStartcoords() {
            return startcoords;
        }

        public void setStartcoords(List<String> startcoords) {
            this.startcoords = startcoords;
        }

    }

    public class ProfileDetail {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("fname")
        @Expose
        private String fname;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("profile")
        @Expose
        private String profile;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFname() {
            return fname;
        }

        public void setFname(String fname) {
            this.fname = fname;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getProfile() {
            return profile;
        }

        public void setProfile(String profile) {
            this.profile = profile;
        }

    }

    public class ReqDvr {

        @SerializedName("drvId")
        @Expose
        private String drvId;
        @SerializedName("called")
        @Expose
        private String called;
        @SerializedName("distVal")
        @Expose
        private String distVal;

        public String getDrvId() {
            return drvId;
        }

        public void setDrvId(String drvId) {
            this.drvId = drvId;
        }

        public String getCalled() {
            return called;
        }

        public void setCalled(String called) {
            this.called = called;
        }

        public String getDistVal() {
            return distVal;
        }

        public void setDistVal(String distVal) {
            this.distVal = distVal;
        }

    }

    public class Riderfb {

        @SerializedName("cmts")
        @Expose
        private String cmts;
        @SerializedName("rating")
        @Expose
        private String rating;

        public String getCmts() {
            return cmts;
        }

        public void setCmts(String cmts) {
            this.cmts = cmts;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

    }

    public class TripDetail {

        @SerializedName("_id")
        @Expose
        private String id;
        @SerializedName("tripno")
        @Expose
        private String tripno;
        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("cpy")
        @Expose
        private Object cpy;
        @SerializedName("cpyid")
        @Expose
        private Object cpyid;
        @SerializedName("dvr")
        @Expose
        private String dvr;
        @SerializedName("dvrid")
        @Expose
        private String dvrid;
        @SerializedName("rid")
        @Expose
        private String rid;
        @SerializedName("ridid")
        @Expose
        private String ridid;
        @SerializedName("fare")
        @Expose
        private String fare;
        @SerializedName("vehicle")
        @Expose
        private String vehicle;
        @SerializedName("service")
        @Expose
        private String service;
        @SerializedName("paymentMode")
        @Expose
        private String paymentMode;
        @SerializedName("estTime")
        @Expose
        private String estTime;
        @SerializedName("scId")
        @Expose
        private Object scId;
        @SerializedName("__v")
        @Expose
        private String v;
        @SerializedName("tripFDT")
        @Expose
        private String tripFDT;
        @SerializedName("utc")
        @Expose
        private String utc;
        @SerializedName("tripDT")
        @Expose
        private String tripDT;
        @SerializedName("driverfb")
        @Expose
        private Driverfb driverfb;
        @SerializedName("riderfb")
        @Expose
        private Riderfb riderfb;
        @SerializedName("needClear")
        @Expose
        private String needClear;
        @SerializedName("curReq")
        @Expose
        private List<String> curReq = null;
        @SerializedName("reqDvr")
        @Expose
        private List<ReqDvr> reqDvr = null;
        @SerializedName("review")
        @Expose
        private String review;
        @SerializedName("tripOTP")
        @Expose
        private List<String> tripOTP = null;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("applyValues")
        @Expose
        private ApplyValues applyValues;
        @SerializedName("adsp")
        @Expose
        private Adsp adsp;
        @SerializedName("acsp")
        @Expose
        private Acsp acsp;
        @SerializedName("dsp")
        @Expose
        private Dsp dsp;
        @SerializedName("csp")
        @Expose
        private Csp csp;
        @SerializedName("paymentSts")
        @Expose
        private String paymentSts;
        @SerializedName("bookingType")
        @Expose
        private String bookingType;
        @SerializedName("triptype")
        @Expose
        private String triptype;
        @SerializedName("requestFrom")
        @Expose
        private String requestFrom;
        @SerializedName("createdAt")
        @Expose
        private String createdAt;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getTripno() {
            return tripno;
        }

        public void setTripno(String tripno) {
            this.tripno = tripno;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Object getCpy() {
            return cpy;
        }

        public void setCpy(Object cpy) {
            this.cpy = cpy;
        }

        public Object getCpyid() {
            return cpyid;
        }

        public void setCpyid(Object cpyid) {
            this.cpyid = cpyid;
        }

        public String getDvr() {
            return dvr;
        }

        public void setDvr(String dvr) {
            this.dvr = dvr;
        }

        public String getDvrid() {
            return dvrid;
        }

        public void setDvrid(String dvrid) {
            this.dvrid = dvrid;
        }

        public String getRid() {
            return rid;
        }

        public void setRid(String rid) {
            this.rid = rid;
        }

        public String getRidid() {
            return ridid;
        }

        public void setRidid(String ridid) {
            this.ridid = ridid;
        }

        public String getFare() {
            return fare;
        }

        public void setFare(String fare) {
            this.fare = fare;
        }

        public String getVehicle() {
            return vehicle;
        }

        public void setVehicle(String vehicle) {
            this.vehicle = vehicle;
        }

        public String getService() {
            return service;
        }

        public void setService(String service) {
            this.service = service;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getEstTime() {
            return estTime;
        }

        public void setEstTime(String estTime) {
            this.estTime = estTime;
        }

        public Object getScId() {
            return scId;
        }

        public void setScId(Object scId) {
            this.scId = scId;
        }

        public String getV() {
            return v;
        }

        public void setV(String v) {
            this.v = v;
        }

        public String getTripFDT() {
            return tripFDT;
        }

        public void setTripFDT(String tripFDT) {
            this.tripFDT = tripFDT;
        }

        public String getUtc() {
            return utc;
        }

        public void setUtc(String utc) {
            this.utc = utc;
        }

        public String getTripDT() {
            return tripDT;
        }

        public void setTripDT(String tripDT) {
            this.tripDT = tripDT;
        }

        public Driverfb getDriverfb() {
            return driverfb;
        }

        public void setDriverfb(Driverfb driverfb) {
            this.driverfb = driverfb;
        }

        public Riderfb getRiderfb() {
            return riderfb;
        }

        public void setRiderfb(Riderfb riderfb) {
            this.riderfb = riderfb;
        }

        public String getNeedClear() {
            return needClear;
        }

        public void setNeedClear(String needClear) {
            this.needClear = needClear;
        }

        public List<String> getCurReq() {
            return curReq;
        }

        public void setCurReq(List<String> curReq) {
            this.curReq = curReq;
        }

        public List<ReqDvr> getReqDvr() {
            return reqDvr;
        }

        public void setReqDvr(List<ReqDvr> reqDvr) {
            this.reqDvr = reqDvr;
        }

        public String getReview() {
            return review;
        }

        public void setReview(String review) {
            this.review = review;
        }

        public List<String> getTripOTP() {
            return tripOTP;
        }

        public void setTripOTP(List<String> tripOTP) {
            this.tripOTP = tripOTP;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public ApplyValues getApplyValues() {
            return applyValues;
        }

        public void setApplyValues(ApplyValues applyValues) {
            this.applyValues = applyValues;
        }

        public Adsp getAdsp() {
            return adsp;
        }

        public void setAdsp(Adsp adsp) {
            this.adsp = adsp;
        }

        public Acsp getAcsp() {
            return acsp;
        }

        public void setAcsp(Acsp acsp) {
            this.acsp = acsp;
        }

        public Dsp getDsp() {
            return dsp;
        }

        public void setDsp(Dsp dsp) {
            this.dsp = dsp;
        }

        public Csp getCsp() {
            return csp;
        }

        public void setCsp(Csp csp) {
            this.csp = csp;
        }

        public String getPaymentSts() {
            return paymentSts;
        }

        public void setPaymentSts(String paymentSts) {
            this.paymentSts = paymentSts;
        }

        public String getBookingType() {
            return bookingType;
        }

        public void setBookingType(String bookingType) {
            this.bookingType = bookingType;
        }

        public String getTriptype() {
            return triptype;
        }

        public void setTriptype(String triptype) {
            this.triptype = triptype;
        }

        public String getRequestFrom() {
            return requestFrom;
        }

        public void setRequestFrom(String requestFrom) {
            this.requestFrom = requestFrom;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

    }

}
