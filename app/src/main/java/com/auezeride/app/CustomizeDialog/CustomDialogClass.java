package com.auezeride.app.CustomizeDialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.ChangePasswordModel;
import com.auezeride.app.Presenter.ChangePasswordPresenter;

import com.auezeride.app.R;
import com.auezeride.app.View.ChangePasswordView;

import java.util.HashMap;
import java.util.List;

import retrofit2.Response;

public class CustomDialogClass extends Dialog implements Validator.ValidationListener, View.OnClickListener, ChangePasswordView {

    public Activity c;
    public Dialog d;
    @NotEmpty(message = "Require")
    private
    MaterialEditText oldPasswordTxt;
    @NotEmpty(message = "Require")
    private
    MaterialEditText newPasswordTxt;
    @NotEmpty(message = "Require")
    private
    MaterialEditText passwordAgainTxt;

    TextView cancelTxt;
    TextView okTxt;

    Validator validator;

    public CustomDialogClass(@NonNull Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.changepassword);
        validator = new Validator(this);
        validator.setValidationListener(this);
        FindviewbyID();
    }

    View decorView;

    private void FindviewbyID() {
        okTxt = findViewById(R.id.ok_txt);
        cancelTxt = findViewById(R.id.cancel_txt);
        decorView = this.getWindow().getDecorView();
        oldPasswordTxt = findViewById(R.id.old_password_txt);
        newPasswordTxt = findViewById(R.id.new_password_txt);
        passwordAgainTxt = findViewById(R.id.password_again_txt);
        okTxt.setOnClickListener(this);
        cancelTxt.setOnClickListener(this);
    }


    @Override
    public void onValidationSucceeded() {
        if (oldPasswordTxt.getText().toString().equalsIgnoreCase(newPasswordTxt.getText().toString())) {
            Utiles.displayMessage(getCurrentFocus(), c.getApplicationContext(), c.getResources().getString(R.string.old_password_and_new_password_same));
        } else if (!newPasswordTxt.getText().toString().equalsIgnoreCase(passwordAgainTxt.getText().toString())) {
            Utiles.displayMessage(getCurrentFocus(), c.getApplicationContext(), c.getResources().getString(R.string.confirm_password_wrong));
        }else
        {
            HashMap<String, String> map = new HashMap<>();
            map.put("oldpassword", oldPasswordTxt.getText().toString());
            map.put("newpassword", newPasswordTxt.getText().toString());
            map.put("confirmpassword", passwordAgainTxt.getText().toString());
            ChangePasswordPresenter changePasswordPresenter = new ChangePasswordPresenter(this);
            changePasswordPresenter.ChangePasswor(map, c, c.getApplicationContext());

        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(c);
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(c, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.cancel_txt:
                Utiles.DismissiAnimation(decorView, this);
                break;
            case R.id.ok_txt:
                validator.validate();
                break;
        }
    }

    @Override
    public void OnSuccessfully(Response<ChangePasswordModel> Response) {
        Utiles.displayMessage(c.getCurrentFocus(), c.getApplicationContext(), Response.body().getMessage());
        Utiles.DismissiAnimation(decorView, this);
    }

    @Override
    public void OnFailure(Response<ChangePasswordModel> Response) {
        try {
            Utiles.showErrorMessage(Response.errorBody().string(), c, getCurrentFocus());
        } catch (Exception e) {
            e.printStackTrace();
            Utiles.displayMessage(getCurrentFocus(), c, c.getResources().getString(R.string.poor_network));
        }
    }
}
