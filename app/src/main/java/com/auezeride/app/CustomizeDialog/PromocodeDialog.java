package com.auezeride.app.CustomizeDialog;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.auezeride.app.CommonClass.CommonData;
import com.auezeride.app.CommonClass.Utiles;
import com.auezeride.app.Model.PromoCodeModel;
import com.auezeride.app.Presenter.PromocodePresenter;
import com.auezeride.app.R;
import com.auezeride.app.View.PromoView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Objects;

import retrofit2.Response;

public class PromocodeDialog extends Dialog implements PromoView {

    public Activity activity;
    public Dialog dialog;
    private MaterialEditText promocodeTxt;
    private Button submit;
    private TextView discount_amount_txt;
    View decorView;
    private String strValiation ="";
    public PromocodeDialog(Activity activity) {
        super(activity);
        // TODO Auto-generated constructor stub
        this.activity = activity;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.promocode);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        ;
        submit = findViewById(R.id.submit);
        decorView = Objects.requireNonNull(this.getWindow()).getDecorView();
        discount_amount_txt = findViewById(R.id.discount_amount_txt);
        promocodeTxt = findViewById(R.id.promocode_txt);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (strValiation.equalsIgnoreCase("OK")) {
                    Utiles.DismissiAnimation(decorView, PromocodeDialog.this);
                } else {
                    if (Objects.requireNonNull(promocodeTxt.getText()).toString().isEmpty()) {
                        promocodeTxt.setError(activity.getResources().getString(R.string.enter_your_promo_code));
                    } else {
                        CommonData.strpromocode = promocodeTxt.getText().toString().toUpperCase();
                        PromocodePresenterCall();
                    }

                }

            }
        });

    }

    public void PromocodePresenterCall() {
        PromocodePresenter promocodePresenter = new PromocodePresenter(activity, this);
        promocodePresenter.getProcodeResult(Objects.requireNonNull(promocodeTxt.getText()).toString());

    }

    @SuppressLint("SetTextI18n")
    @Override
    public void OnSuccessfully(Response<PromoCodeModel> Response) {
        if (Response.body().getSuccess()) {
            discount_amount_txt.setVisibility(View.VISIBLE);
            discount_amount_txt.setText(activity.getResources().getString(R.string.congrats_discount) + Response.body().getDiscountAmt()+ activity.getResources().getString(R.string.percent_discount));
            submit.setText(activity.getResources().getString(R.string.okay));
            strValiation ="OK";
        } else {
            Utiles.displayMessage(getCurrentFocus(), activity, activity.getResources().getString(R.string.something_went_wrong));
        }
    }

    @Override
    public void OnFailure(Response<PromoCodeModel> Response) {
        if (Response.errorBody() != null) {
            try {
                String message = Response.errorBody().string();
                JSONObject jsonObject = new JSONObject(message);
                if (jsonObject.has("message")) {
                    Utiles.displayMessage(getCurrentFocus(), activity, jsonObject.optString("message"));
                }
            } catch (IOException | JSONException e) {
                e.printStackTrace();
            }
        }

    }
}
