package com.auezeride.app.CustomizeDialog;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import androidx.annotation.NonNull;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.auezeride.app.R;

import butterknife.BindView;

/**
 * Created by com on 18-Sep-18.
 */

public class OutstationAlertDialog extends Dialog {

    public Activity activity;
    public String amount;
    @BindView(R.id.close_imgbtn)
    ImageButton closeImgbtn;
    @BindView(R.id.animation_view)
    LottieAnimationView animationView;
    @BindView(R.id.title_txt)
    TextView title_txt;


    public OutstationAlertDialog(@NonNull Activity activity) {
        super(activity);
        this.activity = activity;
        this.amount = amount;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        setContentView(R.layout.outstationalert);
        title_txt = findViewById(R.id.title_txt);
        //title_txt.setText();
        closeImgbtn = findViewById(R.id.close_imgbtn);
        closeImgbtn.setOnClickListener(view -> {
            dismiss();

        });
    }
}
